#use wml::debian::translation-check translation="55f28f8ffad4f01a052c4b81349871fabf4f6e93" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29374">CVE-2020-29374</a>

    <p>Jann Horn fra Google rapporterede om en fejl i Linux' håndtering af 
    virtuel hukommelse.  En forælder- og et barnproces deler i begyndelsen al 
    deres hukommelse, men når en af dem skriver til en delt side, duplikeres 
    siden og delingen ophører (copy-on-write).  Men i tilfælde af at en 
    handling så som vmsplice() krævede at kernen modtog en yderligere reference 
    til en delt side, og copy-on-write optræder under handlingen, kunne kernen 
    have tilgået den forkerte proces' hukommelse.  I nogle programmer kunne det 
    føre til en informationslækage eller datakorruption.</p>

    <p>Dette problem er allerede rettet i de fleste arkitekturer, men ikke på 
    MIPS og System z.  Det løses af denne opdatering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36322">CVE-2020-36322</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28950">CVE-2021-28950</a>

    <p>Værktøjet syzbot opdagede at implementeringen af FUSE 
    (filesystem-in-user-space), ikke på korrekt vis håndterede en FUSE-server, 
    som returnerer ugyldige filattributter.  En lokal bruger med rettigheder til 
    at køre en FUSE-server, kunne udnytte det fejlen til at forårsage et 
    lammelsesangreb (nedbrud).</p>

    <p>Den oprindelige rettelse heraf, indførte et andet potentielt 
    lammelsesangreb (uendelig løkke i kernerummet), hvilket også er 
    rettet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3640">CVE-2021-3640</a>

    <p>Lin Ma opdagede en kapløbstilstand i implementeringen af 
    Bluetooth-protokollen, hvilken kunne køre til en anvendelse efter 
    frigivelse.  En lokal bruger kunne udnytte fejlen til at forårsage et 
    lammelsesangreb (hukommelseskorruption eller nedbrud) eller muligvis 
    til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3744">CVE-2021-3744</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-3764">CVE-2021-3764</a>

    <p>minihanshen rapporterede om fejl i ccp-driveren til AMD Cryptographic 
    Coprocessors, hvilke kunne føre til en ressourcelækage.  På systemer, der 
    anvender driveren, kunne en lokal bruger udnytte fejlen til at forårsage et 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3752">CVE-2021-3752</a>

    <p>Likang Luo fra NSFOCUS Security Team opdagede en fejl i implementeringen 
    af Bluetooth L2CAP, der kunne føre til en anvendelse efter frigivelse.  En 
    lokal bruger kunne udnytte fejlen til at forårsage et lammelsesangreb 
    (hukommelseskorruption eller nedbrud) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3760">CVE-2021-3760</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-4202">CVE-2021-4202</a>

    <p>Lin Ma opdagede kapløbstilstande i NCI-driveren (NFC Controller 
    Interface), hvilke kunne føre til en anvendelse efter frigivelse.  En lokal 
    bruger kunne udnytte fejlen til at forårsage et lammelsesangreb 
    (hukommelseskorruption eller nedbrud) eller muligvis til 
    rettighedsforøgelse.</p>

    <p>Driveren er ikke aktiveret i Debians officielle 
    kerneopsætninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3772">CVE-2021-3772</a>

    <p>En fejl blev fundet i implementeringen af SCTP-protokollen, hvilke kunne 
    gøre det muligt for en netværksforbundet angriber, at afbryde en 
    SCTP-tilknytning.  Angriberen behøvede kun at kende eller gætte IP-adressen 
    og portene, som benyttes i tilknytningen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4002">CVE-2021-4002</a>

    <p>Man opdagede at hugetlbfs, det virtuelle filsystem, der anvendes af 
    applikationer til at allokere enorme sider i RAM, ikke tømte CPU'ens TLB i 
    et tilfælde, hvor det var nødvendigt.  Under nogle omstændigheder, ville en 
    lokal bruger være i stand til at læse og skrive enorme sider, efter deres 
    frigivelse og genallokering til en anden proces.  Det kunne føre til 
    rettighedsforøgelse, lammelsesangreb eller informationslækager.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4083">CVE-2021-4083</a>

    <p>Jann Horn rapporterede om en kapløbstilstand i de lokale (Unix) 
    sockets-garbagecollector, hvilket kunne føre til anvendelse efter 
    frigivelse.  En lokal bruger kunne udnytte det til at forårsage et 
    lammelsesangreb (hukommelseskorruption eller nedbrud) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4135">CVE-2021-4135</a>

    <p>En fejl blev fundet i driveren netdevsim, hvilken kunne føre til en 
    informationslækage.</p>

    <p>Driveren er ikke aktiveret i Debians officielle 
    kerneopsætninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4155">CVE-2021-4155</a>

    <p>Kirill Tkhai opdagede en datalækage i den måde, IOCTL'en XFS_IOC_ALLOCSP 
    i XFS-filsystemet muliggjorde at filer med en ikke-justeret størrelse kunne 
    gøre større på.  En lokal angriber kunne drage nytte af fejlen til at lække 
    data fra XFS-filsystemet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4203">CVE-2021-4203</a>

    <p>Jann Horn rapporterede om en kapløbstilstand i implementeringen af de 
    lokale (Unix-) sockets, hvilken kunne føre til en anvendelse efter 
    frigivelse.  En lokal bruger kunne udnytte fejlen til at lække følsomme 
    oplysninger fra kernen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20317">CVE-2021-20317</a>

    <p>Man opdagede at timerkøstrukturen kunne blive korrupt, førende til at 
    ventende tasks aldrig blev vækket.  En lokal bruger med visse rettigheder, 
    kunne udnytte fejlen til at forårsage et lammelsesangreb (hængende 
    system).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20321">CVE-2021-20321</a>

    <p>En kapløbstilstand blev opdaget i filsystemsdriveren overlayfs.  En lokal 
    bruger med adgang til en overlayfs-mount og til dens overliggende mappe, 
    kunne udnytte fejlen til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20322">CVE-2021-20322</a>

    <p>En informationslækage blev opdaget i IPv4-implementeringen.  En 
    fjernangriber kunne udnytte fejlen til hurtigt at opdage hvilke UDP-porte, 
    et system anvender, gørende det lettere for vedkommende at udføre et 
    DNS-forgiftningsangreb mod det system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22600">CVE-2021-22600</a>

    <p>Værktøjet syzbot fandt en fejl i implementeringen af packetsocket 
    (AF_PACKET), hvilken kunne føre til ukorrekt frigivelse af hukommense.  En 
    lokal bruger med kapabiliteten CAP_NET_RAW (i et hvilket som helst 
    brugernavnerum), kunne udnytte fejlen til lammelsesangreb 
    (hukommelseskorruption eller nedbrud) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28711">CVE-2021-28711</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28712">CVE-2021-28712</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28713">CVE-2021-28713</a> (XSA-391)

    <p>Juergen Gross rapporterede om at ondsindede PV-backends kunne forårsage 
    et lammelsesangreb mod gæster, der serviceres af disse backends gennem 
    hyppigt forekommende events, selv hvis disse backends kører i et mindre 
    prviligeret miljø.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28714">CVE-2021-28714</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28715">CVE-2021-28715</a> (XSA-392)

    <p>Juergen Gross opdagede at Xen-gæster kunne tvinge Linux' netbackdriver 
    til at lægge beslag på store mængder kernehukommelse, medførende 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38300">CVE-2021-38300</a>

    <p>Piotr Krysiuk opdagede en fejl i den klassiske BPF (cBPF) JIT-kompiler 
    til MIPS-arkitekturer.  En lokal bruger kunne udnytte fejlen til at udføre 
    vilkårlig kode i kernen.</p>

    <p>Problemet er afhjulpet ved at sætte sysctl net.core.bpf_jit_enable=0, 
    hvilket er standarden.  Det er *ikke* afhjulpet ved at deaktivere 
    upriviligeret anvendelse af eBPF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39685">CVE-2021-39685</a>

    <p>Szymon Heidrich opdagede en bufferoverløbssårbarhed i 
    USB-gadgetundersystemet, medførende informationsafsløring, lammelsesangreb 
    eller rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39686">CVE-2021-39686</a>

    <p>En kapløbstilstand blev opdaget i Android-binderdriveren, der kunne føre 
    til ukorrekte sikkerhedstjek.  På systemer hvor binderdriveren er indlæst, 
    kunne en lokal bruger udnytte fejlen til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39698">CVE-2021-39698</a>

    <p>Linus Torvalds rapporterede om en fejl i implementeringen af filpolling, 
    hvilken kunne føre til en anvendelse efter frigivelse.  En lokal bruger 
    kunne udnytte fejlen til lammelsesangreb (hukommelseskorruption eller 
    nedbrud) eller muligvis til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39713">CVE-2021-39713</a>

    <p>Værktøjet syzbot fandt en kapløbstilstand i network 
    scheduling-undersystemet, hvilken kunne føre til en anvendelse efter 
    frigivelse.  En lokal bruger kunne udnytte fejlen til lammelsesangreb 
    (hukommelseskorruption eller nedbrud) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41864">CVE-2021-41864</a>

    <p>Et heltalsoverløb blev opdaget i Extended BPF-undersystemet (eBPF).  En 
    lokal bruger kunne udnytte fejlen til lammelsesangreb (hukommelseskorruption 
    eller nedbrud) eller muligvis til rettighedsforøgelse.</p>

    <p>Det kan afhjælpes ved at sætte sysctl kernel.unprivileged_bpf_disabled=1, 
    hvilket deaktiverer anvendelse af eBPF ved upriviligerede brugere.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42739">CVE-2021-42739</a>

    <p>Et heapbufferoverløb blev opdaget i firedtv-driveren til 
    FireWire-forbundne DVB-modtagere.  En lokal bruger med adgang til en 
    firedtv-enhed, kunne udnytte fejlen til lammelsesangreb 
    (hukommelseskorruption eller nedbrud) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43389">CVE-2021-43389</a>

    <p>Active Defense Lab hos Venustech opdagede en fejl i CMTP-undersystemet, 
    ved anvendelse fra Bluetooth, hvilken kunne føre til en læsning udenfor 
    grænserne og objekttypeforvirring.  En lokal bruger med kapabiliteten 
    CAP_NET_ADMIN i det indledende brugernavnerum, kunne udnytte fejlen til 
    lammelsesangreb (hukommelseskorruption eller nedbrud) eller muligvis til 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43975">CVE-2021-43975</a>

    <p>Brendan Dolan-Gavitt rapporterede om en fejl i funktionen 
    hw_atl_utils_fw_rpc_wait() i ethernedenhedsdriveren aQuantia AQtion, hvilken 
    kunne medføre lammelsesangreb eller udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43976">CVE-2021-43976</a>

    <p>Zekun Shen og Brendan Dolan-Gavitt opdagede en fejl i funktionen 
    mwifiex_usb_recv() i USB-driveren Marvell WiFi-Ex.  En angriber, der er i 
    stand til at tilslutte en fabrikeret USB-enhed, kunne drage nytte af fejlen 
    til at forårsage et lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44733">CVE-2021-44733</a>

    <p>En kapløbstilstand blev opdaget i undersystemet Trusted Execution
    Environment (TEE) til Arm-processorer, hvilken kunne føre til en anvendelse 
    efter frigivelse.  En lokal bruger med rettigheder til at tilgå en 
    TEE-enhed, kunne udnytte fejlen til lammelsesangreb (hukommelseskorruption 
    eller nedbrud) eller muligvis til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45095">CVE-2021-45095</a>

    <p>Man opdagede at Phone Network-protokoldriveren (PhoNet), havde en 
    referenceoptællingslækage i funktionen pep_sock_accept().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45469">CVE-2021-45469</a>

    <p>Wenqing Liu rapporterede om en hukommelsestilgang udenfor grænserne i 
    f2fs-implementeringen, hvis en inode har en ugyldig sidste xattr-post.  En 
    angriber, der er i stand til at mounte et særligt fremstillet filaftryk, 
    kunne drage nytte af fejlen til lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45480">CVE-2021-45480</a>

    <p>En hukommelseslækagefejl blev opdaget i funktionen __rds_conn_create() i 
    RDS-protokolundersystemet (Reliable Datagram Sockets).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a> (INTEL-SA-00598)

    <p>Efterforskere ved VUSec opdagede at Branch History Buffer i 
    Intel-processorer kunne udnyttes til at iværksætte informationssidekanaler 
    med spekulativ udførelse.  Problemet svarer til Spectre variant 2, men 
    kræver yderligere afhjælpningsforanstaltninger på nogle processorer.</p>

    <p>Det kan udnyttes til at få fat i følsomme oplysninger fra en anden 
    sikkerhedskontekt, eksempelvis fra brugerrummet til kernen, eller fra en 
    KVM-gæst til kernen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0002">CVE-2022-0002</a> (INTEL-SA-00598)

    <p>Et problem svarende til 
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">\
    CVE-2022-0001</a>, men dækker udnyttelse indenfor en sikkerhedskontekst, 
    eksempelvis fra JIT-kompileret kode i en sandkasse til værtskode i den 
    samme proces.</p>

    <p>Det er delvist afhjulpet ved at deaktivere eBPF for upriviligerede 
    brugere med sysctl: kernel.unprivileged_bpf_disabled=2.  Det gør denne 
    opdatering som standard.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0322">CVE-2022-0322</a>

    <p>Eiichi Tsukata opdagede en fejl i funktionen sctp_make_strreset_req() i 
    implementeringen af SCTP-netværksprotokollen, hvilken kunne medføre 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0330">CVE-2022-0330</a>

    <p>Sushma Venkatesh Reddy opdagede en manglende GPU TLB-flush i 
    i915-driveren, medførende lammelsesangreb eller 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0435">CVE-2022-0435</a>

    <p>Samuel Page og Eric Dumazet rapporteret om et stakoverløb i 
    netværksmodulet til protokollen Transparent Inter-Process Communication 
    (TIPC), medførende lammelsesangreb eller potentielt udførelse af vilkårlig 
    kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0487">CVE-2022-0487</a>

    <p>En anvendelse efter frigivelse blev opdaget i MOXART SD/MMC Host 
    Controller-supportdriveren.  Fejlen påvirker ikke Debians binære pakker, da 
    CONFIG_MMC_MOXART ikke er opsat.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0492">CVE-2022-0492</a>

    <p>Yiqi Sun og Kevin Wang rapporterede at undersystemet cgroup-v1 ikke på 
    korrekt vis begrænsede adgang til release-agent-funktionaliteten.  En lokal 
    bruger kunne drage nytte af fejlen til rettighedsforøgelse og omgåelse af 
    navnerumsisolation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0617">CVE-2022-0617</a>

    <p>butt3rflyh4ck opdagede en NULL-pointerdereference i UDF-filsystemet.  En 
    lokal bruger, der kan mounte et særligt fremstillet UDF-filaftryk, kunne 
    udnytte fejlen til at få systemet til at gå ned.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0644">CVE-2022-0644</a>

    <p>Hao Sun rapporterede om en manglende kontrol af fillæsningsrettigheder i 
    systemkaldene finit_module() og kexec_file_load().  Sikkerhedspåvirkningen 
    af fejlen er uklar, da disse systemkald normalt kun er tilgængelige til 
    root-brugeren.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22942">CVE-2022-22942</a>

    <p>Man opdagede at forkert håndtering af fildescriptorer i driveren VMware 
    Virtual GPU (vmwgfx), kunne medføre informationslækage eller 
    rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24448">CVE-2022-24448</a>

    <p>Lyu Tao rapporterede om en fejl i NFS-implementeringen i Linux-kernen, 
    når der håndteres forespørgsler til at åbne en mappe til en almindelig fil, 
    hvilket kunne medføre en informationslækage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24959">CVE-2022-24959</a>

    <p>En hukommelseslækage blev opdaget i funktionen yam_siocdevprivate() i 
    YAM-driveren til AX.25, hvilken kunne medføre lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25258">CVE-2022-25258</a>

    <p>Szymon Heidrich rapporterede at USB Gadget-undersystemet manglede visse 
    valideringer af descriptorforespørgsler til grænseflade-OS, medførende 
    hukommelseskorruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25375">CVE-2022-25375</a>

    <p>Szymon Heidrich rapporterede at RNDIS USB-gadget manglende validering af 
    størrelsen på kommandoen RNDIS_MSG_SET, medførende informationslækage fra 
    kernehukommelse.</p></li>

</ul>

<p>I den gamle stabile distribution (buster), er disse problemer rettet i 
version 4.19.232-1.  Denne opdatering indeholder mange yderligere fejlrettelser 
fra stabile opdateringer, herunder 4.19.209-4.19.232.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5096.data"
