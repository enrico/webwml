#use wml::debian::translation-check translation="1d1f8d159bd57a26b5a8603a6dfc4a1937981b1c" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Samba, en Unix-server til SMB/CIFS, print og 
login.  Projektet Common Vulnerabilities and Exposures har registreret følgende 
problemer:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14629">CVE-2018-14629</a>

    <p>Florian Stuelpner opdagede at Samba er sårbar over for uendelig 
    forespørgselsrekursion forårsaget af CNAME-løkker, medførende 
    lammelsangreb.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-14629.html">\
    https://www.samba.org/samba/security/CVE-2018-14629.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16841">CVE-2018-16841</a>

    <p>Alex MacCuish opdagede at en bruger med et gyldigt certifikat eller 
    smartcard, kunne få Samba AD DC's KDC til at gå ned, når den er opsat 
    til at acceptere smartcard-autentifikation.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-16841.html">\
    https://www.samba.org/samba/security/CVE-2018-16841.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16851">CVE-2018-16851</a>

    <p>Garming Sam fra Samba Team og Catalyst opdagede en sårbarhed i 
    forbindelse med en NULL-pointerdereference i Samba AD DC's LDAP-server, som 
    gjorde det muligt for en bruger at læse mere end 256 MB LDAP-poster, at 
    få Samba AD DC's LDAP-server til at gå ned.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-16851.html">\
    https://www.samba.org/samba/security/CVE-2018-16851.html</a></p></li>

</ul>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 2:4.5.12+dfsg-2+deb9u4.</p>

<p>Vi anbefaler at du opgraderer dine samba-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende samba, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4345.data"
