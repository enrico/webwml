#use wml::debian::translation-check translation="6fbca817d300a20809fec1f06bb2ae3c92689156"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19880">CVE-2019-19880</a>

    <p>Richard Lorenz descubrió un problema en la biblioteca sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19923">CVE-2019-19923</a>

    <p>Richard Lorenz descubrió un problema de lectura fuera de límites en la biblioteca
    sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19925">CVE-2019-19925</a>

    <p>Richard Lorenz descubrió un problema en la biblioteca sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19926">CVE-2019-19926</a>

    <p>Richard Lorenz descubrió un error de implementación en la biblioteca sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6381">CVE-2020-6381</a>

    <p>El Centro Nacional de Ciberseguridad del Reino Unido (National Cyber Security Centre) descubrió un problema de desbordamiento de entero
    en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6382">CVE-2020-6382</a>

    <p>Soyeon Park y Wen Xu descubrieron un error de tipo en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6383">CVE-2020-6383</a>

    <p>Sergei Glazunov descubrió un error de tipo en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6384">CVE-2020-6384</a>

    <p>David Manoucheri descubrió un problema de «uso tras liberar» en WebAudio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6385">CVE-2020-6385</a>

    <p>Sergei Glazunov descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6386">CVE-2020-6386</a>

    <p>Zhe Jin descubrió un problema de «uso tras liberar» en el procesamiento de voz.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6387">CVE-2020-6387</a>

    <p>Natalie Silvanovich descubrió un error de escritura fuera de límites en la implementación
    de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6388">CVE-2020-6388</a>

    <p>Sergei Glazunov descubrió un error de lectura fuera de límites en la implementación
    de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6389">CVE-2020-6389</a>

    <p>Natalie Silvanovich descubrió un error de escritura fuera de límites en la implementación
    de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6390">CVE-2020-6390</a>

    <p>Sergei Glazunov descubrió un error de lectura fuera de límites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6391">CVE-2020-6391</a>

    <p>Michał Bentkowski descubrió que no se validaban suficientemente entradas no
    confiables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6392">CVE-2020-6392</a>

    <p>El Microsoft Edge Team descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6393">CVE-2020-6393</a>

    <p>Mark Amery descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6394">CVE-2020-6394</a>

    <p>Phil Freo descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6395">CVE-2020-6395</a>

    <p>Pierre Langlois descubrió un error de lectura fuera de límites en la
    biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6396">CVE-2020-6396</a>

    <p>William Luc Ritchie descubrió un error en la biblioteca skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6397">CVE-2020-6397</a>

    <p>Khalil Zhani descubrió un error en la interfaz de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6398">CVE-2020-6398</a>

    <p>pdknsk descubrió una variable no inicializada en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6399">CVE-2020-6399</a>

    <p>Luan Herrera descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6400">CVE-2020-6400</a>

    <p>Takashi Yoneuchi descubrió un error en el intercambio de recursos de origen cruzado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6401">CVE-2020-6401</a>

    <p>Tzachy Horesh descubrió que la entrada proporcionada por el usuario no se validaba lo suficiente.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6402">CVE-2020-6402</a>

    <p>Vladimir Metnew descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6403">CVE-2020-6403</a>

    <p>Khalil Zhani descubrió un error en la interfaz de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6404">CVE-2020-6404</a>

    <p>kanchi descubrió un error en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6405">CVE-2020-6405</a>

    <p>Yongheng Chen y Rui Zhong descubrieron un problema de lectura fuera de límites en la
    biblioteca sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6406">CVE-2020-6406</a>

    <p>Sergei Glazunov descubrió un problema de «uso tras liberar».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6407">CVE-2020-6407</a>

    <p>Sergei Glazunov descubrió un error de lectura fuera de límites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6408">CVE-2020-6408</a>

    <p>Zhong Zhaochen descubrió un error de imposición de reglas en el intercambio de recursos
    de origen cruzado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6409">CVE-2020-6409</a>

    <p>Divagar S y Bharathi V descubrieron un error en la implementación
    de omnibox.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6410">CVE-2020-6410</a>

    <p>evil1m0 descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6411">CVE-2020-6411</a>

    <p>Khalil Zhani descubrió que la entrada proporcionada por el usuario no se validaba lo suficiente.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6412">CVE-2020-6412</a>

    <p>Zihan Zheng descubrió que la entrada proporcionada por el usuario no se validaba lo suficiente.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6413">CVE-2020-6413</a>

    <p>Michał Bentkowski descubrió un error en Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6414">CVE-2020-6414</a>

    <p>Lijo A.T descubrió un error de imposición de reglas en la navegación segura («safe browsing»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6415">CVE-2020-6415</a>

    <p>Avihay Cohen descubrió un error de implementación en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6416">CVE-2020-6416</a>

    <p>Woojin Oh descubrió que no se validaban suficientemente entradas no confiables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6418">CVE-2020-6418</a>

    <p>Clement Lecigne descubrió un error de tipo en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6420">CVE-2020-6420</a>

    <p>Taras Uzdenov descubrió un error de imposición de reglas.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), el soporte de seguridad para chromium se
ha discontinuado.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 80.0.3987.132-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4638.data"
