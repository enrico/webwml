#use wml::debian::translation-check translation="0f2e63875207f63029067f8878524f544b09a20f"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto las vulnerabilidades siguientes en el motor web
webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3862">CVE-2020-3862</a>

    <p>Srikanth Gatta descubrió que un sitio web malicioso podría
    provocar denegación de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3864">CVE-2020-3864</a>

    <p>Ryan Pickren descubrió que un contexto de objetos del DOM puede no haber tenido
    un origen de seguridad único.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3865">CVE-2020-3865</a>

    <p>Ryan Pickren descubrió que se ha podido considerar seguro equivocadamente
    un contexto de objetos del DOM del nivel más alto.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3867">CVE-2020-3867</a>

    <p>Un investigador anónimo descubrió que el procesamiento de contenido
    web preparado maliciosamente puede dar lugar a ejecución de scripts entre sitios universal («universal cross site scripting»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3868">CVE-2020-3868</a>

    <p>Marcin Towalski descubrió que el procesamiento de contenido web preparado
    maliciosamente puede dar lugar a ejecución de código arbitrario.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.26.4-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de webkit2gtk.</p>

<p>Para información detallada sobre el estado de seguridad de webkit2gtk, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4627.data"
