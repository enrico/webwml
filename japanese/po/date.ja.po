msgid ""
msgstr ""
"Project-Id-Version: date.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2010-12-15 08:15+0900\n"
"Last-Translator: Nobuhiro Iwamatsu <iwamatsu@debian.org>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. List of weekday names (used in modification dates)
#: ../../english/template/debian/ctime.wml:11
msgid "Sun"
msgstr "(日)"

#: ../../english/template/debian/ctime.wml:12
msgid "Mon"
msgstr "(月)"

#: ../../english/template/debian/ctime.wml:13
msgid "Tue"
msgstr "(火)"

#: ../../english/template/debian/ctime.wml:14
msgid "Wed"
msgstr "(水)"

#: ../../english/template/debian/ctime.wml:15
msgid "Thu"
msgstr "(木)"

#: ../../english/template/debian/ctime.wml:16
msgid "Fri"
msgstr "(金)"

#: ../../english/template/debian/ctime.wml:17
msgid "Sat"
msgstr "(土)"

#. List of month names (used in modification dates, and may be used in news 
#. listings)
#: ../../english/template/debian/ctime.wml:23
msgid "Jan"
msgstr "1"

#: ../../english/template/debian/ctime.wml:24
msgid "Feb"
msgstr "2"

#: ../../english/template/debian/ctime.wml:25
msgid "Mar"
msgstr "3"

#: ../../english/template/debian/ctime.wml:26
msgid "Apr"
msgstr "4"

#: ../../english/template/debian/ctime.wml:27
msgid "May"
msgstr "5"

#: ../../english/template/debian/ctime.wml:28
msgid "Jun"
msgstr "6"

#: ../../english/template/debian/ctime.wml:29
msgid "Jul"
msgstr "7"

#: ../../english/template/debian/ctime.wml:30
msgid "Aug"
msgstr "8"

#: ../../english/template/debian/ctime.wml:31
msgid "Sep"
msgstr "9"

#: ../../english/template/debian/ctime.wml:32
msgid "Oct"
msgstr "10"

#: ../../english/template/debian/ctime.wml:33
msgid "Nov"
msgstr "11"

#: ../../english/template/debian/ctime.wml:34
msgid "Dec"
msgstr "12"

#. List of long month names (may be used in "spoken" dates and date ranges).
#: ../../english/template/debian/ctime.wml:39
msgid "January"
msgstr "1"

#: ../../english/template/debian/ctime.wml:40
msgid "February"
msgstr "2"

#: ../../english/template/debian/ctime.wml:41
msgid "March"
msgstr "3"

#: ../../english/template/debian/ctime.wml:42
msgid "April"
msgstr "4"

#. The <void> tag is to distinguish short and long forms of May.
#. Do not put it in msgstr.
#: ../../english/template/debian/ctime.wml:45
msgid "<void id=\"fullname\" />May"
msgstr "5"

#: ../../english/template/debian/ctime.wml:46
msgid "June"
msgstr "6"

#: ../../english/template/debian/ctime.wml:47
msgid "July"
msgstr "7"

#: ../../english/template/debian/ctime.wml:48
msgid "August"
msgstr "8"

#: ../../english/template/debian/ctime.wml:49
msgid "September"
msgstr "9"

#: ../../english/template/debian/ctime.wml:50
msgid "October"
msgstr "10"

#: ../../english/template/debian/ctime.wml:51
msgid "November"
msgstr "11"

#: ../../english/template/debian/ctime.wml:52
msgid "December"
msgstr "12"

#. $dateform: Date format (sprintf) for modification dates.
#. Available variables are: $mday = day-of-month, $monnr = month number,
#. $mon = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:60
msgid ""
"q{[%]s, [%]s [%]2d [%]02d:[%]02d:[%]02d [%]s [%]04d}, $wday, $mon, $mday, "
"$hour, $min, $sec, q{UTC}, 1900+$year"
msgstr ""
"q{[%]s 年 [%]s 月 [%]s 日 [%]s [%]02d:[%]02d:[%]02d [%]s}, 1900+$year, $mon, "
"$mday, $wday, $hour, $min, $sec, q{UTC}"

#. $newsdateform: Date format (sprintf) for news items.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:68
msgid "q{[%]02d [%]s [%]04d}, $mday, $mon_str, $year"
msgstr "q{[%]04d-[%]02d-[%]02d}, $year, $mon, $mday"

#. $spokendateform: Date format (sprintf) for "spoken" dates
#. (such as the current release date).
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:79
msgid "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"
msgstr "q{[%]d 年 [%]d 月 [%]d 日}, $year, $mon, $mday"

#. $spokendateform_noyear: Date format (sprintf) for "spoken" dates
#. (such as the current release date), without the year.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy).
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:90
msgid "q{[%]d [%]s}, $mday, $mon_str"
msgstr "q{[%]02d 月 [%]02d 日}, $mon, $mday"

#. $spokendateform_noday: Date format (sprintf) for "spoken" dates
#. (such a conference event), without the day.
#. Available variables are: $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:99
msgid "q{[%]s [%]s}, $mon_str, $year"
msgstr "q{[%]02d 年 [%]02d 月}, $year, $mon"

#. $rangeform_samemonth: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges within the same month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday = end
#. day-of-month, $smon = month number, $smon_str = month string (from @longmoy)
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:110
msgid "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"
msgstr "q{[%]02d 月 [%]02d 日 - [%]02d 日}, $smon, $sday, $eday"

#. $rangeform_severalmonths: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges spanning the end of a month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday, end
#. day-of-month, $smon = start month number, $emon = end month number,
#. $smon_str = start month string (from @longmoy), $emon_str = end month string
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:122
msgid "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"
msgstr ""
"q{[%]02d 月 [%]02d 日 - [%]02d 月 [%]02d 日}, $smon, $sday, $emon, $eday"
