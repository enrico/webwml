<define-tag pagetitle>Publicación do Debian 11 <q>bullseye</q></define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff" maintainer="Pablo <parodper@gmail.com>"


<p>Tras 2 anos, 1 mes e 9 días de traballo o proxecto
Debian está orgulloso de presentar a nova versión estable 11 (alcumada <q>bullseye</q>),
que terá asistencia técnica para os seguintes 5 anos grazas ao esforzo de tanto o
<a href="https://security-team.debian.org/">equipo de Seguridad de Debian</a> 
como do equipo de <a href="https://wiki.debian.org/LTS">Asistencia a Longo Prazo de Debian</a>.
</p>

<p>
Debian 11 <q>bullseye</q> inclúe varias aplicacións e ambientes de
escritorio. Inclúense, entre outros, os seguintes ambientes:
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>


<p>Esta versión contén máis de 11.294 paquetes novos, en total 59.551 paquetes.
Non se inclúen os máis de 9.519 paquetes que se marcaran coma <q>obsoletos</q> e
que foron eliminados. Actualizáronse 42.821 paquetes, e 5.434 quedaron sen modificar.
</p>

<p>
<q>bullseye</q>  é a nosa primeira versión que inclúe un núcleo Linux con
compatibilidade co formato exFAT, e úsao por omisión para montar discos exFAT.
Grazas a isto xa non é preciso usar a implementación exFAT do tipo
sistema-de-ficheiros-en-espazo-de-usuario incluída no paquete exfat-fuse. As
ferramentas para crear e comprobar os discos exFAT inclúense no paquete exfatprogs.
</p>

 
<p>
A maioría das impresoras modernas son capaces de imprimir sen controladores,
e de escanear sen precisar os controladores do fabricante, que non soen ser libres.

<q>bullseye</q> inclúe un novo paquete, ipp-usb, que usa o protocolo independente
IPP sobre USB, compatible con moitas impresoras modernas. Isto permite se poida usar
un dispositivo USB como se fora un dispositivo de rede. O sistema de fondo sen
controladores oficial de SANE é sane-escl, do paquete libsane1, que usa o protocolo
eSCL.
</p>

<p>
Systemd en <q>bullseye</q> activa por omisión o rexistro permanente, co almacenamento
volátil como respaldo implícito. Isto permite que os usuarios que non usen características
específicas poidan desinstalar os vellos daemons rexistradores, e pasen a usar só o
rexistro de systemd.
</p>

<p>
O equipo de Debian Med está a contribuír na loita contra a COVID-19,
empaquetando os programas para a investigación da secuencia do virus e
para a loita contra a pandemia coas ferramentas usadas en epidemioloxía.
O equipo continuará centrándose nas ferramentas usadas nos dous campos 
para o aprendizaxe automático. A colaboración con Control de Calidade e
Integración Continua é moi importante para obter resultados reproducibles,
que son a base da ciencia moderna.

A Mestura Debian Med ten un conxunto de aplicacións, que precisan ter un alto
rendemento, que xa se poden beneficiar de SIMD a Esgalla. Para instalar os paquetes
que mantén o equipo Debian Med instale os metapaquetes chamados med-*, que están
na versión 3.6.x.
</p>

<p>
A partir de agora moitos idiomas, como o chinés, o xaponés ou o coreano; terán un novo
método de entrada de Fcitx 5, que é o sucesor do famoso Fcitx4 en <q>buster</q>.
Esta nova versión ten maior compatibilidade cos engadidos de Wayland (o xestor de
pantallas por omisión).
</p>

<p>
Debian 11 <q>bullseye</q> actualizou moitos paquetes e programas (máis de
o 72% dos paquetes da versión anterior), por exemplo:
</p>
<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>series 5.10 do núcleo Linux</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>máis de 59.000 paquetes preparados, compilados a partir de
máis de 30.000 paquetes fonte.</li>

### Check numbers for 29,000 source packages -DN
</ul>

<p>
Esta grande selección de paquetes, e a tradicional extensa compatibilidade
con moitas arquitecturas, amosa como Debian se mantén fiel ao obxectivo
de ser <q>O Sistema Operativo Universal</q>. Debian é axeitado para usarse
de moitas formas: xa for coma sistema de escritorio, para portátiles, para
servidores de desenvolvemento, para sistemas múltiples; ou para servidores
de almacenamento, web ou bases de datos. Tamén os controis de calidade,
coma a instalación automática e as probas ao actualizar, realizados a todos
os paquetes do arquivo de Debian asegurase de que <q>bullseye</q> cumpre
as grandes expectativas que os usuarios poñen nas versións estables de Debian.
</p>

<p>
Debian é compatible con nove arquitecturas:
PC de 64 bits / Intel EM64T / x86-64 (<code>amd64</code>),
PC de 64 bits / Intel IA-32 (<code>i386</code>),
Motorola de 64 bits con extremidade menor/IBM PowerPC (<code>ppc64el</code>),
IBM S/390 de 64 bits (<code>s390x</code>),
para ARM, <code>armel</code>
e <code>armhf</code> para sistemas de 32 bits novos e vellos,
xunto con <code>arm64</code> para a arquitectura de 64 bits <q>AArch64</q>,
e para MIPS, arquitecturas <code>mipsel</code> (extremidade menor) para os sistemas de 32 bits
e <code>mips64el</code> para os sistemas de 64 bits con extremidade menor.
</p>

<h3>Quere probalo?</h3>
<p>
Se só quere probar Debian 11 <q>bullseye</q> sen instalalo pode
usar calquera das <a href="$(HOME)/CD/live/">imaxes en vivo («live»)</a>, que cargan e inician
o sistema operativo, en modo de só lectura, na memoria do ordenador.
</p>

<p>
Hai imaxes en vivo para as arquitecturas <code>amd64</code> e
<code>i386</code>, e hai versións para DVD, unidades USB e arranque
dende a rede. O usuario pode escoller entre os distintos ambientes de
escritorio: GNOME, KDE Plasma, LXDE, LXQt, MATE e Xfce.
Debian Live <q>bullseye</q> ten unha imaxe en vivo normalizada, polo que é posible
probar un sistema Debian básico, sen ningunha interface gráfica.
</p>

<p>
Se lle agrada o sistema operativo ten a opción de instalalo dende a imaxe
cara o disco duro do ordenador. A imaxe inclúe tanto o instalador independente
Calamares coma o Instalador propio de Debian.
Pode atopar máis información nas
<a href="$(HOME)/releases/bullseye/releasenotes">notas da versión</a> e na
sección <a href="$(HOME)/CD/live/">instalar imaxes en vivo</a>, no sitio
web de Debian.
</p>

<p>
Se quere instalar Debian 11 <q>bullseye</q> directamente no disco duro
do seu ordenador pode escoller entre unha grande variedade de métodos de
instalación, como Blu-ray, DVD, CD, unidades USB ou a través dunha conexión
de rede. Mediante esas imaxes pódense instalar unha variedade de aplicacións e
ambientes de escritorio &mdash; Cinnamon, GNOME, KDE Plasma Desktop, LXDE, LXQt,
MATE e Xfce &mdash;.
Ademais tamén hai discos CD <q>poliarquitectura</q>, que permiten instalar a
un conxunto de arquitecturas a partir do mesmo disco. Ou sempre queda a opción
de crear unha unidade USB iniciable
(consulte a <a href="$(HOME)/releases/bullseye/installmanual">Guia de Instalación</a> para máis detalles).
</p>

# Translators: some text taken from /devel/debian-installer/News/2021/20210802

<p>
Realizamos moitas modificacións no Instalador de Debian,
o que resultou nunha mellor compatibilidade co «hardware»,
e outras características.
</p>
<p>
Nalgúns casos unha instalación exitosa pode ter problemas coa pantalla,
ao reiniciar no sistema instalado. Para estes casos hai algúns
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">arranxos dispoñibles</a>
que poden permitir iniciar sesión de calquera forma.
Tamén hai un
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">método usando o isenkram</a>
que permite que os usuarios detecten e instalen o microcódigo necesario
nos seus sistemas, de maneira automática. Por suposto, débense contrastar
os pros e contras de usar esta ferramenta, pois é probable que teña que
instalar paquetes non libres.</p>

<p>
  Ademais disto, a
  <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">imaxe non libre do instalador que inclúe paquetes co microcódigo</a>
  foi mellorada para que poida anticipar o microcódigo necesario
  no sistema instalado (por exemplo, para as tarxetas gráficas de AMD
  ou Nvidia; ou para os sistemas de audio de Intel de última xeración).
</p>

<p>
Para os usuarios da nube Debian ofrece soporte técnico directo
para moitas das plataformas da nube máis importantes. As imaxes
oficiais de Debian son fáciles de obter dende o mercado de cada imaxe.
Debian tamén publica <a href="https://cloud.debian.org/images/openstack/current/">imaxes OpenStack precompiladas</a>
para as arquitecturas <code>amd64</code> e <code>arm64</code>,
xa listas para descargar e usar en nubes locais.
</p>

<p>
Debian está dispoñible en 76 idiomas, a maioría deles dispoñibles
tanto nas interfaces de texto como nas interfaces gráficas.
</p>

<p>
As imaxes de instalación pódense descargar dende
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (forma recomendada),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, ou
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; véxase
<a href="$(HOME)/CD/">Debian en CD</a> para máis información. <q>bullseye</q> estará
dispoñible en discos DVD, CD-ROM e Blu-Ray físicos dentro de pouco;
tamén se poderán obter mediante moitos <a href="$(HOME)/CD/vendors">vendedores</a>.
</p>


<h3>Actualizar Debian</h3>
<p>
As actualizacións a Debian 11 dende a versión anterior, Debian 10
(alcumada <q>buster</q>), son xestionadas automaticamente na maioría
de configuracións pola ferramenta de xestión de paquetes APT.
</p>

<p>
En bullseye a suite de seguridade chámase bullseye-security, e os usuarios
deberían adaptar os seus ficheiros coas fontes de APT, segundo for necesario
ao actualizar. Se configurou APT para conxelar paquetes ou usar <code>APT::Default-Release</code>
é probable que teña que mudar cousas. Consulte a sección
<a href="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive">Cambios na estrutura do arquivo de seguridade</a>
das notas da versión para máis información.
</p>

<p>
Se está a actualizar de forma remota teña en mente a sección
<a href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">Imposible conectarse mediante SSH durante a actualización</a>. 
</p>

<p>
Coma sempre os sistemas Debian deberían actualizarse sen problemas, no sitio,
sen ningún momento de pausa; pero recomendamos ler as <a href="$(HOME)/releases/bullseye/releasenotes">notas da versión</a>
para informarse dos posibles problemas, e por as instrucións detalladas sobre
como instalar e actualizar. Segundo pasen as semanas as notas da versión seguirán mellorándose
e traducíndose a outros idiomas.
</p>


<h2>Sobre Debian</h2>

<p>
Debian é un sistema operativo libre, desenvolto por
milleiros de voluntarios de todo o mundo, que colaboran mediante
o Internet. Os fundamento do proxecto Debian son os seus colaboradores,
a dedicación ao Contrato Social de Debian e o Software Libre; e o seu
compromiso a manter o mellor sistema operativo posible. Esta nova versión
é outro importante paso nesa dirección.
</p>


<h2>Información de Contacto</h2>

<p>
Para máis información visite as páxinas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a> ou envíe un correo electrónico a
&lt;press@debian.org&gt;.
</p>
