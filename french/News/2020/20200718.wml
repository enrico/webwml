#use wml::debian::translation-check translation="b51002b9cd5b7dbca0e39ddb2d17bcc495ead21a" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 9.13</define-tag>
<define-tag release_date>2020-07-18</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la treizième (et dernière) mise
à jour de sa distribution oldstable Debian <release> (nom de code <q><codename></q>). 
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version oldstable. Les annonces
de sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.</p>

<p>
Après cette version intermédiaire, les équipes de sécurité et de publication
de Debian ne produiront plus de mises à jour pour Debian 9. Les utilisateurs qui
souhaitent continuer à bénéficier du suivi de sécurité devraient mettre à niveau
vers Debian 10, ou consulter <url "https://wiki.debian.org/LTS"> pour avoir des
détails sur le sous-ensemble d'architectures et de paquets couverts par le projet
« Long Term Support »</p>

<p>
Veuillez noter que cette révision ne constitue pas une nouvelle version
Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
<q><codename></q>. Après l'installation, les paquets peuvent être mis
à niveau vers les versions actuelles à l'aide d'un miroir Debian à jour.</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
révision.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrections de bogues divers</h2>

<p>Cette révision d'oldstable ajoute quelques importantes corrections aux
paquets suivants :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction acmetool "Reconstruction avec une version récente de golang pour récupérer des corrections de sécurité">
<correction atril "dvi : atténuation d'attaques par injection de commande en protégeant le nom de fichier [CVE-2017-1000159] ; correction de vérifications de débordement dans le dorsal de tiff [CVE-2019-1010006] ; tiff : gestion d'échec issu de TIFFReadRGBAImageOriented [CVE-2019-11459]">
<correction bacula "Ajout du paquet de transition bacula-director-common, évitant la perte de /etc/bacula/bacula-dir.conf lors d'une purge ; fichiers PID rendus propriété du superutilisateur">
<correction base-files "Mise à jour de /etc/debian_version pour cette version">
<correction batik "Correction d'une falsification de requêtes côté serveur au moyen d'attributs xlink:href [CVE-2019-17566]">
<correction c-icap-modules "Prise en charge de ClamAV 0.102">
<correction ca-certificates "Mise à jour du paquet CA de Mozilla vers la version 2.40, mise en liste noire des racines Symantec qui ne sont plus de confiance et de <q>AddTrust External Root</q> expiré ; suppression des certificats validés seulement par courriel">
<correction chasquid "Reconstruction avec une version récente de golang pour récupérer des corrections de sécurité">
<correction checkstyle "Correction d'un problème d'injection d’entités externes XML [CVE-2019-9658 CVE-2019-10782]">
<correction clamav "Nouvelle version amont [CVE-2020-3123] ; corrections de sécurité [CVE-2020-3327 CVE-2020-3341]">
<correction compactheader "Nouvelle version amont, compatible avec les dernières versions de Thunderbird">
<correction cram "Échecs de test ignorés pour corriger des problèmes de construction">
<correction csync2 "Échec de la commande HELLO quand SSL est requis">
<correction cups "Correction d'un dépassement de tampon de tas [CVE-2020-3898] et de <q>la fonction ippReadIO peut lire hors limites un champ d'extension</q> [CVE-2019-8842]">
<correction dbus "Nouvelle version amont stable ; problème de déni de service évité [CVE-2020-12049] ; utilisation de mémoire après libération évitée si deux noms d'utilisateur partagent un UID">
<correction debian-installer "Mise à jour pour l'ABI du noyau 4.9.0-13">
<correction debian-installer-netboot-images "Reconstruction avec stretch-proposed-updates">
<correction debian-security-support "Mise à jour de l'état de la prise en charge de plusieurs paquets">
<correction erlang "Correction de l'utilisation de chiffrements TLS faibles [CVE-2020-12872]">
<correction exiv2 "Correction d'un problème de déni de service [CVE-2018-16336] ; correction d'un correctif trop restrictif pour CVE-2018-10958 et CVE-2018-10999">
<correction fex "Mise à jour de sécurité">
<correction file-roller "Correction de sécurité [CVE-2020-11736]">
<correction fwupd "Nouvelle version amont ; utilisation d'un CNAME pour rediriger vers le réseau de diffusion de contenu correct pour les métadonnées ; pas d'interruption de démarrage si le fichier de métadonnées XML n'est pas valable ; ajout des clés GPG publiques de la Fondation Linux pour les microprogrammes et les métadonnées ; limite de taille des métadonnées relevé à 10 Mo">
<correction glib-networking "Renvoi d'une erreur de mauvaise identité si l'identité n'est pas configurée [CVE-2020-13645]">
<correction gnutls28 "Correction d'un problème de corruption de mémoire [CVE-2019-3829] ; correction de fuite de mémoire ; ajout de la prise en charge des tickets de session de longueur nulle, correction d'erreurs de connexion de sessions TLS1.2 vers certains fournisseurs d'hébergement">
<correction gosa "Vérification renforcée des réussites et échecs de LDAP [CVE-2019-11187] ; correction de compatibilité avec les dernières versions de PHP ; rétroportage de plusieurs autres correctifs ; remplacement de (un)serialize par json_encode/json_decode pour atténuer une injection d'objet PHP [CVE-2019-14466]">
<correction heartbleeder "Reconstruction avec une version récente de golang pour récupérer des corrections de sécurité">
<correction intel-microcode "Retour à des versions publiées précédemment de certains microcodes contournant des arrêts de l'initialisation sur Skylake-U/Y et Skylake Xeon E3">
<correction iptables-persistent "Pas d'échec en cas d'échec de modprobe">
<correction jackson-databind "Correction de multiples problèmes de sécurité affectant BeanDeserializerFactory [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 and CVE-2019-17267]">
<correction libbusiness-hours-perl "Utilisation explicite d'années à quatre chiffres, corrigeant des problèmes de construction et d'utilisation">
<correction libclamunrar "Nouvelle version amont stable ; ajout d'un méta-paquet non versionné">
<correction libdbi "Appel _error_handler() à nouveau commenté, corrigeant des problèmes avec les utilisateurs">
<correction libembperl-perl "Gestion des pages d'erreur d'Apache &gt;= 2.4.40">
<correction libexif "Corrections de sécurité [CVE-2016-6328 CVE-2017-7544 CVE-2018-20030 CVE-2020-12767 CVE-2020-0093] ; corrections de sécurité [CVE-2020-13112 CVE-2020-13113 CVE-2020-13114] ; correction d'un dépassement de tampon en lecture [CVE-2020-0182] et d'un dépassement d'entier non signé [CVE-2020-0198]">
<correction libvncserver "Correction d'un dépassement de tas [CVE-2019-15690]">
<correction linux "Nouvelle version amont stable ; mise à jour de l'ABI vers 4.9.0-13">
<correction linux-latest "Mise à jour pour l'ABI du noyau 4.9.0-13">
<correction mariadb-10.1 "Nouvelle version amont stable ; corrections de sécurité [CVE-2020-2752 CVE-2020-2812 CVE-2020-2814]">
<correction megatools "Ajout de la prise en charge du nouveau format de liens mega.nz">
<correction mod-gnutls "Suites de chiffrement obsolètes évitées dans l'ensemble de tests ; correction des échecs de tests quand ils sont combinés à des corrections d'Apache pour le CVE-2019-10092">
<correction mongo-tools "Reconstruction avec une version récente de golang pour récupérer des corrections de sécurité">
<correction neon27 "Traitement des échecs de tests liés à OpenSSL comme non fatals">
<correction nfs-utils "Correction d'une possible vulnérabilité d'écrasement de fichier [CVE-2019-3689] ; la totalité de /var/lib/nfs n'est plus rendue propriété de l'utilisateur de statd">
<correction nginx "Correction d'une vulnérabilité de dissimulation de requête de la page d'erreur [CVE-2019-20372]">
<correction node-url-parse "Nettoyage des chemins et des hôtes avant l'analyse [CVE-2018-3774]">
<correction nvidia-graphics-drivers "Nouvelle version amont stable ; corrections de sécurité [CVE-2020-5963 CVE-2020-5967]">
<correction pcl "Correction de dépendance manquante à libvtk6-qt-dev">
<correction perl "Correction de multiples problèmes de sécurité liés aux expressions rationnelles [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Correction d'une vulnérabilité de script intersite [CVE-2020-8035]">
<correction php-horde-data "Correction d'une vulnérabilité d'exécution de code authentifié à distance [CVE-2020-8518]">
<correction php-horde-form "Correction d'une vulnérabilité d'exécution de code authentifié à distance [CVE-2020-8866]">
<correction php-horde-gollem "Correction d'une vulnérabilité de script intersite dans la sortie de breadcrumb [CVE-2020-8034]">
<correction php-horde-trean "Correction d'une vulnérabilité d'exécution de code authentifié à distance [CVE-2020-8865]">
<correction phpmyadmin "Plusieurs corrections de sécurité [CVE-2018-19968 CVE-2018-19970 CVE-2018-7260 CVE-2019-11768 CVE-2019-12616 CVE-2019-6798 CVE-2019-6799 CVE-2020-10802 CVE-2020-10803 CVE-2020-10804 CVE-2020-5504]">
<correction postfix "Nouvelle version amont stable">
<correction proftpd-dfsg "Correction de gestion de paquets SSH_MSG_IGNORE">
<correction python-icalendar "Correction de dépendances à Python3">
<correction rails "Correction d'une vulnérabilité de script intersite possible au moyen de l'assistant d'échappement de Javascript [CVE-2020-5267]">
<correction rake "Correction d'une vulnérabilité d'injection de commande [CVE-2020-8130]">
<correction roundcube "Correction d'un problème de vulnérabilité de script intersite au moyen de messages HTML  malveillants avec un élément svg dans l'espace de noms [CVE-2020-15562]">
<correction ruby-json "Correction d'une vulnérabilité de création d'objet non sûr [CVE-2020-10663]">
<correction ruby2.3 "Correction d'une vulnérabilité de création d'objet non sûr [CVE-2020-10663]">
<correction sendmail "Correction du résultat du processus de contrôle d'exécution de file d'attente en mode <q>split daemon</q>, <q>NOQUEUE: connect from (null)</q>, suppression d'échec lors de l'utilisation de BTRFS">
<correction sogo-connector "Nouvelle version amont, compatible avec les dernières versions de Thunderbird">
<correction ssvnc "Correction d'écriture hors limites [CVE-2018-20020], de boucle infinie [CVE-2018-20021], d'initialisation incorrecte [CVE-2018-20022], d'un potentiel déni de service [CVE-2018-20024]">
<correction storebackup "Correction d'une possible vulnérabilité d'élévation de privilèges [CVE-2020-7040]">
<correction swt-gtk "Correction de dépendance manquante à libwebkitgtk-1.0-0">
<correction tinyproxy "Création de fichier PID avant l'abaissement des privilèges d'un compte non administrateur [CVE-2017-11747]">
<correction tzdata "Nouvelle version amont stable">
<correction websockify "Correction de dépendance manquante à python{3,}-pkg-resources">
<correction wpa "Correction de contournement de protection de déconnexion PMF de mode AP [CVE-2019-16275] ; correction de problèmes de randomisation de MAC avec certaines cartes">
<correction xdg-utils "Nettoyage du nom de fenêtre avant de l'envoyer sur D-Bus ; gestion correcte des répertoires dont les noms contiennent des espaces ; création du répertoire <q>applications</q> si nécessaire">
<correction xml-security-c "Correction du calcul de longueur dans la méthode concat">
<correction xtrlock "Correction du blocage de certains périphériques tactiles multipoints lors du verrouillage [CVE-2016-10894]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
oldstable. L'équipe de sécurité a déjà publié une annonce pour chacune de
ces mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2017 4005 openjfx>
<dsa 2018 4255 ant>
<dsa 2018 4352 chromium-browser>
<dsa 2019 4379 golang-1.7>
<dsa 2019 4380 golang-1.8>
<dsa 2019 4395 chromium>
<dsa 2019 4421 chromium>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4621 openjdk-8>
<dsa 2020 4622 postgresql-9.6>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4628 php7.0>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4642 thunderbird>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4666 openldap>
<dsa 2020 4668 openjdk-8>
<dsa 2020 4670 tiff>
<dsa 2020 4671 vlc>
<dsa 2020 4673 tomcat8>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4683 thunderbird>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4692 netqmail>
<dsa 2020 4693 drupal7>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4698 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4703 mysql-connector-java>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4706 drupal7>
<dsa 2020 4707 mutt>
<dsa 2020 4711 coturn>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4715 imagemagick>
<dsa 2020 4717 php7.0>
<dsa 2020 4718 thunderbird>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>


<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>

<correction certificatepatrol "Incompatible avec les versions actuelles de Firefox ESR">
<correction colorediffs-extension "Incompatible avec les versions actuelles de Thunderbird">
<correction dynalogin "Dépend de simpleid qui doit être supprimé">
<correction enigmail "Incompatible avec les versions actuelles de Thunderbird">
<correction firefox-esr "[armel] plus pris en charge (requiert nodejs)">
<correction firefox-esr "[mips mipsel mips64el] plus pris en charge (nécessite la dernière version de rustc)">
<correction getlive "Cassé du fait de modifications de Hotmail">
<correction gplaycli "Cassé par les modifications de l'API de Google">
<correction kerneloops "Service amont plus disponible">
<correction libmicrodns "Problèmes de sécurité">
<correction libperlspeak-perl "Problèmes de sécurité ; non maintenu">
<correction mathematica-fonts "Repose sur un emplacement de téléchargement non disponible">
<correction pdns-recursor "Problèmes de sécurité ; non pris en charge">
<correction predictprotein "Dépend de profphd qui doit être supprimé">
<correction profphd "Inutilisable">
<correction quotecolors "Incompatible avec les versions actuelles de Thunderbird">
<correction selenium-firefoxdriver "Incompatible avec les versions actuelles de Firefox ESR">
<correction simpleid "Ne fonctionne pas avec PHP7">
<correction simpleid-ldap "Dépend de simpleid qui doit être supprimé">
<correction torbirdy "Incompatible avec les versions actuelles de Thunderbird">
<correction weboob "Non maintenu ; déjà supprimé des dernières versions">
<correction yahoo2mbox "Cassé depuis plusieurs années">

</table>

<h2>Installateur Debian</h2>

L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de oldstable.

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>


<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution oldstable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>
Mises à jour proposées à la distribution oldstable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>
Informations sur la distribution oldstable (notes de publication, <i>errata</i>,
etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.</p>


<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de Debian <a
href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier électronique
à &lt;press@debian.org&gt; ou contactez l'équipe de publication de la version
stable à &lt;debian-release@lists.debian.org&gt;.
</p>
