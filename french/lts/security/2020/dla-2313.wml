#use wml::debian::translation-check translation="863c9f0e40c5faedad87947f49c937202ced075a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité d’élévation des privilèges a été découverte dans <a
href="http://www.net-snmp.org/">Net-SNMP</a>, un ensemble d’outils pour
collecter et structurer l’information à propos des périphériques sur des réseaux
d’ordinateurs, due au traitement incorrect de liens symboliques
(<tt>CVE-2020-15861</tt>).</p>

<p>Cette mise à jour de sécurité applique un correctif de l’amont pour leur
traitement précédent du <tt>CVE-2020-15862</tt> dans le cadre de la
<tt>DLA-2299-1</tt>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15861">CVE-2020-15861</a>

<p>Élévation des privilèges due à la gestion de liens symboliques.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15862">CVE-2020-15862</a>

<p>Élévation des privilèges.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 5.7.3+dfsg-1.7+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets net-snmp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2313.data"
# $Id: $
