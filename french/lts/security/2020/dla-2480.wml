#use wml::debian::translation-check translation="48495fee44497bc4ab28fa2e43d90b963c382c10" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans salt.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16846">CVE-2020-16846</a>

<p>Un utilisateur non authentifié avec accès réseau à l’API de Salt peut utiliser des
injections d’interpréteur de commandes pour exécuter du code sur l’API de Salt en
utilisant le client SSH.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17490">CVE-2020-17490</a>

<p>Lors de l’utilisation des fonctions create_ca, create_csr et
create_self_signed_cert dans le module d’exécution tls, il n’est pas sûr que
la clé soit créée avec les permissions correctes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25592">CVE-2020-25592</a>

<p>Validation adéquate des accréditations et jetons eauth en accord avec leurs
listes de contrôle d’accès (ACL). Avant cette modification, eauth n’était pas
validé correctement lors de l’appel à Salt par SSH à l’aide de salt-api. N’importe
quelle valeur de <q>eauth</q> ou <q>token</q> permet à l’utilisateur de
contourner l’authentification et de réaliser des appels à Salt par SSH</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2016.11.2+ds-1+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets salt.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de salt, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/salt">https://security-tracker.debian.org/tracker/salt</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2480.data"
# $Id: $
