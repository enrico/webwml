#use wml::debian::translation-check translation="0fe5ca4223c771f1964eb5c85cd8bfbd129673cb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La communauté de lemonldap-ng a corrigé une vulnérabilité dans les fichiers
de configuration par défaut de Nginx
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-24660">CVE-2020-24660</a>).
Les paquets Debian n’installent aucun site par défaut, mais la documentation
fournit des exemples non sûrs de configuration de Nginx avant cette version.
En cas d’utilisation d’un gestionnaire lemonldap-ng avec Nginx, une
vérification des fichiers de configuration s’impose.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.9.7-3+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lemonldap-ng.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lemonldap-ng, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/lemonldap-ng">https://security-tracker.debian.org/tracker/lemonldap-ng</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2367.data"
# $Id: $
