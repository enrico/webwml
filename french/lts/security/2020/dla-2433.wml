#use wml::debian::translation-check translation="85813c9439ae86f8ffa851bb7251c43d1050db26" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans la bibliothèque de
chiffrement bouncycastle par lequel des attaquants pouvaient obtenir des
informations sensibles à cause de différences perceptibles dans sa réponse
à une entrée non valable.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26939">CVE-2020-26939</a>

<p>Dans <q>Legion of the Bouncy Castle BC</q> avant la version 1.55 et BC-FJA
avant la version 1.0.1.2, des attaquants pouvaient obtenir des informations
sensibles à propos d’un exposant privé à cause d’une différence perceptible
dans le comportement lors d’entrées erronées. Cela se produit dans
org.bouncycastle.crypto.encodings.OAEPEncoding. L’envoi d’un texte chiffré non
valable se déchiffrant en des données utiles brèves dans le décodeur OAEP
pourrait aboutir à l’envoi d’une exception précoce, divulguant
éventuellement quelques informations sur l’exposant privé de la clé RSA privée
réalisant le chiffrement.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.56-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bouncycastle.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2433.data"
# $Id: $
