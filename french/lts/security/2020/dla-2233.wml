#use wml::debian::translation-check translation="0f81c1fd3e5f3051ec10c7b021d80edc73ce94d8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait deux problèmes dans
<a href="https://www.djangoproject.com/">Django</a>, le cadriciel de
développement web en Python :</p>

<ul>
<li>
<a href="https://security-tracker.debian.org/tracker/CVE-2020-13254">
CVE-2020-13254 : divulgation potentielle de données à l’aide de clés memcached
mal formées.</a>

<p>Dans le cas où un dorsal memcached ne réalise pas une validation de clé,
le passage de clés mal formées en cache pourrait aboutir à une collision de clés
et une divulgation potentielle de données. Pour éviter cela, une validation de
clé a été ajoutée dans les dorsaux memcached de cache.</p>
</li>
<li>
<a href="https://security-tracker.debian.org/tracker/CVE-2020-13596">
CVE-2020-13596 : XSS possible à l'aide d'un ForeignKeyRawIdWidget
d’administration.
</a>

<p>Les paramètres de requête pour le ForeignKeyRawIdWidget d’administration
n’étaient pas proprement encodés pour l’URL, constituant un vecteur d’attaque
XSS. ForeignKeyRawIdWidget assure désormais que les paramètres de requête soient
correctement encodés pour l’URL.</p>
</li>
</ul>

<p>Pour plus d’informations, veuillez consulter
<a href="https://www.djangoproject.com/weblog/2020/jun/03/security-releases/">l’annonce
de l’amont</a>.</p>

<p>Cette publication corrige aussi des échecs de test introduits dans
<tt>1.7.11-1+deb8u3</tt> et <tt>1.7.11-1+deb8u8</tt> par les correctifs
respectifs pour
<a href="https://security-tracker.debian.org/tracker/CVE-2019-19844">CVE-2018-7537</a>
et <a href="https://security-tracker.debian.org/tracker/CVE-2019-19844">CVE-2019-19844</a>.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.7.11-1+deb8u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2233.data"
# $Id: $
