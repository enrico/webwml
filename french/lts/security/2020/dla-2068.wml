#use wml::debian::translation-check translation="a1eba72230582a5d199c4ab12c5748e6b73fe25d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-2215">CVE-2019-2215</a>

<p>L’outil syzkaller a découvert une vulnérabilité d’utilisation de
mémoire après libération dans le pilote binder d’Android. Un utilisateur local
sur un système avec ce pilote activé pourrait utiliser cela pour provoquer
un déni de service (corruption de mémoire ou plantage) ou éventuellement pour
une élévation des privilèges. Cependant, ce pilote n’est pas activé dans les
paquets de noyaux de Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10220">CVE-2019-10220</a>

<p>Divers développeurs et chercheurs ont trouvé que si un système de fichiers
contrefait ou un serveur de fichiers malveillant fournissait un répertoire avec
des noms de fichier incluant des caractères « / », cela pourrait dérouter et
éventuellement contrecarrer les vérifications de sécurité dans des applications
lisant le répertoire.</p>

<p>Le noyau renverra désormais une erreur lors de la lecture d’un tel répertoire,
plutôt que de passer des noms de fichier non valables dans l’espace utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14895">CVE-2019-14895</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-14901">CVE-2019-14901</a>

<p>ADLab de Venustech a découvert des dépassements potentiels de tampon de tas
dans le pilote wifi mwifiex. Sur des systèmes utilisant ce pilote, un point
d’accès sans fil malveillant ou un pair adhoc/P2P pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14896">CVE-2019-14896</a>,
 <a href="https://security-tracker.debian.org/tracker/CVE-2019-14897">CVE-2019-14897</a>

<p>ADLab de Venustech des dépassements potentiels de tampons de tas et de pile
dans le pilote wifi libertas. Sur des systèmes utilisant ce pilote, un point
d’accès sans fil malveillant ou un pair adhoc/P2P pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15098">CVE-2019-15098</a>

<p>Hui Peng et Mathias Payer ont signalé que le pilote wifi ath6kl ne validait
pas correctement les descripteurs USB, ce qui pourrait conduire à un
déréférencement de pointeur NULL. Un attaquant capable d’ajouter des
périphériques USB pourrait utiliser cela afin de provoquer un déni de service
(bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15217">CVE-2019-15217</a>

<p>L’outil syzkaller a découvert que le pilote de média zr364xx ne gérait
pas correctement les périphériques sans chaîne de nom de fabricant, ce qui
pourrait conduire à un déréférencement de pointeur NULL. Un attaquant capable
d’ajouter des périphériques USB pourrait utiliser cela afin de provoquer un déni
de service (bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15291">CVE-2019-15291</a>

<p>L’outil syzkaller a découvert que le pilote de média b2c2-flexcop-usb ne
validait pas correctement les descripteurs USB, ce qui pourrait conduire à un
déréférencement de pointeur NULL. Un attaquant capable d’ajouter des
périphériques USB pourrait utiliser cela afin de provoquer un déni de service
(bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15505">CVE-2019-15505</a>

<p>L’outil syzkaller a découvert que le pilote de média technisat-usb2 ne
validait pas correctement les paquets IR entrants, ce qui pourrait conduire
à une lecture excessive de tampon de tas. Un attaquant capable d’ajouter des
périphériques USB pourrait utiliser cela afin de provoquer un déni de service
(bogue/oops) ou pour lire des informations sensibles de la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16746">CVE-2019-16746</a>

<p>Il a été découvert que la pile wifi ne validait pas le contenu des en-têtes
de balise de proximité fournies par l’espace utilisateur pour être utilisées sur
une interface wifi dans le mode « point d'accès », ce qui pourrait conduire à un
dépassement de tampon de tas. Un utilisateur local ayant le droit de configurer
une interface wifi pourrait utiliser cela afin de provoquer un déni de service
(corruption de mémoire ou plantage) ou, éventuellement, pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17052">CVE-2019-17052</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-17053">CVE-2019-17053</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-17054">CVE-2019-17054</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-17055">CVE-2019-17055</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-17056">CVE-2019-17056</a>

<p>Ori Nimron a signalé que diverses implémentations de protocole réseau — AX.25,
IEEE 802.15.4, Appletalk, ISDN et NFC — permettaient à tous les utilisateurs de
créer des sockets bruts. Un utilisateur local pourrait utiliser cela pour
envoyer des paquets arbitraires sur des réseaux utilisant ces protocoles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17133">CVE-2019-17133</a>

<p>Nicholas Waisman a signalé que la pile wifi ne validait pas les informations
SSID reçues avant de les copier, ce qui pourrait conduire à un dépassement de
tampon si cela n’était pas validé par le pilote ou le micrologiciel. Un point
d’accès sans fil malveillant peut être capable d’utiliser cela afin de provoquer
un déni de service (corruption de mémoire ou plantage) ou pour une exécution de
code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17666">CVE-2019-17666</a>

<p>Nicholas Waisman a signalé que les pilotes wifi rtlwifi ne validaient pas
correctement les informations P2P reçues, conduisant à un dépassement de tampon.
Un pair P2P malveillant pourrait utiliser cela afin de provoquer un déni de
service (corruption de mémoire ou plantage) ou pour une exécution de code
à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19051">CVE-2019-19051</a>

<p>Navid Emamdoost a découvert une fuite potentielle de mémoire dans le pilote
wimax i2400m si l’opération logicielle rfkill échoue. L’impact de sécurité n’est
pas défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19052">CVE-2019-19052</a>

<p>Navid Emamdoost a découvert une fuite potentielle de mémoire dans le pilote
CAN  gs_usb si l’opération open (interface-up) échoue. L’impact de sécurité
n’est pas défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19056">CVE-2019-19056</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19057">CVE-2019-19057</a>

<p>Navid Emamdoosta découvert une fuite potentielle de mémoire dans le pilote
wifi mwifiex si l’opération probe échoue. L’impact de sécurité n’est pas défini.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19062">CVE-2019-19062</a>

<p>Navid Emamdoost a découvert une fuite potentielle de mémoire dans le
sous-système AF_ALG si l’opération CRYPTO_MSG_GETALG échoue. Un utilisateur
local pourrait éventuellement utiliser cela afin de provoquer un déni de service
(épuisement de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19066">CVE-2019-19066</a>

<p>Navid Emamdoosta découvert une fuite potentielle de mémoire dans le pilote
SCSI bfa si l’opération get_fc_host_stats échoue. L’impact de sécurité n’est pas
défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19227">CVE-2019-19227</a>

<p>Dan Carpenter a signalé des vérifications manquantes dans l’implémentation du
protocole Appletalk, ce qui pourrait conduire à un déréférencement de pointeur
NULL. L’impact de sécurité n’est pas défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19332">CVE-2019-19332</a>

<p>L’outil syzkaller a découvert une vérification manquante de limites dans
l’implémentation de KVM pour x86, ce qui pourrait conduire à un dépassement de
tampon de tas. Un utilisateur local autorisé à utiliser KVM pourrait utiliser
cela afin de provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19523">CVE-2019-19523</a>

<p>L’outil syzkaller a découvert un bogue d’utilisation de mémoire après
libération dans le pilote USB adutux. Un attaquant capable d’ajouter ou de
retirer des périphériques USB pourrait utiliser cela afin de provoquer un déni
de service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19524">CVE-2019-19524</a>

<p>L’outil syzkaller a découvert une situation de compétition dans la
bibliothèque ff-memless utilisée par les pilotes d’entrée. Un attaquant capable
d’ajouter ou de retirer des périphériques USB pourrait utiliser cela afin de
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19527">CVE-2019-19527</a>

<p>L’outil syzkaller a découvert que le pilote hiddev ne gérait pas les
situations de compétition correctement entre une tâche ouvrant le périphérique
et la déconnexion du matériel sous-jacent. Un utilisateur local autorisé
à accéder aux périphériques hiddev et capable d’ajouter ou retirer des
périphériques USB, pourrait utiliser cela afin de provoquer un déni de service
(corruption de mémoire ou plantage) ou, éventuellement, pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19530">CVE-2019-19530</a>

<p>L’outil syzkaller a découvert une utilisation potentielle de mémoire après
libération dans le pilote de réseau cdc-acm. Un attaquant capable d’ajouter des
périphériques USB pourrait utiliser cela afin de provoquer un déni de service
(corruption de mémoire ou plantage) ou, éventuellement, pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19531">CVE-2019-19531</a>

<p>L’outil syzkaller a découvert un bogue d’utilisation de mémoire après
libération dans le pilote USB yurex. Un attaquant capable d’ajouter ou retirer
des périphériques USB pourrait utiliser cela afin de provoquer un déni de
service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19532">CVE-2019-19532</a>

<p>L’outil syzkaller a découvert un dépassement potentiel de tampon de tas dans
pilote d’entrée hid-gaff et qui a été aussi trouvé dans beaucoup d’autres pilotes
d’entrée. Un attaquant capable d’ajouter des périphériques USB pourrait utiliser
cela afin de provoquer un déni de service (corruption de mémoire ou plantage)
ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19533">CVE-2019-19533</a>

<p>L’outil syzkaller a découvert que le pilote de média ttusb-dec manquait d’une
initialisation de structure, ce qui pourrait divulguer des informations
sensibles de la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19534">CVE-2019-19534</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19536">CVE-2019-19536</a>

<p>L’outil syzkaller a découvert que le pilote CAN peak_usb manquait d’une
initialisation de quelques structures, ce qui pourrait divulguer des
informations sensibles de la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19537">CVE-2019-19537</a>

<p>L’outil syzkaller a découvert des situations de compétition dans la pile USB,
impliquant des enregistrements de périphérique caractère. Un attaquant capable
d’ajouter des périphériques USB pourrait utiliser cela afin de provoquer un déni
de service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19767">CVE-2019-19767</a>

<p>L’outil syzkaller a découvert que des volumes ext4 contrefaits pourraient
déclencher un dépassement de tampon dans le pilote de système de fichiers ext4.
Un attaquant capable de monter de tels volumes pourrait utiliser cela afin de
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19922">CVE-2019-19922</a>

<p>Il a été découvert qu’une modification dans Linux 3.16.61, « sched/fair: Fix
bandwidth timer clock drift condition », pourrait conduire à des tâches
ralenties avant d’utiliser leur quota total de temps CPU. Un utilisateur local
pourrait utiliser cela pour ralentir d’autres tâches d’utilisateur. Cette
modification a été abandonnée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19947">CVE-2019-19947</a>

<p>Il a été découvert que le pilote CAN kvaser_usb manquait d’une initialisation
de quelques structures, ce qui pourrait divulguer des informations sensibles de
la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19965">CVE-2019-19965</a>

<p>Gao Chuan a signalé une situation de compétition dans la bibliothèque libsas
utilisée par les pilotes SCSI d’hôte, ce qui pourrait conduire à un
déréférencement de pointeur NULL. Un attaquant capable d’ajouter ou de retirer
des périphériques SCSI pourrait utiliser cela afin de provoquer un déni de
service (bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19966">CVE-2019-19966</a>

<p>L’outil syzkaller a découvert une vérification d’erreur manquante dans le
pilote de média cpia2, ce qui pourrait conduire à une utilisation de mémoire
après libération. Un attaquant capable d’ajouter des périphériques USB pourrait
utiliser cela afin de provoquer un déni de service (corruption de mémoire ou
plantage) ou, éventuellement, pour une élévation des privilèges.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.81-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2068.data"
# $Id: $
