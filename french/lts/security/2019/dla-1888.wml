#use wml::debian::translation-check translation="cec8a2da6088b6132390fa8a5f7f0359bc61d004" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans imagemagick, une boîte
à outils de traitement d’image.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12974">CVE-2019-12974</a>

<p>Déréférencement de pointeur NULL dans ReadPANGOImage et ReadVIDImage
(coders/pango.c et coders/vid.c). Cette vulnérabilité peut être exploitée par
des attaquants distants pour provoquer un déni de service à l’aide de données
d’image contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13135">CVE-2019-13135</a>

<p>Plusieurs utilisations de valeurs non initialisées dans ReadCUTImage,
UnpackWPG2Raster et UnpackWPGRaster (coders/wpg.c et coders/cut.c). Ces
vulnérabilités peuvent être exploitées par des attaquants distants pour
provoquer un déni de service, une divulgation ou modification non permise
d’informations à l’aide de données d’image contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13295">CVE-2019-13295</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-13297">CVE-2019-13297</a>

<p>Plusieurs lectures excessives de tampon de tas dans AdaptiveThresholdImage
(magick/threshold.c). Ces vulnérabilités peuvent être exploitées par des
attaquants distants pour provoquer un déni de service, une divulgation ou
modification non permise d’informations à l’aide de données d’image
contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13304">CVE-2019-13304</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-13305">CVE-2019-13305</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-13306">CVE-2019-13306</a>

<p>Plusieurs dépassements de tampons de pile dans WritePNMImage (coders/pnm.c),
conduisant à un dépassement d’écriture pouvant atteindre dix octets. Des
attaquants distants peuvent exploiter ces défauts pour éventuellement réaliser
une exécution de code ou un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 8:6.8.9.9-5+deb8u17.</p>
<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1888.data"
# $Id: $
