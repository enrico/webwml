#use wml::debian::translation-check translation="b6d67dfd98c2bb2090e6c83a25d3f8b51bba0c63" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>La bibliothèque urllib de Python fournit la prise en charge d'un second,
et pas très connu, schéma d’URL pour accéder aux fichiers locaux (« local_file:// »).
Ce schéma peut être utilisé pour contourner les protections qui essaient de
bloquer l’accès aux fichiers locaux et qui interdisent uniquement le schéma bien
connu « file:// ». Cette mise à jour corrige cette vulnérabilité en interdisant le
schéma « local_file:// ».</p>

<p>Cette mise à jour corrige aussi une autre régression introduite dans la
mise à jour publiée sous DLA-1835-1 qui brisait l’installation de
libpython3.4-testsuite.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 3.4.2-1+deb8u5.</p>
<p>Nous vous recommandons de mettre à jour vos paquets python3.4.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python3.4, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python3.4">https://security-tracker.debian.org/tracker/python3.4</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1852.data"
# $Id: $
