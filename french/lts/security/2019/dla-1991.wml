#use wml::debian::translation-check translation="a35381f341191ee2176be8d14bb43d1c845b23f6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Dans libssh2, la logique SSH_MSG_DISCONNECT dans packet.c possède un
dépassement d'entier dans les vérifications de limites, permettant à un
attaquant de spécifier un décalage arbitraire (hors limites) pour une lecture
ultérieure en mémoire. Un serveur SSH contrefait peut alors divulguer des
informations sensibles ou causer une condition de déni de service sur le système
client quand un utilisateur est connecté au serveur.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1.4.3-4.1+deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libssh2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1991.data"
# $Id: $
