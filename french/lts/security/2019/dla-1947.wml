#use wml::debian::translation-check translation="38ba14679e7eab814a17ca371d4279374f9e17a2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans LibreOffice, la suite
bureautique.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9848">CVE-2019-9848</a>

<p>Nils Emmerich a découvert que des documents malveillants pourraient exécuter
du code Python arbitraire grâce à LibreLogo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9849">CVE-2019-9849</a>

<p>Matei Badanoiu a découvert que le mode prudent ne s'appliquait pas aux puces
graphiques.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9850">CVE-2019-9850</a>

<p>Les protections mises en œuvre dans
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9848">CVE-2019-9848</a>
pourraient être contournées à cause d’une validation insuffisante d’URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9851">CVE-2019-9851</a>

<p>Gabriel Masei a découvert que des documents malveillants pourraient exécuter
des scripts arbitraires préinstallés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9852">CVE-2019-9852</a>

<p>Nils Emmerich a découvert que la protection mise en œuvre pour corriger
<a href="https://security-tracker.debian.org/tracker/CVE-2018-16858">CVE-2018-16858</a>
pourrait être contournée par une attaque d’encodage d’URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9853">CVE-2019-9853</a>

<p>Nils Emmerich a découvert que des document malveillants pourraient contourner
des réglages de sécurité de document pour exécuter des macros contenues dans le
document.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9854">CVE-2019-9854</a>

<p>La mise en œuvre pour corriger
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9852">CVE-2019-9852</a>
pourrait être contournée à cause d’une vérification manquante d'entrée.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:4.3.3-2+deb8u13.</p>
<p>Nous vous recommandons de mettre à jour vos paquets libreoffice.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1947.data"
# $Id: $
