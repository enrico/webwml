#use wml::debian::translation-check translation="afd5e92340d2ead7961ca440181e447460d05f15" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été trouvées dans icedtea-web,
une implémentation de JNLP (Java Network Launching Protocol).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10181">CVE-2019-10181</a>

<p>Dans icedtea-web, du code exécutable pourrait être injecté dans un fichier
JAR sans compromettre la vérification de signature. Un attaquant pourrait
utiliser ce défaut pour injecter du code dans un JAR de confiance. Le code
serait exécuté à l’intérieur du bac à sable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10182">CVE-2019-10182</a>

<p>Icedtea-web ne vérifiait pas correctement les chemins des éléments
&lt;jar/&gt; dans les fichiers JNLP. Un attaquant pourrait forcer une victime
pour exécuter une application contrefaite pour l'occasion et utiliser ce défaut
pour téléverser des fichiers arbitraires dans des emplacements arbitraires dans
l’environnement de l’utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10185">CVE-2019-10185</a>

<p>Icedtea-web était vulnérable à une attaque <q>zip-slip</q> pendant
l’auto-extraction de fichier JAR. Un attaquant pourrait utiliser ce défaut pour
écrire des fichiers dans des emplacements arbitraires. Cela pourrait aussi être
utilisé pour remplacer l’application principale en cours d’exécution et,
éventuellement, s’échapper du bac à sable.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.5.3-1+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets icedtea-web.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1914.data"
# $Id: $
