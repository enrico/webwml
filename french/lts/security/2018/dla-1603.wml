#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été trouvés dans suricata, un outil de détection
d’intrusion et de prévention.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7177">CVE-2017-7177</a>

<p>Suricata a un problème de contournement de défragmentation IPv4, causé par un
manque de vérification pour le protocole IP lors de la mise en correspondance de
fragments.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15377">CVE-2017-15377</a>

<p>Il était possible de déclencher un tas de vérifications redondantes sur le
contenu d’un trafic réseau contrefait avec une certaine signature, à cause de
DetectEngineContentInspection dans detect-engine-content-inspection.c. Le moteur
de recherche ne stoppe pas quand il devrait lorsqu’aucune correspondance n’est
trouvée. À la place, il stoppe seulement lorsque la limite
d’inspection-récursion est atteinte (3000 par défaut).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6794">CVE-2018-6794</a>

<p>Suricata est prédisposé à une vulnérabilité de contournement de détection
HTTP dans detect.c et stream-tcp.c. Si un serveur malveillant casse un flux TCP
normal et envoie des données avant que l’initialisation de connexion (3-way) soit
terminée, alors les données envoyées par le serveur malveillant seront acceptées
par les clients web tels qu’un navigateur web ou des utilitaires en LDC de
Linux, mais ignorées par les signatures d’IDS de Suricata. Cela affecte
principalement les signatures d’IDS pour le protocole HTTP et le contenu de flux
TCP. Les signatures pour les paquets TCP inspecteront un tel réseau comme
 habituellement.</p>

<p>TEMP-0856648-2BC2C9 (pas encore de numéro CVE attribué)</p>

<p>Lecture hors limites dans app-layer-dns-common.c. Pour un enregistrement A ou
AAAA de taille zéro, 4 ou 16 octets sont toujours lus.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 2.0.7-2+deb8u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets suricata.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1603.data"
# $Id: $
