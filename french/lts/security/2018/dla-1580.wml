#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que systemd est sujet à plusieurs vulnérabilités de
sécurité, allant d’attaques par déni de service à une possible augmentation de
droits à ceux du superutilisateur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1049">CVE-2018-1049</a>

<p>Une situation de compétition existe entre les unités .mount et .automount de
sorte que les requêtes automount du noyau pourraient ne pas être servies par
systemd, menant à ce que le noyau bloque le point de montage jusqu'à ce que n’importe
quel processus essayant d’utiliser ce <q>mount</q> gèlent. Une situation de
compétition comme celle-ci pourrait conduire à un déni de service, jusqu’à ce
que les points de montage soient démontés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15686">CVE-2018-15686</a>

<p>Une vulnérabilité dans unit_deserialize de systemd permet à un attaquant de
fournir un état arbitraire à travers une réexécution de systemd à l’aide de
NotifyAccess. Cela peut être utilisé pour influencer improprement l’exécution de
systemd et éventuellement une augmentation de droits à ceux du
superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15688">CVE-2018-15688</a>

<p>Une vulnérabilité de dépassement de tampon dans le client dhcp6 systemd
permet à un serveur dhcp6 malveillant de surcharger la mémoire de tas dans
systemd-networkd, qui n’est pas activé par défaut dans Debian.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 215-17+deb8u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets systemd.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1580.data"
# $Id: $
