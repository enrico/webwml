#use wml::debian::translation-check translation="5093adb58187d3b324285a4ca3c36045a2deb4fa" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les mises à jour de sécurité précédentes de Salt, un gestionnaire
d'exécution à distance, ont introduit des régressions pour lesquelles des
corrections complémentaires ont été publiées :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16846">CVE-2020-16846</a> régression

<p>L'initialisation de la clé maître de <q>salt-ssh</q> échoue</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3197">CVE-2021-3197</a> régression

<p>Des arguments valables sont rejetés pour le client SSH</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28243">CVE-2020-28243</a> complément

<p>Injection d'argument évitée dans restartcheck</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25282">CVE-2021-25282</a> régression

<p>La méthode pillar_roots.write ne peut pas écrire dans des
sous-répertoires</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25284">CVE-2021-25284</a> régression

<p>La fonction <q>cmd.run</q> plante lors du passage d'un argument à tuple</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2016.11.2+ds-1+deb9u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets salt.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de salt, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/salt">\
https://security-tracker.debian.org/tracker/salt</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2480-2.data"
# $Id: $
