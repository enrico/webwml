#use wml::debian::translation-check translation="d99a464491d4a05a96896fda2361a48b3e5572ab" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>L'implémentation de UntypedObjectDeserializer dans jackson-databind, une
bibliothèque JSON rapide et puissante pour Java, était prédisposée à une
attaque par déni de service lors du traitement de valeurs de tableau et
objets profondément imbriqués.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.8.6-1+deb9u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jackson-databind.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jackson-databind,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/jackson-databind">\
https://security-tracker.debian.org/tracker/jackson-databind</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">\
https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2990.data"
# $Id: $
