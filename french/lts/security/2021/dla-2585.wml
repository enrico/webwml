#use wml::debian::translation-check translation="27c5ac639565165ff8ef81d9ddc8de0945e0507a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>libupnp, le kit de développement portable pour les périphériques UPnP, permet
à des attaquants distants de provoquer un déni de service (plantage) à l'aide
d'un message SSDP contrefait à cause d’un déréférencement de pointeur NULL dans
les fonctions FindServiceControlURLPath et FindServiceEventURLPath dans
genlib/service_table/service_table.c.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1:1.6.19+git20160116-1.2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libupnp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libupnp, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libupnp">\
https://security-tracker.debian.org/tracker/libupnp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2585.data"
# $Id: $
