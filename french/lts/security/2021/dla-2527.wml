#use wml::debian::translation-check translation="0f8c1a26bd18c924370ac4b66154d37b8abfc80f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>golang-go.crypto a été récemment mis à jour avec un correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>. En conséquence, cela requiert que tous les paquets utilisant le code affecté
soient recompilés pour intégrer le correctif de sécurité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>

<p>Un problème a été découvert dans les bibliothèques Go additionnelles de
chiffrement, c'est-à-dire, golang-googlecode-go-crypto. Si plus de 256 GiB de
séquence de clé ou si par ailleurs le compteur dépasse 32 bits,
l’implémentation de amd64 générera d’abord une sortie incorrecte et puis
reviendra à la séquence de clé générée précédemment. Des octets répétés de
séquence de clé peuvent conduire à une perte de confidentialité dans les
applications de chiffrement ou à une prévisibilité dans les applications de
chiffrement pseudo-aléatoire (CSPRNG).</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 2.21-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets snapd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de snapd, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/snapd">https://security-tracker.debian.org/tracker/snapd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2527.data"
# $Id: $
