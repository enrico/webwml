#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans Apache Batik avant la version 1.9, des fichiers existant sur le système
de fichiers du serveur qui utilise batik, peuvent être divulgués à des
utilisateurs arbitraires envoyant des fichiers SVG formés de manière
malveillante. Les types de fichier pouvant être montrés dépendent du contexte
d’utilisateur dans lequel l’application exploitable est exécutée. Si
l’utilisateur est superutilisateur, une compromission totale du serveur
— incluant des fichiers confidentiels ou sensibles — serait possible. XXE peut
aussi être utilisé pour attaquer la disponibilité du serveur à l’aide d’un déni
de service car les références à l’intérieur du document XML peuvent trivialement
déclencher une attaque par amplification.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.7+dfsg-3+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets batik.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-926.data"
# $Id: $
