#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans la bibliothèque
graphique GD. Ils pourraient conduire à l'exécution de code arbitraire ou
provoquer un plantage de l'application.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9317">CVE-2016-9317</a>

<p>Dépassement d'entier signé dans gd_io.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10167">CVE-2016-10167</a>

<p>Traitement incorrect de données d’image manquantes pouvant provoquer un
plantage</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10168">CVE-2016-10168</a>

<p>GD2 stocke le nombre de morceaux horizontaux et verticaux sous forme de mots
(c'est-à-dire, 2 octets signés). Ces valeurs sont multipliées et assignées à un
entier lors de la lecture de l’image, ce qui peut provoquer un dépassement
d'entier.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.0.36~rc1~dfsg-6.1+deb7u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgd2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-804.data"
# $Id: $
