#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l’interpréteur Ruby 1.9.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0898">CVE-2017-0898</a>

<p>Vulnérabilité de sous-alimentation de tampon dans Kernel.sprintf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0899">CVE-2017-0899</a>

<p>Vulnérabilité de séquence d’échappement ANSI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0900">CVE-2017-0900</a>

<p>Vulnérabilité de déni de service dans la commande query.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0901">CVE-2017-0901</a>

<p>Installateur de gem permettant à un gem malveillant d’écraser des fichiers
arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10784">CVE-2017-10784</a>

<p>Vulnérabilité d’injection de séquence d’échappement dans l’authentification
 Basic de WEBrick.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14033">CVE-2017-14033</a>

<p>Vulnérabilité de sous-alimentation de tampon de décodage ASN1 dans OpenSSL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14064">CVE-2017-14064</a>

<p>Vulnérabilité d’exposition de tas lors de la génération de JSON.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.9.3.194-8.1+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby1.9.1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1114.data"
# $Id: $
