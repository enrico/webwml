#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Bob Friesenhahn du projet GraphicsMagick a découvert une vulnérabilité
d'injection de commande dans ImageMagick, un ensemble de programmes pour
manipuler des images. Un attaquant doté du contrôle sur l'image d'entrée ou
sur le nom de fichier d'entrée peut exécuter des commandes arbitraires avec
les privilèges de l'utilisateur exécutant l'application.</p>

<p>Cette mise à jour retire la possibilité d'utiliser un tuyau (|) dans les
noms de fichiers devant interagir avec imagemagick.</p>

<p>La mise à niveau de libmagickcore5 est importante et pas seulement celle
du paquet imagemagick. Les applications qui utilisent libmagickcore5
pourraient aussi être affectées et doivent être redémarrées après la mise
à niveau.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 8:6.7.7.10-5+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-500.data"
# $Id: $
