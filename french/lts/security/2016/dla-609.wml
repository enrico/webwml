#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour corrige les CVE décrits ci-dessous.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3857">CVE-2016-3857</a>

<p>Chiachih Wu a signalé deux bogues dans la couche de compatibilité OABI
d'ARM qui peuvent être utilisés par des utilisateurs locaux pour une
élévation de privilèges. La couche de compatibilité OABI est activée dans
toutes les versions du noyau pour armel et armhf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4470">CVE-2016-4470</a>

<p>Wade Mealing de Red Hat Product Security Team a signalé que dans
certains cas d'erreur le sous-système KEYS déréférencera un pointeur non
initialisé. Un utilisateur local peut utiliser l'appel système keyctl()
pour un déni de service (plantage) ou éventuellement pour une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5696">CVE-2016-5696</a>

<p>Yue Cao, Zhiyun Qian, Zhongjie Wang, Tuan Dao et Srikanth V.
Krishnamurthy de l'université de Californie, à Riverside, et Lisa
M. Marvel du laboratoire de recherche de l'armée des États-Unis ont
découvert que l'implémentation de Linux de la fonctionnalité <q>Challenge
ACK</q> de TCP a pour conséquence une attaque par canal auxiliaire qui peut
être utilisée pour découvrir des connexions TCP entre des adresses IP
spécifiques et pour injecter des messages dans ces connexions.</p>

<p>Lorsqu'un service est rendu disponible par TCP, cela peut permettre
à des attaquants distants de se faire passer pour un autre utilisateur
connecté au serveur, ou pour le serveur pour un autre utilisateur connecté.
Dans le cas où le service utilise un protocole avec authentification de
message (par exemple TLS ou SSH), cette vulnérabilité permet seulement un
déni de service (échec de connexion). Une attaque prend des dizaines de
secondes, aussi il est peu probable que des connexions TCP de courte durée
soient aussi vulnérables.</p>

<p>Ce problème peut être atténué en augmentant la limite de débit pour les
<q>Challenge ACK</q> de TCP pour qu'elle ne soit jamais dépassée : sysctl
net.ipv4.tcp_challenge_ack_limit=1000000000</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5829">CVE-2016-5829</a>

<p>Plusieurs vulnérabilités de dépassement de tas ont été découvertes dans
le pilote hiddev, permettant à un utilisateur local, doté d'un accès à un
périphérique HID, de provoquer un déni de service ou éventuellement une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6136">CVE-2016-6136</a>

<p>Pengfei Wang a découvert que le sous-système audit avait un bogue de
type <q>double fetch</q> ou <q>TOCTTOU</q> dans son traitement des
caractères spéciaux dans le nom d'un exécutable. Quand la journalisation
par audit de execve() est activée, cela permet à un utilisateur local de
générer des messages de journal trompeurs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6480">CVE-2016-6480</a>

<p>Pengfei Wang a découvert que le pilote aacraid pour les contrôleurs RAID
d'Adaptec avait un bogue de type <q>double fetch</q> ou <q>TOCTTOU</q>
dans sa validation des messages <q>FIB</q> passés au travers de l'appel
système ioctl(). Cela n'a pas d'impact de sécurité pratique dans les
versions actuelles de Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6828">CVE-2016-6828</a>

<p>Marco Grassi a signalé un bogue d'utilisation de mémoire après
libération dans l'implémentation de TCP qui peut être déclenché par des
utilisateurs locaux. Son impact sur la sécurité n'est pas clair, mais il
pourrait inclure un déni de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7118">CVE-2016-7118</a>

<p>Marcin Szewczyk a signalé que l'appel de fcntl() sur un descripteur de
fichier pour un répertoire sur un système de fichiers aufs pourrait avoir
pour conséquence un <q>oops</q>. Cela permet à des utilisateurs locaux de
provoquer un déni de service. Il s'agit d'une régression spécifique
à Debian introduite dans la version 3.2.81-1.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.2.81-2. Cette version corrige aussi un bogue d'échec de
construction (bogue nº 827561) pour les noyaux personnalisés où
CONFIG_MODULES est désactivé, une régression dans la version 3.2.81-1.
Elle met aussi à jour le jeu de fonctions PREEMPT_RT vers la
version 3.2.81-rt117.</p>

<p>Pour Debian 8 <q>Jessie</q>, le
<a href="https://security-tracker.debian.org/tracker/CVE-2016-3857">CVE-2016-3857</a>
n'a pas d'impact ; les <a href="https://security-tracker.debian.org/tracker/CVE-2016-4470">CVE-2016-4470</a>
et <a href="https://security-tracker.debian.org/tracker/CVE-2016-5829">CVE-2016-5829</a>
ont été corrigés dans linux version 3.16.7-ckt25-2+deb8u3 ou antérieure et
les problèmes restants sont corrigés dans la version 3 16.36-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-609.data"
# $Id: $
