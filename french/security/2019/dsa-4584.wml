#use wml::debian::translation-check translation="be70261454371a10eda4db4df697e3ff80e95b74" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans spamassassin, un filtre
anti-pourriel basé sur Perl et l'analyse de texte.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11805">CVE-2018-11805</a>

<p>Des fichiers de règles ou de configuration malveillants, éventuellement
téléchargés d'un serveur de mise à jour, pourraient exécuter des commandes
arbitraires selon plusieurs scénarios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12420">CVE-2019-12420</a>

<p>Des messages multi-parties contrefaits pour l'occasion peuvent faire que
spamassassin utilise des ressources excessives, avec pour conséquence un
déni de service.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
 dans la version 3.4.2-1~deb9u2.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 3.4.2-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets spamassassin.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de spamassassin,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/spamassassin">\
https://security-tracker.debian.org/tracker/spamassassin</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4584.data"
# $Id: $
