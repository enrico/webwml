#use wml::debian::translation-check translation="dcf86adf061c386a0fc377fda8ff1f638c073181" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>zsh, un langage de script et un interpréteur de commandes puissant,
n'empêchait pas l'expansion récursive de PROMPT_SUBST. Cela pourrait
permettre à un attaquant d'exécuter des commandes arbitraires dans un
interpréteur de commande d'utilisateur, par exemple en contraignant un
utilisateur de la fonction vcs_info à basculer vers une branche de Git avec
un nom contrefait pour l'occasion.</p>

<p>Pour la distribution oldstable (Buster), ce problème a été corrigé dans
la version 5.7.1-1+deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 5.8-6+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zsh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zsh, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/zsh">\
https://security-tracker.debian.org/tracker/zsh</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5078.data"
# $Id: $
