#
# Damyan Ivanov <dmn@debian.org>, 2011-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.61\n"
"PO-Revision-Date: 2020-12-17 14:42+0200\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@ludost.net>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 3.38.0\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Статистика на преводите на уеб сайта на Дебиан"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Има %d страници за превод."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Има %d байта за превод."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Има %d низа за превод."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Грешна версия на превода"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Преводът е твърде стар"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Оригиналният документ е променян след превода"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Оригиналният документ вече не съществува"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "не е наличен брой на попаденията"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "попадения"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Извличане на данни за разликите"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "Създадено с <transstatslink>"

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Обобщена информация за превода на"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Няма превод"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Остарял"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Преведено"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Обновен"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "файла"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "байта"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Забележка: списъкът на страниците е подреден по популярност. Поставете "
"мишката над името на страницата за да видите броя попадения."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Преводи, които имат нужда от обновяване"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Файл"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Разлики"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Бележка"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Обобщени разлики"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr "Команден за Git"

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Журнал на промените"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Превод"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Отговорник"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Статус"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Преводач"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Дата"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Общи страници без превод"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Общи страници без превод"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Новини без превод"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Новини без превод"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Страници за консултанти и потребители без превод"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Страници за консултанти и потребители без превод"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Международни страници без превод"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Международни страници без превод"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Преведени страници (обновени)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Преведени низове (файлове PO)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "Статистика на преведените низове (PO)"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Мъгяви"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Без превод"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Общо"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Общо:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Преведени страници"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Статистика на преводите по брой страници"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Език"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Преводи"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Преведени страници (по размер)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Статистика на преводите по размер на страниците"

#~ msgid "Colored diff"
#~ msgstr "Оцветени разлики"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Оцветени разлики"

#~ msgid "Created with"
#~ msgstr "Създаден с"

#~ msgid "Origin"
#~ msgstr "Оригинален документ"

#~ msgid "Unified diff"
#~ msgstr "Разлики в стандартен формат"
