#use wml::debian::translation-check translation="5ff7a077f0e13d9fe9cae52a517d81e9a15c05d7" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag description>보안 업데이트</define-tag>
<define-tag moreinfo>
<p>여러 취약점이 리눅스 커널에서 발견되어 권한 상승, 서비스 거부 또는 정보 유출을 일으킬 수 있습니다.
</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14821">CVE-2019-14821</a>

    <p>Matt Delco reported a race condition in KVM's coalesced MMIO
    facility, which could lead to out-of-bounds access in the kernel.
    A local attacker permitted to access /dev/kvm could use this to
    cause a denial of service (memory corruption or crash) or possibly
    for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14835">CVE-2019-14835</a>

    <p>Peter Pi of Tencent Blade Team discovered a missing bounds check
    in vhost_net, the network back-end driver for KVM hosts, leading
    to a buffer overflow when the host begins live migration of a VM.
    An attacker in control of a VM could use this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15117">CVE-2019-15117</a>

    <p>Hui Peng and Mathias Payer reported a missing bounds check in the
    usb-audio driver's descriptor parsing code, leading to a buffer
    over-read.  An attacker able to add USB devices could possibly use
    this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15118">CVE-2019-15118</a>

    <p>Hui Peng and Mathias Payer reported unbounded recursion in the
    usb-audio driver's descriptor parsing code, leading to a stack
    overflow.  An attacker able to add USB devices could use this to
    cause a denial of service (memory corruption or crash) or possibly
    for privilege escalation.  On the amd64 architecture, and on the
    arm64 architecture in buster, this is mitigated by a guard page
    on the kernel stack, so that it is only possible to cause a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15902">CVE-2019-15902</a>

    <p>Brad Spengler reported that a backporting error reintroduced a
    spectre-v1 vulnerability in the ptrace subsystem in the
    ptrace_get_debugreg() function.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 4.9.189-3+deb9u1.</p>

<p>For the stable distribution 안정배포(buster)에서, 이 문제를
버전 4.19.67-2+deb10u1에서 수정했습니다.</p>

<p>linux 패키지를 업그레이드 하는 게 좋습니다.</p>

<p>linux의 자세한 보안 상태는 보안 추적 페이지를 참조하십시오:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4531.data"
