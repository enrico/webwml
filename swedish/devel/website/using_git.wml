#use wml::debian::template title="Använda git för att manipulera webbplatsens källkod"
#use wml::debian::translation-check translation="74a4415fa07f5810685f1d0568bf4b695cc931b3"

<h2>Introduktion</h2>

<p>Git är ett <a href="https://en.wikipedia.org/wiki/Version_control">\
versionshanteringssystem</a> som hjälper till att hantera flera personer som
jobbar på samma material samtidigt. Varje användare kan ha en lokal kopia
av ett huvudförråd. De lokala kopiorna kan befinna sig på samma maskin, eller
över hela världen. Användare kan sedan modifiera sin lokala kopia som de
vill och sedan när det modifierade materialet är färdigt så kan de
committa ändringar och sedan skicka till huvudförrådet.</p>

<p>Git kommer inte att låta dig pusha en commit direkt om fjärrförrådet har
några nyare commits (ändringar) än din lokala kopia på samma gren.
I sådana fall där en konflikt sker, var vänlig hämta och uppdatera din lokala
kopia först och <code>rebase</code> dina nya modifikationer på den senaste
ändringen.
</p>

<h3><a name="write-access">Skrivrättigheter till Git-förrådet</a></h3>

<p>Hela Debianwebbplatsens källkod hanteras med Git. Den finns vid
<url https://salsa.debian.org/webmaster-team/webwml/>. Som standard
har gäster inte rättigheter har pusha commits till källkodsförrådet.
Du behöver någon sorts rättighet för att få skrivåtkomst till förrådet.
</p>

<h4><a name="write-access-unlimited">Obegränsade skrivrättigheter</a></h4>

<p>
Om du behöver obegränsade skrivrättigheter till förrådet (om du exempelvis är 
på väg att bli en frekvent bidragslämnare), var vänlig efterfråga detta
via webbgränssnittet på <url https://salsa.debian.org/webmaster-team/webwml/>
efter att du har loggat in på salsa-plattformen.</p>

<p>
Om du är ny på utveckling av Debian's webbsida och inte har någon tidigare
erfarenhet, var vänlig kontakta även <a href="mailto:debian-www@lists.debian.org">
debian-www@lists.debian.org</a> med en introduktion om dig själv innan du
efterfrågar obegränsade skrivrättigheter. Vänligen
skriv något användbart i din introduktion, så som vilket språk du vill jobba
med eller vilken del av webbplatsen som du kommer att jobba på och vem
som kommer att gå i god för dig.
</p>

<h4><a name="write-access-via-merge-request">Skriv till förrådet via Merge Requests</a></h4>
<p>
Om du inte har intentionen att få obegränsade skrivrättigheter till förrådet
eller inte har möjligheten till detta, så kan du alltid skicka Merge Requests
och låta andra utvecklare undersöka och acceptera ditt arbete. Var vänlig
skicka Merge Requests enligt standardförfarandet som det tillhandahålls av
Salsa Gitlab-plattformen via dess webbgränssnitt. (läs
<a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Arbetsflöde vid projektförgreningar</a>
och
<a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">När du arbetar i en förgrening</a>
för ytterligare detaljer).
</p>

<p>
Merge Requests övervakas inte av alla webbplatsutvecklare och kan därmed kanske
inte behandlas i tid. Om du inte är säker på om ditt bidrag skulle accepteras,
vänligen skicka e-post till sändlistan
<a href="https://lists.debian.org/debian-www/">debian-www</a> och
efterfråga en granskning.
</p>

<h3><a name="work-on-repository">Jobba med förrådet</a></h3>

<h4><a name="get-local-repo-copy">Hämta en lokal kopia av förrådet</a></h4>

<p>Först och främst måste du installera git för att jobba med förrådet. Sedan
måste du konfigurera din e-post och användarnamn på din dator (vänligen
se allmän git-dokumentation för att se hur man gör detta). Sedan kan du
klona förrådet (med andra ord - göra en lokal kopia av det) på ett av två
sätt.</p>

<p>Det rekommenderade sättet att jobba med webwml är att först registrera ett
konto på salsa.debian.org och aktivera git SSH-åtkomst genom att ladda upp
en publik SSH-nyckel till ditt salsa-konto. Se <a
href="https://salsa.debian.org/help/ssh/index.md">salsas hjälpsidor</a>
flr ytterligare detaljer om hur man gör det. Sedan kan du klona webwml-förrådet
med hjälp av följande kommando:</p>

<pre>
   git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>

<p>Om du inte har ett salsa-konto är en alternativ metod att klona
förrådet med hjälp av HTTPS-protokollet:</p>

<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>

<p>Detta kommer att ge dig samma förråd lokalt, men du kommer inte att kunna
pusha ändringar direkt tillbaks på detta sätt.</p>

<p>En kloning av fullständiga webwml-förrådet skulle kräva hämtning av runt
500MB data, och kan därmed vara svårt för dem med långsam eller instabil
internetuppkoppling. Du kan försöka med "grund" kloning med minimalt djup
först för mindre initial dataöverföring:</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Efter att du har hämtat en användbar (grund) kopia av förrådet, kan
du göra din lokala kopia djupare och eventuellt fullständigt återställa den
till ett komplett lokalt förråd:
</p>

<pre>
  git fetch --deepen=1000 # fördjupa förrådet med ytterligare 1000 commits
  git fetch --unshallow   # hämta alla saknade commits, och konvertera förrådet till ett fullständigt
</pre>

<h4><a name="partial-content-checkout">Ofullständig innehållshämtning</a></h4>

<p>Du kan skapa en checkout för endast en undergrupp av sidorna så här:</p>

<pre>
   $ git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git
   $ cd webwml
   $ git config core.sparseCheckout true
   I webwml: Skapa filen .git/info/sparse-checkout med innehåll som detta
   (om du endast vill ha basfilerna, Engelska, Catalanska och Spanska översättningarna):
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
   och sedan:
   $ git checkout --
</pre>

<h3><a name="submit-changes">Skicka in lokala ändringar</a></h3>

<h4><a name="keep-local-repo-up-to-date">Håll ditt lokala förråd uppdaterat</a></h4>

<p>Med några dagars mellanrum (och åtminstone före du påbörjar redigeringsarbete!)
kommer du att vilja köra en</p>

<pre>
   git pull
</pre>

<p>för att få alla filer från förrådet som har ändrats.</p>

<p>
Det rekommenderas starkt att hålla din lokala git-arbetskatalog ren innan
du utför en "git pull" och arbetar med denna. Om du har icke uppskickade
ändringar eller lokala commits som inte finns i fjärrförrådet på aktuell
gren kommer "git pull" att automatiskt skapa merge commits eller till och med
misslyckas på grund av konflikter. Vänligen överväg att hålla ofullständigt
arbete i en annan gren eller genom att använda kommandon som "git stash".
</p>

<p>Notera att git är ett distribuerat (och inte centraliserat)
versionshanteringssystem. Detta betyder att när du committar ändringar så kommer
dessa endast att lagras i ditt lokala förråd. För att dela dessa med andra
så måste du pusha dina ändringar till det centraliserade förrådet på salsa.</p>

<h4><a name="example-edit-english-file">Exempel på en ändring i en engelsk fil</a></h4>

<p>
Ett exempel på hur man redigerar engelska filer i webbplatsens källkodsförråd
följer. Efter du har hämtat en lokal kopia på förrådet mha "git clone" och innan
du påbörjar redigeringsarbetet, kör följande kommando:
</p>

<pre>
   $ git pull
</pre>

<p>Gör nu ändringar till filer. När du är klar, kan du committa dina ändringar
till det lokala förådet med hjälp av</p>

<pre>
   $ git add sökväg/till/fil(er)
   $ git commit -m "Ditt ändringsmeddelande"
</pre>

<p>Om du har obegränsade skrivrättigheter till förrådet kan du nu pusha
dina ändringar direkt till Salsa-förrådet:</p>

<pre>
   $ git push
</pre>

<p>Om du inte har direkt skrivåtkomst till webwml-förrådet, var vänlig
överväg att tillhandahålla dina ändringar med hjälp av en Merge Request som
det tillhandahålls av Salsa Gitlab-plattformen eller fråga andra utvecklare
om hjälp.
</p>

<p>Det är allt som behövs när det gäller enkel användning av git för att
manipulera Debians webbplats källkod. För mer information om git, var vänlig
se Git's dokumentation.</p>

<h4><a name="closing-debian-bug-in-git-commits">Stänga Debianfelrapporter mha git commits</a></h4>

<p>
Om du inkluderar <code>Closes: #</code><var>nnnnnn</var> i ditt
commitloggmeddelande, så kommer felnummer <code>#</code><var>nnnnnn</var> att
stängas automatiskt när du pushar dina ändringar. Den exakta formen för detta
är samma som
<a href="$(DOC)/debian-policy/ch-source.html#id24">i Debians policy</a>.</p>

<p>
Många Debianwebbplatser stödjer SSL/TLS, var vänlig använd HTTPS-länkar
där det är möjligt och förnuftigt.
Några Debian/DebConf/SPI/osv webbplats har inte SSL-stöd eller är
endast signerade av SPI och inte av någon SSL CA som är betrodd av
webbläsare utanför Debian, så därmed bör vi undvika att länka till
https:-versioner av dessa webbplatser så att personer som inte använder
Debian får fel som de inte förstår sig på.
Gitförrådet kommer att avvisa commits som innehåller rena HTTP-länkar för
Debianwebbplatser som stödjer HTTPS eller innehåller HTTPS-länkar för
Debian/DebConf/SPI-webbplatserna som är kända att inte stödja HTTPS eller
använder certifikat som endast är signerade av SPI.</p>

<h3><a name="translation-work">Jobba på översättningar</a></h3>

<p>
Översättningar skall alltid hållas uppdaterade och up-to-date med dess
engelska motsvarighet. Huvudet "translation-check" i översatta filer används
för att spåra vilka versioner av engelska filer som den aktuella översättningen
baseras på. Om du ändrar översatta filer, kan du vara tvungen att uppdatera
translation-check-huvudet för att matcha git commit hashen i motsvarande
ändring i den Engelska filen. Du kan hitta denna hash med </p>

<pre>
$ git log sökväg/till/engelsk/fil
</pre>

<p>Om du gör en ny översättning av en fil, vänligen använd <q>copypage.pl</q>-skriptet
så kommer det att göra en ny mall för ditt språk, och inkludera ett korrekt
översättningshuvud.</p>

<h4><a name="translation-smart-change">Översättningsändringar med smart_change.pl</a></h4>

<p><code>smart_change.pl</code> är ett skript som är designat för att göra
det lättare att uppdatera original-filer och dess översättningar tillsammans.
Det finns två sätt att göra detta, beroende på vilken typ av ändringar du gör.</p>

<p>För att använda <code>smart_change.pl</code> för att endast uppdatera
translation-check-huvudena när du arbetar med filer manuellt:</p>

<ul>
  <li>Gör ändringarna till originalfil(erna), och committa</li>
  <li>Uppdatera översättningar</li>
  <li>Kör smart_change.pl - denna kommer att snappa upp förändringarna och
      uppdatera huvuden i de översatta filerna</li>
  <li>Granska ändringarna (exempelvis med <q>git diff -u</q>)</li>
  <li>Committa och pusha ändringarna</li>
</ul>

<p>Eller, om du använder smart_change med ett reguljärt uttryck för att göra
flera ändringar över filer i ett pass:</p>

<ol>
  <li>Kör <code>smart_change.pl -s s/FOO/BAR/ origfile1 origfile2 ...</code></li>
  <li>Granska förändringarna (t.ex. med <code>git diff</code>)
  <li>Committa original-filerna</li>
  <li>Kör <code>smart_change.pl origfile1 origfile2</code>
    (vilket är <strong>utan reguljära uttrycket</strong> den här gången); det kommer nu
	 att endast uppdatera huvudena i de översatta filerna</li>
  <li>Slutligen, committa översättningsändringarna</li>
</ol>

<p>Detta är mer komplicerat än tidigare (det kräver två commits), men
oundvikligt på grund av hur git commit-hashen fungerar.</p>

<h2><a name="notifications">Få aviseringar</a></h2>

<h3><a name="commit-notifications">Få commit-aviseringar</a></h3>

<p>
Vi har satt upp konfigurationen av projektet i Salsa så att commits
visas i IRC-kanalen #debian-www.</p>

<p>Om du vill få aviseringar via e-post när det finns commits i webwml-förrådet
vänligen prenumerera på pseudopaketet <q>www.debian.org</q> via
tracker.debian.org och aktivera nyckelordet <q>vcs</q> där, genom att följa
dessa steg (en gång endast):</p>

<ul>
  <li>Öppna en webbläsare och gå till <url https://tracker.debian.org/pkg/www.debian.org></li>
  <li>Prenumerera på pseudopaketet <q>www.debian.org</q>. (Du kan autentisera
      via SSO eller registrera en e-post och lösenord, om du inte redan har 
      använt tracker.debian.org för andra ändamål).</li>
  <li>Gå till <url https://tracker.debian.org/accounts/subscriptions/>, och sedan till <q>modify
      keywords</q>, kryssa i <q>vcs</q> (om den inte redan är ikryssad) och spara.</li>
  <li>Från och med nu kommer du att få e-post när någon committar till
      webwml-förrådet. Vi kommer att lägga till de andra webmaster-team-förråden inom kort.</li>
</ul>

<h3><a name="merge-request-notifications">Få Merge-Requst-aviseringar</a></h3>

<p>
Om du vill få aviseringar via e-post när det finns nya Merge Requests skickade
till webwml-förrådet på Salsa Gitlab-plattformen, kan du konfigurera dina
notifierings-inställningar för webwml-förrådet via webgränssnittet,
och följa dessa steg:
</p>

<ul>
   <li>Logga in på ditt Salsa-konto och gå till projekt-sidan;</li>
   <li>Klicka på ringklocke-ikonen på översidan av projekt-sidan;</li>
   <li>Välj notifieringsnivån som du önskar.</li>
</ul>
