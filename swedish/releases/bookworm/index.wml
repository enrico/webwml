#use wml::debian::template title="Versionsfakta för Debian &ldquo;Bookworm&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="603a794aa9a8c6625ee116d9bce0ce8183f1476e"

<if-stable-release release="bookworm">

<p>Debian <current_release_bookworm> släpptes
<a href="$(HOME)/News/<current_release_newsurl_bookworm/>"><current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
  "Debian 12.0 släpptes ursprungligen <:=spokendate('XXXXXXXX'):>."
/>
Utgåvan inkluderade många stora
förändringar, vilka beskrivs i vårt
our <a href="$(HOME)/News/XXXX/XXXXXXXX">pressmeddelande</a> och
<a href="releasenotes">versionsfakta</a>.</p>

#<p><strong>Debian 12 har ersatts av
#<a href="../trixie/">Debian 13 (<q>Trixie</q>)</a>.
#Reguljära säkerhetsuppdateringar har upphört från och med <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><srong>Bookworm stöds av långtidsstöd (Long Term Support - LTS) fram
#till slutet på xxxxx 20xx. LTS begränsas till i386, amd64, armel, armhf och arm64.
#Inga andra arkitekturer stöd längre i Bookworm.
#För ytterligare information, vänligen se <a
#href="https://wiki.debian.org/LTS">LTS-avdelningen i Debian-wikin</a>.
#</strong></p>

<p>För att få tag på och installera Debian, se vår sida med
<a href="debian-installer/">installationsinformation</a> samt
<a href="installmanual">installationsguiden</a>. För att uppgradera från en
tidigare Debianutgåva, se informationen i
<a href="releasenotes">versionsfakta</a>.</p>

### Activate the following when LTS period starts.
#<p>Arkitekturer som stöd under perioden för långtidsstöd:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Datorarkitekturer som stöds i den ursprungliga utgåvan av Bookworm:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Tvärt emot våra önskemål finns det en del problem i denna utgåva, även om den
kallas för <em>stabil</em>. Vi har sammanställt
<a href="errata">en lista över de största kända problemen</a>, och du kan alltid
<a href="reportingbugs">rapportera andra problem</a> till oss.</p>

<p>Sist, men inte minst, har vi en lista över <a href="credits">folk som skall
ha tack</a> för att ha möjliggjort denna version.</p>
</if-stable-release>

<if-stable-release release="bullseye">

<p>Kodnamnet för nästa stora Debian-utgåva efter <a
href="../bullseye/">Bullseye</a> är <q>Bookworm</q>.</p>

<p>Denna utgåva startade som en kopia av Bullseye, och är för närvarande i en fas
som kallas <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">testing</a></q>.
(i uttestning). Detta betyder att saker inte borde gå sönder lika illa som i
den instabila eller den experimentella distributionen, eftersom paket tillåts in
i denna utgåva endast efter att en viss tid har gått, och när de inte har
några utgåvekritiska fel rapporterade mot sig.</p>

<p>Vänligen notera att säkerhetsuppdateringar för <q>testing</q>-distributionen
<strong>inte</strong> ännu hanteras av säkerhetsgruppen. Därför får <q>uttestningsutgåvan</q>
<strong>inte</strong> säkerhetsuppdateringar i rimlig tid.
# For more information please see the
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">announcement</a>
# of the Testing Security Team.
Du uppmanas att för närvarande ändra dina sources.list-poster från testing till
stretch om du behöver säkerhetsstöd. Se även posten i
<a href="$(HOME)/security/faq#testing">Säkerhetsgruppens FAQ</a> för
<q>uttestnings</q>-distributionen.</p>

<p>Det kan finnas ett <a href="releasenotes">utkast av versionfakta</a> tillgängligt.
Vänligen <a href="https://bugs.debian.org/release-notes">kontrollera även de
föreslagna tilläggen till versionsfakta</a>.<p>

<p>För installationsavbildningar och dokumentation om hur man installerar
<q>testing</q>, se <a href="$(HOME)/devel/debian-installer/">sidan för Debian-Installer</a>.</p>

<p>För att hitta mer information om hur <q>testing</q>-distributionen fungerar,
se <a href="$(HOME)/devel/testing">utvecklarnas information om den</a>.</p>

<p>Folk frågar ofta om det finns en enda <q>förloppsmätare</q>.
Olyckligtvis finns ingen sådan, men vi kan hänvisa dig till flera olika
platser som beskriver saker som behöver hända för att utgåvan skall ske:</p>

<ul>
  <li><a href="https://release.debian.org/">Generell utgåvestatus</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Utgåve-kritiska fel</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Fel i grundsystemet</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Fel i standard- och task-paket</a></li>
</ul>

<p>Utöver detta, så skickar den utgåveansvarige generella statusrapporter
till <a href="https://lists.debian.org/debian-devel-announce/">\
sändlistan debian-devel-announce</a>.</p>

</if-stable-release>

<if-stable-release release="buster">

<p>Kodnamnet för nästa stora Debianutgåva efter <a
href="../bullseye /">Bullseye</a> är <q>Bookworm</q>. För närvarande
har inte <q>Bullseye</q> släppts ännu. Så <q>Bookworm</q> är fortfarande
långt borta.</p>

</if-stable-release>
