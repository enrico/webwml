#use wml::debian::template title="Updates voor Debian 2.0"
#use wml::debian::translation-check translation="77fca729a2ec87b4ac92922fa059fdcb368e44c6"

<P>Verschillende pakketten in Debian 2.0 zijn sinds de release op 24 juli
bijgewerkt met verbeteringen op het gebied van beveiliging en andere
wijzigingen die te belangrijk zijn om te wachten tot de volgende hoofdrelease.

<P>Debian zet zich in om zo snel mogelijk beveiligingsupdates voor de stabiele
distributie te leveren, maar we hebben ook tijd nodig om dergelijke updates
grondig te testen om er zeker van te zijn dat ze aan onze hoge normen voldoen.

<P>Zij die willen helpen bij het testproces of van mening zijn dat een bepaalde
kwetsbaarheid te kritiek is om te wachten op Debian's eigen testproces, kunnen
bijgewerkte pakketten halen uit de distributie
<a href="http://ftp.debian.org/debian/dists/proposed-updates/">proposed-updates</a>.
Degenen die het nieuwe pakketgereedschap <EM>apt</EM> gebruiken, willen
misschien wel de volgende regel toevoegen aan hun bestand
<code>/etc/apt/sources.list</code> om een vlotte opwaardering naar de recentere
pakketten makkelijker te maken, waarbij de URI op passende wijze aangepast wordt:


<div class="centerblock">
<p>
<code>deb http://ftp1.us.debian.org/debian dists/proposed-updates/</code>
</p>
</div>

