#use wml::debian::template title="Λογότυπα του Debian " BARETITLE=true
#include "$(ENGLISHDIR)/logos/index.data"
#use wml::debian::translation-check translation="97732ca8593e39ce8b981adf7f81657417b62c73" maintainer="galaxico"

<p>Το Debian έχει δύο λογότυπα. Το <a href="#open-use">επίσημο λογότυπο</a> (γνωστό
επίσης και ως 
   <q>ανοιχτής χρήσης λογότυπο</q>) περιέχει τον πασίγνωστο <q>στρόβιλο (swirl)</q> του Debian και
   αντιπροσωπεύει με τον καλλίτερο τρόπο την οπτική ταυτότητα του Σχεδίου Debian. Υπάρχει επίσης ένα ξεχωριστό, <a
  href="#restricted-use">περιορισμένης χρήσης λογότυπο</a>, για χρήση μόνο από το Σχέδιο Debian 
  και τα μέλη του. Για να αναφερθείτε στο Debian, παρακαλούμε χρησιμοποιήστε το λογότυπο ανοιχτής χρήσης.</p>

<hr>

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="open-use">Ανοιχτής χρήσης λογότυπο του Debian</a></th>
</tr>
<tr>
<td>

<p>Το ανοιχτής χρήσης λογότυπο του Debian έρχεται σε δύο "γεύσεις", με ή χωρίς την ταμπέλα
  &ldquo;Debian&rdquo;.</p>

<p>Τα ανοιχτής χρήσης λογότυπα του Debian είναι Copyright (c) 1999 του
  <a href="https://www.spi-inc.org/">Software in the Public Interest, Inc.</a>,
  και κυκλοφορούν κάτω από τους όρους της 
  <a href="https://www.gnu.org/copyleft/lgpl.html">GNU Lesser General Public
  License</a>, έκδοση 3 ή οποιαδήποτε μεταγενέστερη, ή, αν το επιλέγετε, την άδεια
  <a href="https://creativecommons.org/licenses/by-sa/3.0/">Creative Commons
  Attribution-ShareAlike 3.0 Unported License</a>.</p>

<p>Σημείωση: θα εκτιμούσαμε αν κάνατε την εικόνα έναν σύνδεσμο στον ιστότοπο
<a href="$(HOME)">https://www.debian.org/</a> αν πρόκειται να την χρησιμοποιήσετε σε μια ιστοσελίδα.</p>
</td>
<td>
<openlogotable>
</td>
</tr>
</table>

<hr />

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="restricted-use">Περιορισμένης χρήσης λογότυπο του Debian</a></th>
</tr>
<tr>
<td>
<h3>Άδεια του περιορισμένης χρήσης λογότυπου του Debian</h3>

<p>Πνευματικά Δικαιώματα/Copyright (c) 1999 Software in the Public Interest</p>
<ol>
	<li>Αυτό το λογότυπο μπορεί να χρησιμοποιηθεί μόνο αν:
         <ul>
	    <li>το προϊόν για το οποίο χρησιμοποιείται κατασκευάζεται με χρήση
	    μιας τεκμηριωμένης διαδικασίας όπως δημοσιεύεται στον ιστότοπο 
		www.debian.org (για παράδειγμ δημιουργία επίσημων CD), ή </li>
	    <li>δίνεται επίσημη άδεια από το Debian για την χρήση του για έναν σκοπό </li>
         </ul>
	 </li>
	<li>Μπορεί να χρησιμοποιηθεί αν ένα επίσημο κομμάτι του debian (που αποφασίζεται με
	 βάση τους κανόνες στο Ι) είναι μέρος του όλου προϊόντος, και εφόσον γίνεται ξεκάθαρο
	 ότι έχει δοθεί επίσημα άδεια μόνο για αυτό το μέρος</li> 
	<li>Διατηρούμε το δικαίωμα να ανακαλέσουμε την άδεια για ένα προϊόν</li>
</ol>

<p>Έχει δοθεί άδεια για τη χρήση του περιορισμένης χρήσης λογότυπου σε είδη ιματισμού (μπλουζάκια,
καπέλα, κλπ.) στον βαθμό που αυτά κατασκευάζονται από έναν προγραμματιστή/μια προγραμματίστρια του Debian 
και δεν πωλούνται για κέρδος.</p>
</td>
<td>
<officiallogotable>
</td>
</tr>
</table>

<hr />

<h2>Σχετικά με τα λογότυπα του Debian</h2>
<p>
Τα λογότυπα του Debian επιλέχθηκαν με ψηφοφορία από τους προγραμματιστές/προγραμματίστριες του Debian
το 1999.
Δημιουργήθηκαν από τον <a href="mailto:rsilva@debian.org">Raul Silva</a>. Το κόκκινο χρώμα στη
γραμματοσειρά είναι τυπικά το <strong>Rubine Red 2X CVC</strong>. Το επικαιροποιημένο/σημερινό ισοδύναμο
είναι είτε το PANTONE Strong Red C (που αποδίδεται στο RGB ως #CE0056) είτε το PANTONE Rubine Red C
(που αποδίδεται στο RGB ως #CE0058). Μπορείτε να βρείτε περισσότερες λεπτομέρειες σχετικά
με τα λογότυπα και τον λόγο αντικατάστασης του PANTONE Rubine Red 2X CVC ή και για άλλα ισοδύναμα 
του κόκκινου χρώματος στην σελίδα <a href="https://wiki.debian.org/DebianLogo">Debian Logo Wiki</a>.</p>

<h2>Άλλες εικόνες προώθησης</h2>

<h3>Κουμπιά (web) Debian</h3>

<p><img class="ico" src="button-1.gif" alt="[Debian GNU/Linux]" />
Αυτό είναι το πρώτο κουμπί που φτιάχτηκε για το Σχέδιο. Η άδεια αυτού του λογότυπου 
είναι ισοδύναμη με αυτήν για το ανοιχτης χρήσης λογότυπο. Το κουμπί δημιουργήθηκε 
από τον <a href="mailto:csmall@debian.org">Craig Small</a>.</p>

<p>Αυτά είναι μερικά ακόμα κουμπιά που έχουν σχεδιαστεί για το Debian:</p>
<br />
<morebuttons>

<h3>Το λογότυπο Διαφορετικότητας του Debian </h3>

<p>Υπάρχει μια παραλλαγή του λογότυπου του Debian για την προώθηση της διαφορετικότητας 
στην κοινότητά μας, ονομάζεται λογότυπο Διαφορετικότητας του Debian:
<br/>
<img src="diversity-2019.png" alt="[Debian Diversity logo]" />
<br/>
Το λογότυπο δημιουργήθηκε από τον <a href="https://wiki.debian.org/ValessioBrito">Valessio Brito</a>
και είναι υπο την άδεια GPLv3.
Ο πηγαίος κώδικας (μορφή SVG) είναι διαθέσιμος στο <a href="https://gitlab.com/valessiobrito/artwork">αποθετήριο Git</a>
του συγγραφέα.
<br />
</p>

<h3>Το εξαγωνικό αυτοκόλλητο του Debian</h3>

<p>Αυτό είναι ένα αυτοκόλλητο που ακολουθεί τις 
<a href="https://github.com/terinjokes/StickerConstructorSpec">προδιαγραφές του εξαγωνικού αυτοκόλλητου</a>:
<br/>
<img src="hexagonal.png" alt="[Debian GNU/Linux]" />
<br/>
Ο πηγαίος κώδικας (σε μορφή SVG) και ένα αρχείο Makefile για τη δημιουργία των προεπισκοπήσεων σε png και svg μορφή
είναι διαθέσιμα στο αποθετήριο
<a href="https://salsa.debian.org/debian/debian-flyers/tree/master/hexagonal-sticker">Debian flyers</a>.

Η άδεια χρήσης αυτού του αυτοκόλλητου είναι ισοδύναμη με αυτήν για το ανοιχτής χρήσης λογότυπου.  
Το αυτοκόλλητο δημιουργήθηκε από την <a href="mailto:valhalla@trueelena.org">Elena Grandi</a>.</p>
<br />

#
# Logo font: Poppl Laudatio Condensed
#
# https://lists.debian.org/debian-www/2003/08/msg00261.html
#
