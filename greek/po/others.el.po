# galaxico <galas@tee.gr>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2020-09-09 14:27+0300\n"
"Last-Translator: galaxico <galas@tee.gr>\n"
"Language-Team: debian-l10n-greek@lists.debian.org\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.3\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Γωνιά των νέων Μελών"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Βήμα 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Βήμα 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Βήμα 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Βήμα 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Βήμα 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Βήμα 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Βήμα 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Λίστα ελέγχου του Υποψήφιου"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Δείτε τη σελίδα <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/"
"french/</a> (διαθέσιμη μόνο στα Γαλλικά) για περισσότερες πληροφορίες."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Περισσότερες πληροφορίες"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Δείτε τη σελίδα <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/"
"intl/spanish/</a> (διαθέσιμη μόνο στα Ισπανικά) για περισσότερες πληροφορίες."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Τηλέφωνο"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Τηλεομοιοτυπία"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Διεύθυνση"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Προϊόντα"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "Μπλουζάκια T-shirt"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "καπέλα"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "αυτοκόλλητα"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "κούπες"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "άλλος ιματισμός"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "υποκάμισο"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "frisbee"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "βάσεις ποντικιού (mouse pad)"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "σήματα"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "μπασκέτες"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "σκουλαρίκια"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "βαλίτσες"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "ομπρέλλες"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "μαξιλαροθήκες"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "κλειδοθήκες"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "Ελβετικοί σουγιάδες"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "κλειδιά USB"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "Αναδέτες λαιμού"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "άλλα"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "Διαθέσιμες Γλώσσες:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "Παράδοση Διεθνώς:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr "εντός Ευρώπης"

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr "Αρχική χώρα:"

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Δωρίζει χρήματα στο Debian"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""
"Τα χρήματα χρησιμοποιούνται για τη διοργάνωση τοπικών εκδηλώσεων για το "
"ελεύθερο λογισμικό"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "Με&nbsp;``Debian''"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Χωρίς&nbsp;``Debian''"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Encapsulated PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Με τη δύναμη του Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Με τη δύναμη του Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Με τη δύναμη του Debian]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (mini button)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "το ίδιο με το παραπάνω"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Πόσο καιρό χρησιμοποιείτε το Debian;"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Είστε προγραμματιστής του Debian;"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "Σε ποιούς τομείς του Debian εμπλέκεστε;"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Τι σας έκανε να ενδιαφερθείτε να δουλέψετε με το Debian;"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Έχετε οποιεσδήποτε συμβουλές για γυναίκες που ενδιαφέρονται να εμπλακούν "
"περισσότερο με το Debian;"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr "Συμμετέχετε με άλλες γυναίκες σε κάποιες ομάδες τεχνολογίας; Σε ποιες;"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Πείτε λίγα παραπάνω για σας..."

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr ""
#~ "Υπάρχουν <total_consultant>  σύμβουλοι του Debian σε <total_country> "
#~ "χώρες "

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "<void id=\"d-i\" />Unknown"

#~ msgid "??"
#~ msgstr ";;"

#~ msgid "ALL"
#~ msgstr "ΌΛΑ"

#~ msgid "BAD"
#~ msgstr "Άσχημα"

#~ msgid "BAD?"
#~ msgstr "Άσχημα;"

#~ msgid "Booting"
#~ msgstr "Εκκίνηση"

#~ msgid "Building"
#~ msgstr "Κατασκευάζεται"

#~ msgid "Clear"
#~ msgstr "Καθαρισμός"

#~ msgid "Company:"
#~ msgstr "Εταιρεία:"

#~ msgid "Download"
#~ msgstr "Μεταφόρτωση"

#~ msgid "Email:"
#~ msgstr "Ηλ.Διεύθυνση:"

#~ msgid "Location:"
#~ msgstr "Τοποθεσία:"

#~ msgid "Mailing List Subscription"
#~ msgstr "Συνδρομή σε κατάλογο"

#~ msgid "Mailing List Unsubscription"
#~ msgstr "Κατάργηση Συνδρομής"

#~ msgid "Moderated:"
#~ msgstr "Ελεγχόμενη:"

#~ msgid "Name:"
#~ msgstr "Όνομα:"

#~ msgid "No description given"
#~ msgstr "Δεν δόθηκε περιγραφή"

#~ msgid "No images"
#~ msgstr "Χωρίς είδωλα"

#~ msgid "No kernel"
#~ msgstr "Χωρίς πυρήνα"

#~ msgid "Not yet"
#~ msgstr "Όχι ακόμη"

#~ msgid "OK"
#~ msgstr "Εντάξει"

#~ msgid "OK?"
#~ msgstr "Εντάξει;"

#~ msgid "Old banner ads"
#~ msgstr "Παλαιότερες διαφημίσεις"

#~ msgid ""
#~ "Only messages signed by a Debian developer will be accepted by this list."
#~ msgstr ""
#~ "Αυτή η λίστα λαμβάνει μηνύματα που είναι υπογεγραμένα μόνο από Debian "
#~ "Προγραμματιστή."

#~ msgid "Package"
#~ msgstr "Πακέτο"

#~ msgid ""
#~ "Please respect the <a href=\"./#ads\">Debian mailing list advertising "
#~ "policy</a>."
#~ msgstr ""
#~ "Παρακαλούμε σεβαστείτε την <a href=\"./#ads\">Πολιτική διαφημίσεων "
#~ "συνδρομητικών καταλόγων του Debian</a>."

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Επιλέξτε σε ποιους καταλόγους θέλετε συνδρομή:"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Παρακαλούμε επιλέξτε τους καταλόγους για κατάργηση συνδρομής:"

#~ msgid "Posting messages allowed only to subscribers."
#~ msgstr "Αποστολή μηνυμάτων μόνο από συνδρομητές."

#~ msgid "Q"
#~ msgstr "Ε"

#~ msgid "Rates:"
#~ msgstr "Χρεώσεις:"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription "
#~ "web form</a> is also available, for unsubscribing from mailing lists. "
#~ msgstr ""
#~ "Δείτε τη σελίδα  <a href=\"./#subunsub\">Συνδρομητικοί κατάλογοι</a> για "
#~ "πληροφορίες εγγραφής μέσω ηλ. ταχυδρομείου. Υπάρχει επίσης διαθέσιμη μια "
#~ "σελίδα <a href=\"unsubscribe\">διαγραφής</a> για την κατάργηση συνδρομής. "

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription "
#~ "web form</a> is also available, for subscribing to mailing lists. "
#~ msgstr ""
#~ "Δείτε τη σελίδα <a href=\"./#subunsub\">Συνδρομητικοί κατάλογοι</a> για "
#~ "πληροφορίες διαγραφής μέσω ηλ. ταχυδρομείου. Υπάρχει επίσης διαθέσιμη μια "
#~ "σελίδα <a href=\"subscribe\">εγγραφής</a> για την δημιουργία συνδρομής. "

#~ msgid "Status"
#~ msgstr "Κατάσταση"

#~ msgid "Subscribe"
#~ msgstr "Συνδρομή"

#~ msgid "Subscription:"
#~ msgstr "Συνδρομή:"

#~ msgid "Topics:"
#~ msgstr "Θέματα:"

#~ msgid "URL"
#~ msgstr "Διεύθυνση"

#~ msgid "URL:"
#~ msgstr "Διεύθυνση:"

#~ msgid "Unavailable"
#~ msgstr "Μη διαθέσιμο"

#~ msgid "Unknown"
#~ msgstr "Άγνωστο"

#~ msgid "Unsubscribe"
#~ msgstr "Κατάργηση"

#~ msgid "Version"
#~ msgstr "Έκδοση"

#, fuzzy
#~ msgid "Who:"
#~ msgstr "Τηλέφωνο:"

#~ msgid "Willing to Relocate"
#~ msgstr "Ενδιαφέρεται για μεταφορά"

#~ msgid "Working"
#~ msgstr "Εργάζονται"

#~ msgid "Your E-Mail address:"
#~ msgstr "Η Ηλεκτρονική σας Διεύθυνση:"

#~ msgid "closed"
#~ msgstr "κλειστό"

#~ msgid "developers only"
#~ msgstr "μόνο για προγραμματιστές"

#~ msgid "open"
#~ msgstr "ανοικτό"

#~ msgid "or"
#~ msgstr "ή"

#~ msgid "p<get-var page />"
#~ msgstr "p<get-var page />"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (χαλασμένο)"
