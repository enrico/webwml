msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2010-01-18 08:32+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "Tiedosto"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "Paketti"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "Tilanne"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "Kääntäjä"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "Ryhmä"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "Päiväys"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "Tila"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "Tekstit"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "Vika"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang />, puhealue <get-var country />"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "Tuntematon kieli"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr "Tämä sivu on tuotettu <get-var date /> kerätyistä tiedoista."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr ""
"Ennen kuin työskentelet näiden tiedostojen parissa, varmista että ne ovat "
"ajan tasalla!"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "Osasto: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "Paikallistaminen (l10n)"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "Kielet"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "Sijoitukset"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "Vinkit"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "Virheet"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "POT-tiedostot"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "Vinkkejä kääntäjille"

#~ msgid "Original templates"
#~ msgstr "Alkuperäiset mallit"

#~ msgid "Translated templates"
#~ msgstr "Käännetyt mallit"
