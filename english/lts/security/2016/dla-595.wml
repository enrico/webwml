<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in the dissectors for NDS,
PacketBB, WSP, MMSE, RLC, LDSS, RLC and OpenFlow, which could result
in denial of service or the execution of arbitrary code.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.12.1+g01b65bf-4+deb8u6~deb7u3.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-595.data"
# $Id: $
