<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Bjoern Jacke discovered that Exim, Debian's default mail transfer agent,
may leak the private DKIM signing key to the log files if specific
configuration options are met.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.80-7+deb7u4.</p>

<p>We recommend that you upgrade your exim4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-762.data"
# $Id: $
