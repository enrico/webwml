<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Cure53 discovered that in SimpleSAMLphp, in rare circumstances an
invalid signature on the SAML 2.0 HTTP Redirect binding could be
considered valid.</p>

<p>Additionally this update fixes a regression introduced in DLA-1298
by the backported patch for SSA-201802-01/<a href="https://security-tracker.debian.org/tracker/CVE-2018-7644">CVE-2018-7644</a>.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.9.2-1+deb7u4.</p>

<p>We recommend that you upgrade your simplesamlphp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1314.data"
# $Id: $
