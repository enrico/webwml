<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in GraphicsMagick, the image
processing system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20184">CVE-2018-20184</a>

    <p>The WriteTGAImage function (tga.c) is affected by a heap-based buffer
    overflow. Remote attackers might leverage this vulnerability to cause
    a denial of service via a crafted image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20185">CVE-2018-20185</a>

    <p>The ReadBMPImage function (bmp.c) is affected by a heap-based buffer
    over-read. Remote attackers might leverage this vulnerability to cause
    a denial of service via a crafted image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20189">CVE-2018-20189</a>

    <p>The ReadDIBImage function (coders/dib.c) is affected by an assertion
    error. Remote attackers might leverage this vulnerability to cause
    a denial of service via a crafted image file.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.20-3+deb8u5.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1619.data"
# $Id: $
