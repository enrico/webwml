<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that rubyzip, a Ruby module for reading and writing zip
files, contained a Directory Traversal vulnerability that can be
exploited to write arbitrary files to the filesystem.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.1.6-1+deb8u2.</p>

<p>We recommend that you upgrade your ruby-zip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1467.data"
# $Id: $
