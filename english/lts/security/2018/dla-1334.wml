<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7651">CVE-2017-7651</a>
      <p>A crafted CONNECT packet from an unauthenticated client could
      result in extraordinary memory consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7652">CVE-2017-7652</a>

      <p>In case all sockets/file descriptors are exhausted, a SIGHUP
      signal to reload the configuration could result in default
      config values (especially bad security settings)</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.15-2+deb7u3.</p>

<p>We recommend that you upgrade your mosquitto packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1334.data"
# $Id: $
