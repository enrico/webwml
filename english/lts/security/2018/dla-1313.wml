<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in the ISC DHCP client,
relay and server. The Common Vulnerabilities and Exposures project
identifies the following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5732">CVE-2018-5732</a>

     <p>Felix Wilhelm of the Google Security Team discovered that the DHCP
     client is prone to an out-of-bound memory access vulnerability when
     processing specially constructed DHCP options responses, resulting
     in potential execution of arbitrary code by a malicious DHCP server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5733">CVE-2018-5733</a>

     <p>Felix Wilhelm of the Google Security Team discovered that the DHCP
     server does not properly handle reference counting when processing
     client requests. A malicious client can take advantage of this flaw
     to cause a denial of service (dhcpd crash) by sending large amounts
     of traffic.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.2.2.dfsg.1-5+deb70u9.</p>

<p>We recommend that you upgrade your isc-dhcp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1313.data"
# $Id: $
