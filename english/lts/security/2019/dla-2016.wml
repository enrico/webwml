<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been identified in the VNC code of ssvnc, an
encryption-capable VNC client..</p>

<p>The vulnerabilities referenced below are issues that have originally been
reported against Debian source package libvncserver (which also ships the
libvncclient shared library). The ssvnc source package in Debian ships a
custom-patched, stripped down and outdated variant of libvncclient, thus
some of libvncclient's security fixes required porting over.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20020">CVE-2018-20020</a>

    <p>LibVNC contained heap out-of-bound write vulnerability inside
    structure in VNC client code that can result remote code execution</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20021">CVE-2018-20021</a>

    <p>LibVNC contained a CWE-835: Infinite loop vulnerability in VNC client
    code. Vulnerability allows attacker to consume excessive amount of
    resources like CPU and RAM</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20022">CVE-2018-20022</a>

    <p>LibVNC contained multiple weaknesses CWE-665: Improper Initialization
    vulnerability in VNC client code that allowed attackers to read stack
    memory and could be abused for information disclosure. Combined with
    another vulnerability, it could be used to leak stack memory layout
    and in bypassing ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20024">CVE-2018-20024</a>

    <p>LibVNC contained null pointer dereference in VNC client code that
    could result DoS.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.0.29-2+deb8u1.</p>

<p>We recommend that you upgrade your ssvnc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2016.data"
# $Id: $
