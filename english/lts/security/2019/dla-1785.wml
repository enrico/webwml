<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Numerous security vulnerabilities were fixed in Imagemagick. Various
memory handling problems and cases of missing or incomplete input
sanitizing may result in denial of service, memory or CPU exhaustion,
information disclosure or potentially the execution of arbitrary code
when a malformed image file is processed.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
8:6.8.9.9-5+deb8u16.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1785.data"
# $Id: $
