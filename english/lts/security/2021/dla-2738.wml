<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in c-ares, an asynchronous name resolver.
Missing input validation of host names returned by Domain Name Servers can
lead to output of wrong hostnames.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.12.0-1+deb9u2.</p>

<p>We recommend that you upgrade your c-ares packages.</p>

<p>For the detailed security status of c-ares please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/c-ares">https://security-tracker.debian.org/tracker/c-ares</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2738.data"
# $Id: $
