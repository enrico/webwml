<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in scrollz, an advanced ircII-based IRC client.
A crafted CTCP UTC message could allow an attacker to disconnect the
victim from an IRC server due to a segmentation fault and client crash.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
2.2.3-1+deb9u1.</p>

<p>We recommend that you upgrade your scrollz packages.</p>

<p>For the detailed security status of scrollz please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/scrollz">https://security-tracker.debian.org/tracker/scrollz</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2746.data"
# $Id: $
