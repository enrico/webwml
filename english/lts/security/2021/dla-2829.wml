<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An out-of-bounds buffer read on truncated key frames in vp8_decode_frame
has been fixed in libvpx, a popular library for the VP8 and VP9 video codecs.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.6.1-3+deb9u3.</p>

<p>We recommend that you upgrade your libvpx packages.</p>

<p>For the detailed security status of libvpx please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libvpx">https://security-tracker.debian.org/tracker/libvpx</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2829.data"
# $Id: $
