<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple CVE(s) were discovered in the src:wordpress package.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11026">CVE-2020-11026</a>

    <p>Files with a specially crafted name when uploaded to the
    Media section can lead to script execution upon accessing
    the file. This requires an authenticated user with privileges
    to upload files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11027">CVE-2020-11027</a>

    <p>A password reset link emailed to a user does not expire upon
    changing the user password. Access would be needed to the email
    account of the user by a malicious party for successful execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11028">CVE-2020-11028</a>

    <p>Some private posts, which were previously public, can result in
    unauthenticated disclosure under a specific set of conditions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11029">CVE-2020-11029</a>

    <p>A vulnerability in the stats() method of class-wp-object-cache.php
    can be exploited to execute cross-site scripting (XSS) attacks.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.1.30+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2208.data"
# $Id: $
