<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in ndpi, an extensible deep packet inspection
library. The Oracle protocol dissector contains an heap-based buffer
over-read, which could crash the application that uses this library and
may result in denial of service.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.8-1+deb9u1.</p>

<p>We recommend that you upgrade your ndpi packages.</p>

<p>For the detailed security status of ndpi please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ndpi">https://security-tracker.debian.org/tracker/ndpi</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2354.data"
# $Id: $
