<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the OpenJDK Java runtime,
resulting in denial of service, bypass of sandbox restrictions or
information disclosure.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
8u272-b10-0+deb9u1.</p>

<p>We recommend that you upgrade your openjdk-8 packages.</p>

<p>For the detailed security status of openjdk-8 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openjdk-8">https://security-tracker.debian.org/tracker/openjdk-8</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2412.data"
# $Id: $
