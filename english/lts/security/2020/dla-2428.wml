<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple buffer overflow vulnerabilities were found in the QUIC
image decoding process of the SPICE remote display system.</p>

<p>Both the SPICE client (spice-gtk) and server are affected by
these flaws. These flaws allow a malicious client or server to
send specially crafted messages that, when processed by the
QUIC image compression algorithm, result in a process crash or
potential code execution.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.33-3.3+deb9u2.</p>

<p>We recommend that you upgrade your spice-gtk packages.</p>

<p>For the detailed security status of spice-gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/spice-gtk">https://security-tracker.debian.org/tracker/spice-gtk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2428.data"
# $Id: $
