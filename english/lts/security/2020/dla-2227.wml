<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in BIND, a DNS server
implementation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8616">CVE-2020-8616</a>

     <p>It was discovered that BIND does not sufficiently limit the number
     of fetches performed when processing referrals. An attacker can take
     advantage of this flaw to cause a denial of service (performance
     degradation) or use the recursing server in a reflection attack with
     a high amplification factor.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8617">CVE-2020-8617</a>

     <p>It was discovered that a logic error in the code which checks TSIG
     validity can be used to trigger an assertion failure, resulting in
     denial of service.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:9.9.5.dfsg-9+deb8u19.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2227.data"
# $Id: $
