<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a denial of service vulnerability in the
MIT Kerberos network authentication system, <tt>krb5</tt>. The lack of a limit
in the ASN.1 decoder could lead to infinite recursion and allow an attacker to
overrun the stack and cause the process to crash.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28196">CVE-2020-28196</a></li>
</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.15-1+deb9u2.</p>

<p>We recommend that you upgrade your krb5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2437.data"
# $Id: $
