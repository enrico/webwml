<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3640">CVE-2021-3640</a>

    <p>LinMa of BlockSec Team discovered a race condition in the
    Bluetooth SCO implementation that can lead to a use-after-free.  A
    local user could exploit this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3752">CVE-2021-3752</a>

    <p>Likang Luo of NSFOCUS Security Team discovered a flaw in the
    Bluetooth L2CAP implementation that can lead to a user-after-free.
    A local user could exploit this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4002">CVE-2021-4002</a>

    <p>It was discovered that hugetlbfs, the virtual filesystem used by
    applications to allocate huge pages in RAM, did not flush the
    CPU's TLB in one case where it was necessary.  In some
    circumstances a local user would be able to read and write huge
    pages after they are freed and reallocated to a different process.
    This could lead to privilege escalation, denial of service or
    information leaks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4083">CVE-2021-4083</a>

    <p>Jann Horn reported a race condition in the local (Unix) sockets
    garbage collector, that can lead to use-after-free.  A local user
    could exploit this to cause a denial of service (memory corruption
    or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4155">CVE-2021-4155</a>

    <p>Kirill Tkhai discovered a data leak in the way the XFS_IOC_ALLOCSP
    IOCTL in the XFS filesystem allowed for a size increase of files
    with unaligned size. A local attacker can take advantage of this
    flaw to leak data on the XFS filesystem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4202">CVE-2021-4202</a>

    <p>Lin Ma discovered a race condition in the NCI (NFC Controller
    Interface) driver, which could lead to a use-after-free.  A local
    user could exploit this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.</p>

    <p>This protocol is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28711">CVE-2021-28711</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28712">CVE-2021-28712</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28713">CVE-2021-28713</a> (XSA-391)</p>

    <p>Juergen Gross reported that malicious PV backends can cause a denial
    of service to guests being serviced by those backends via high
    frequency events, even if those backends are running in a less
    privileged environment.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28714">CVE-2021-28714</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28715">CVE-2021-28715</a> (XSA-392)</p>

    <p>Juergen Gross discovered that Xen guests can force the Linux
    netback driver to hog large amounts of kernel memory, resulting in
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29264">CVE-2021-29264</a>

    <p>It was discovered that the <q>gianfar</q> Ethernet driver used with
    some Freescale SoCs did not correctly handle a Rx queue overrun
    when jumbo packets were enabled.  On systems using this driver and
    jumbo packets, an attacker on the network could exploit this to
    cause a denial of service (crash).</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33033">CVE-2021-33033</a>

    <p>The syzbot tool found a reference counting bug in the CIPSO
    implementation that can lead to a use-after-free.</p>

    <p>This protocol is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39685">CVE-2021-39685</a>

    <p>Szymon Heidrich discovered a buffer overflow vulnerability in the
    USB gadget subsystem, resulting in information disclosure, denial of
    service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39686">CVE-2021-39686</a>

    <p>A race condition was discovered in the Android binder driver, that
    could lead to incorrect security checks.  On systems where the
    binder driver is loaded, a local user could exploit this for
    privilege escalation.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39698">CVE-2021-39698</a>

    <p>Linus Torvalds reported a flaw in the file polling implementation,
    which could lead to a use-after-free.  A local user could exploit
    this for denial of service (memory corruption or crash) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39714">CVE-2021-39714</a>

    <p>A potential reference count overflow was found in the Android Ion
    driver.  On systems where the Ion driver is loaded, a local user
    could exploit this for denial of service (memory corruption or
    crash) or possibly for privilege escalation.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43976">CVE-2021-43976</a>

    <p>Zekun Shen and Brendan Dolan-Gavitt discovered a flaw in the
    mwifiex_usb_recv() function of the Marvell WiFi-Ex USB Driver. An
    attacker able to connect a crafted USB device can take advantage of
    this flaw to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45095">CVE-2021-45095</a>

    <p>It was discovered that the Phone Network protocol (PhoNet) driver
    has a reference count leak in the pep_sock_accept() function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a>

<p>(INTEL-SA-00598)</p>

    <p>Researchers at VUSec discovered that the Branch History Buffer in
    Intel processors can be exploited to create information side    channels with speculative execution.  This issue is similar to
    Spectre variant 2, but requires additional mitigations on some
    processors.</p>

    <p>This can be exploited to obtain sensitive information from a
    different security context, such as from user-space to the kernel,
    or from a KVM guest to the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0002">CVE-2022-0002</a>

<p>(INTEL-SA-00598)</p>

    <p>This is a similar issue to <a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a>, but covers exploitation
    within a security context, such as from JIT-compiled code in a
    sandbox to hosting code in the same process.</p>

    <p>This can be partly mitigated by disabling eBPF for unprivileged
    users with the sysctl: kernel.unprivileged_bpf_disabled=2.  This
    update does that by default.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0330">CVE-2022-0330</a>

    <p>Sushma Venkatesh Reddy discovered a missing GPU TLB flush in the
    i915 driver, resulting in denial of service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0435">CVE-2022-0435</a>

    <p>Samuel Page and Eric Dumazet reported a stack overflow in the
    networking module for the Transparent Inter-Process Communication
    (TIPC) protocol, resulting in denial of service or potentially the
    execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0487">CVE-2022-0487</a>

    <p>A use-after-free was discovered in the MOXART SD/MMC Host Controller
    support driver. This flaw does not impact the Debian binary packages
    as CONFIG_MMC_MOXART is not set.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0492">CVE-2022-0492</a>

    <p>Yiqi Sun and Kevin Wang reported that the cgroup-v1 subsystem does
    not properly restrict access to the release-agent feature. A local
    user can take advantage of this flaw for privilege escalation and
    bypass of namespace isolation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0617">CVE-2022-0617</a>

    <p>butt3rflyh4ck discovered a NULL pointer dereference in the UDF
    filesystem. A local user that can mount a specially crafted UDF
    image can use this flaw to crash the system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24448">CVE-2022-24448</a>

    <p>Lyu Tao reported a flaw in the NFS implementation in the Linux
    kernel when handling requests to open a directory on a regular file,
    which could result in a information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25258">CVE-2022-25258</a>

    <p>Szymon Heidrich reported the USB Gadget subsystem lacks certain
    validation of interface OS descriptor requests, resulting in memory
    corruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25375">CVE-2022-25375</a>

    <p>Szymon Heidrich reported that the RNDIS USB gadget lacks validation
    of the size of the RNDIS_MSG_SET command, resulting in information
    leak from kernel memory.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.9.303-1.  This update additionally includes many more bug fixes from
stable updates 4.9.291-4.9.303 inclusive.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2940.data"
# $Id: $
