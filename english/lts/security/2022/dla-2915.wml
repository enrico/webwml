<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were found in ConnMan, a connection manager for embedded
devices, that could cause denial of service via service crash or excessive
CPU usage.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.33-3+deb9u3.</p>

<p>We recommend that you upgrade your connman packages.</p>

<p>For the detailed security status of connman please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/connman">https://security-tracker.debian.org/tracker/connman</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2915.data"
# $Id: $
