<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A flaw was found in usbguard, an USB device authorization policy framework.
When using the usbguard-dbus daemon an unprivileged user could make USBGuard
allow all USB devices to be connected in the future.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.6.2+ds1-2+deb9u1.</p>

<p>We recommend that you upgrade your usbguard packages.</p>

<p>For the detailed security status of usbguard please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/usbguard">https://security-tracker.debian.org/tracker/usbguard</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2979.data"
# $Id: $
