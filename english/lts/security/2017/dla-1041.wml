<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10686">CVE-2017-10686</a>

      <p>In Netwide Assembler (NASM) 2.14rc0, there are multiple heap use
      after free vulnerabilities in the tool nasm. The related heap is
      allocated in the token() function and freed in the detoken()
      function (called by pp_getline()) - it is used again at multiple
      positions later that could cause multiple damages. For example,
      it causes a corrupted double-linked list in detoken(), a double
      free or corruption in delete_Token(), and an out-of-bounds write
      in detoken(). It has a high possibility to lead to a remote code
      execution attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11111">CVE-2017-11111</a>

      <p>In Netwide Assembler (NASM) 2.14rc0, preproc.c allows remote
      attackers to cause a denial of service (heap-based buffer
      overflow and application crash) or possibly have unspecified
      other impact via a crafted file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.10.01-1+deb7u1.</p>

<p>We recommend that you upgrade your nasm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1041.data"
# $Id: $
