<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>joernchen discovered that the git-cvsserver subcommand of Git, a
distributed version control system, suffers from a shell command
injection vulnerability due to unsafe use of the Perl backtick
operator.  The git-cvsserver subcommand is reachable from the
git-shell subcommand even if CVS support has not been configured
(however, the git-cvs package needs to be installed).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.7.10.4-1+wheezy6.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1120.data"
# $Id: $
