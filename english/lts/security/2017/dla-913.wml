<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that Apache ActiveMQ exposed a remote shutdown command in
the ActiveMQConnection class. An attacker could use this flaw to
achieve denial of service on a client.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.6.0+dfsg-1+deb7u3.</p>

<p>We recommend that you upgrade your activemq packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-913.data"
# $Id: $
