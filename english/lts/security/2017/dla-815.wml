<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jann Horn of Google Project Zero discovered that NTFS-3G, a read-write
NTFS driver for FUSE, does not scrub the environment before executing
modprobe with elevated privileges. A local user can take advantage of
this flaw for local root privilege escalation.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:2012.1.15AR.5-2.1+deb7u3.</p>

<p>We recommend that you upgrade your ntfs-3g packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-815.data"
# $Id: $
