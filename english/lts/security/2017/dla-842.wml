<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in qemu-kvm, a full
virtualization solution for Linux hosts on x86 hardware with x86 guests.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2615">CVE-2017-2615</a>

    <p>The Cirrus CLGD 54xx VGA Emulator in qemu-kvm is vulnerable to an
    out-of-bounds access issue. It could occur while copying VGA data
    via bitblt copy in backward mode.</p>

    <p>A privileged user inside guest could use this flaw to crash the
    Qemu process resulting in DoS OR potentially execute arbitrary
    code on the host with privileges of qemu-kvm process on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2620">CVE-2017-2620</a>

    <p>The Cirrus CLGD 54xx VGA Emulator in qemu-kvm is vulnerable to an
    out-of-bounds access issue. It could occur while copying VGA data
    in cirrus_bitblt_cputovideo.</p>

    <p>A privileged user inside guest could use this flaw to crash the
    Qemu process resulting in DoS OR potentially execute arbitrary
    code on the host with privileges of qemu-kvm process on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5898">CVE-2017-5898</a>

    <p>The CCID Card device emulator support is vulnerable to an integer
    overflow flaw. It could occur while passing message via
    command/responses packets to and from the host.</p>

    <p>A privileged user inside guest could use this flaw to crash the
    qemu-kvm process on the host resulting in a DoS.</p>

    <p>This issue does not affect the qemu-kvm binaries in Debian but we
    apply the patch to the sources to stay in sync with the qemu
    package.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5973">CVE-2017-5973</a>

    <p>The USB xHCI controller emulator support in qemu-kvm is vulnerable
    to an infinite loop issue. It could occur while processing control
    transfer descriptors' sequence in xhci_kick_epctx.</p>

    <p>A privileged user inside guest could use this flaw to crash the
    qemu-kvm process resulting in a DoS.</p></li>

</ul>

<p>This update also updates the fix <a href="https://security-tracker.debian.org/tracker/CVE-2016-9921">CVE-2016-9921</a>
since it was too strict and broke certain guests.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u20.</p>

<p>We recommend that you upgrade your qemu-kvm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-842.data"
# $Id: $
