<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were two vulnerabilities in python-django, a
high-level Python web development framework.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7233">CVE-2017-7233</a> (#859515)

<p>Open redirect and possible XSS attack via
user-supplied numeric redirect URLs. Django relies on user input in some cases
(e.g. django.contrib.auth.views.login() and i18n) to redirect the user to an
<q>on success</q> URL. The security check for these redirects (namely is_safe_url())
considered some numeric URLs (e.g.  http:999999999) <q>safe</q> when they shouldn't
be. Also, if a developer relied on is_safe_url() to provide safe redirect
targets and puts such a URL into a link, they could suffer from an XSS attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7234">CVE-2017-7234</a> (#895516)

<p>Open redirect vulnerability in
django.views.static.serve; A maliciously crafted URL to a Django site using the
serve() view could redirect to any other domain. The view no longer does any
redirects as they don't provide any known, useful functionality.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in python-django version
1.4.22-1+deb7u3.</p>

<p>We recommend that you upgrade your python-django packages.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-885.data"
# $Id: $
