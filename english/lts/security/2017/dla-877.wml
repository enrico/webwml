<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>libtiff is vulnerable to multiple buffer overflows and integer overflows
that can lead to application crashes (denial of service) or worse.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10266">CVE-2016-10266</a>

    <p>Integer overflow that can lead to divide-by-zero in
    TIFFReadEncodedStrip (tif_read.c).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10267">CVE-2016-10267</a>

    <p>Divide-by-zero error in OJPEGDecodeRaw (tif_ojpeg.c).
    
<a href="https://security-tracker.debian.org/tracker/CVE-2016-10268">CVE-2016-10268</a></p>

    <p>Heap-based buffer overflow in TIFFReverseBits (tif_swab.c).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10269">CVE-2016-10269</a>

    <p>Heap-based buffer overflow in _TIFFmemcpy (tif_unix.c).</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u11.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-877.data"
# $Id: $
