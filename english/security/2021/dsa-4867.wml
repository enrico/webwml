<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the GRUB2 bootloader.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14372">CVE-2020-14372</a>

    <p>It was discovered that the acpi command allows a privileged user to
    load crafted ACPI tables when Secure Boot is enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25632">CVE-2020-25632</a>

    <p>A use-after-free vulnerability was found in the rmmod command.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25647">CVE-2020-25647</a>

    <p>An out-of-bound write vulnerability was found in the
    grub_usb_device_initialize() function, which is called to handle USB
    device initialization.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27749">CVE-2020-27749</a>

    <p>A stack buffer overflow flaw was found in grub_parser_split_cmdline.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27779">CVE-2020-27779</a>

    <p>It was discovered that the cutmem command allows a privileged user
    to remove memory regions when Secure Boot is enabled.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20225">CVE-2021-20225</a>

    <p>A heap out-of-bounds write vulnerability was found in the short form
    option parser.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20233">CVE-2021-20233</a>

    <p>A heap out-of-bound write flaw was found caused by mis-calculation
    of space required for quoting in the menu rendering.</p></li>

</ul>

<p>Further detailed information can be found at
<a href="https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot">https://www.debian.org/security/2021-GRUB-UEFI-SecureBoot</a></p>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.02+dfsg1-20+deb10u4.</p>

<p>We recommend that you upgrade your grub2 packages.</p>

<p>For the detailed security status of grub2 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/grub2">https://security-tracker.debian.org/tracker/grub2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4867.data"
# $Id: $
