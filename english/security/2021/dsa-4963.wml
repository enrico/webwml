<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in OpenSSL, a Secure
Sockets Layer toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3711">CVE-2021-3711</a>

    <p>John Ouyang reported a buffer overflow vulnerability in the SM2
    decryption. An attacker able to present SM2 content for
    decryption to an application can take advantage of this flaw to
    change application behaviour or cause the application to crash
    (denial of service).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3712">CVE-2021-3712</a>

    <p>Ingo Schwarze reported a buffer overrun flaw when processing ASN.1
    strings in the X509_aux_print() function, which can result in denial
    of service.</p></li>

</ul>

<p>Additional details can be found in the upstream advisory:
<a href="https://www.openssl.org/news/secadv/20210824.txt">https://www.openssl.org/news/secadv/20210824.txt</a></p>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 1.1.1d-0+deb10u7.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 1.1.1k-1+deb11u1.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4963.data"
# $Id: $
