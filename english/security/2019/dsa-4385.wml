<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>halfdog discovered an authentication bypass vulnerability in the Dovecot
email server. Under some configurations Dovecot mistakenly trusts the
username provided via authentication instead of failing. If there is no
additional password verification, this allows the attacker to login as
anyone else in the system. Only installations using:</p>

<ul>
<li>auth_ssl_require_client_cert = yes</li>
<li>auth_ssl_username_from_cert = yes</li>
</ul>

<p>are affected by this flaw.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 1:2.2.27-3+deb9u3.</p>

<p>We recommend that you upgrade your dovecot packages.</p>

<p>For the detailed security status of dovecot please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4385.data"
# $Id: $
