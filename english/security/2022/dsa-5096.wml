<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29374">CVE-2020-29374</a>

    <p>Jann Horn of Google reported a flaw in Linux's virtual memory
    management.  A parent and child process initially share all their
    memory, but when either writes to a shared page, the page is
    duplicated and unshared (copy-on-write).  However, in case an
    operation such as vmsplice() required the kernel to take an
    additional reference to a shared page, and a copy-on-write occurs
    during this operation, the kernel might have accessed the wrong
    process's memory.  For some programs, this could lead to an
    information leak or data corruption.</p>

    <p>This issue was already fixed for most architectures, but not on
    MIPS and System z.  This update corrects that.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36322">CVE-2020-36322</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28950">CVE-2021-28950</a>

    <p>The syzbot tool found that the FUSE (filesystem-in-user-space)
    implementation did not correctly handle a FUSE server returning
    invalid attributes for a file.  A local user permitted to run a
    FUSE server could use this to cause a denial of service (crash).</p>

    <p>The original fix for this introduced a different potential denial
    of service (infinite loop in kernel space), which has also been
    fixed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3640">CVE-2021-3640</a>

    <p>Lin Ma discovered a race condiiton in the Bluetooth protocol
    implementation that can lead to a use-after-free.  A local
    user could exploit this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3744">CVE-2021-3744</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-3764">CVE-2021-3764</a>

    <p>minihanshen reported bugs in the ccp driver for AMD
    Cryptographic Coprocessors that could lead to a resource leak.
    On systems using this driver, a local user could exploit this to
    cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3752">CVE-2021-3752</a>

    <p>Likang Luo of NSFOCUS Security Team discovered a flaw in the
    Bluetooth L2CAP implementation that can lead to a user-after-free.
    A local user could exploit this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3760">CVE-2021-3760</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-4202">CVE-2021-4202</a>

    <p>Lin Ma discovered race conditions in the NCI (NFC Controller
    Interface) driver, which could lead to a use-after-free.  A local
    user could exploit this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3772">CVE-2021-3772</a>

    <p>A flaw was found in the SCTP protocol implementation, which would
    allow a networked attacker to break an SCTP association.  The
    attacker would only need to know or guess the IP addresses and
    ports for the association.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4002">CVE-2021-4002</a>

    <p>It was discovered that hugetlbfs, the virtual filesystem used by
    applications to allocate huge pages in RAM, did not flush the
    CPU's TLB in one case where it was necessary.  In some
    circumstances a local user would be able to read and write huge
    pages after they are freed and reallocated to a different process.
    This could lead to privilege escalation, denial of service or
    information leaks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4083">CVE-2021-4083</a>

    <p>Jann Horn reported a race condition in the local (Unix) sockets
    garbage collector, that can lead to use-after-free.  A local user
    could exploit this to cause a denial of service (memory corruption
    or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4135">CVE-2021-4135</a>

    <p>A flaw was found in the netdevsim driver which would lead to an
    information leak.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4155">CVE-2021-4155</a>

    <p>Kirill Tkhai discovered a data leak in the way the XFS_IOC_ALLOCSP
    IOCTL in the XFS filesystem allowed for a size increase of files
    with unaligned size. A local attacker can take advantage of this
    flaw to leak data on the XFS filesystem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4203">CVE-2021-4203</a>

    <p>Jann Horn reported a race condition in the local (Unix) sockets
    implementation that can lead to a use-after-free.  A local user
    could exploit this to leak sensitive information from the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20317">CVE-2021-20317</a>

    <p>It was discovered that the timer queue structure could become
    corrupt, leading to waiting tasks never being woken up.  A local
    user with certain privileges could exploit this to cause a denial
    of service (system hang).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20321">CVE-2021-20321</a>

    <p>A race condition was discovered in the overlayfs filesystem
    driver.  A local user with access to an overlayfs mount and to its
    underlying upper directory could exploit this for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20322">CVE-2021-20322</a>

    <p>An information leak was discovered in the IPv4 implementation.  A
    remote attacker could exploit this to quickly discover which UDP
    ports a system is using, making it easier for them to carry out a
    DNS poisoning attack against that system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22600">CVE-2021-22600</a>

    <p>The syzbot tool found a flaw in the packet socket (AF_PACKET)
    implementation which could lead to incorrectly freeing memory.  A
    local user with CAP_NET_RAW capability (in any user namespace)
    could exploit this for denial of service (memory corruption or
    crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28711">CVE-2021-28711</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28712">CVE-2021-28712</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28713">CVE-2021-28713</a> (XSA-391)

    <p>Juergen Gross reported that malicious PV backends can cause a denial
    of service to guests being serviced by those backends via high
    frequency events, even if those backends are running in a less
    privileged environment.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28714">CVE-2021-28714</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2021-28715">CVE-2021-28715</a> (XSA-392)

    <p>Juergen Gross discovered that Xen guests can force the Linux
    netback driver to hog large amounts of kernel memory, resulting in
    denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38300">CVE-2021-38300</a>

    <p>Piotr Krysiuk discovered a flaw in the classic BPF (cBPF) JIT
    compiler for MIPS architectures.  A local user could exploit
    this to excute arbitrary code in the kernel.</p>

    <p>This issue is mitigated by setting sysctl
    net.core.bpf_jit_enable=0, which is the default.  It is *not*
    mitigated by disabling unprivileged use of eBPF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39685">CVE-2021-39685</a>

    <p>Szymon Heidrich discovered a buffer overflow vulnerability in the
    USB gadget subsystem, resulting in information disclosure, denial of
    service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39686">CVE-2021-39686</a>

    <p>A race condition was discovered in the Android binder driver, that
    could lead to incorrect security checks.  On systems where the
    binder driver is loaded, a local user could exploit this for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39698">CVE-2021-39698</a>

    <p>Linus Torvalds reported a flaw in the file polling implementation,
    which could lead to a use-after-free.  A local user could exploit
    this for denial of service (memory corruption or crash) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39713">CVE-2021-39713</a>

    <p>The syzbot tool found a race condition in the network scheduling
    subsystem which could lead to a use-after-free.  A local user
    could exploit this for denial of service (memory corruption or
    crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41864">CVE-2021-41864</a>

    <p>An integer overflow was discovered in the Extended BPF (eBPF)
    subsystem.  A local user could exploit this for denial of service
    (memory corruption or crash), or possibly for privilege
    escalation.</p>

    <p>This can be mitigated by setting sysctl
    kernel.unprivileged_bpf_disabled=1, which disables eBPF use by
    unprivileged users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42739">CVE-2021-42739</a>

    <p>A heap buffer overflow was discovered in the firedtv driver for
    FireWire-connected DVB receivers.  A local user with access to a
    firedtv device could exploit this for denial of service (memory
    corruption or crash), or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43389">CVE-2021-43389</a>

    <p>The Active Defense Lab of Venustech discovered a flaw in the CMTP
    subsystem as used by Bluetooth, which could lead to an
    out-of-bounds read and object type confusion.  A local user with
    CAP_NET_ADMIN capability in the initial user namespace could
    exploit this for denial of service (memory corruption or crash),
    or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43975">CVE-2021-43975</a>

    <p>Brendan Dolan-Gavitt reported a flaw in the
    hw_atl_utils_fw_rpc_wait() function in the aQuantia AQtion ethernet
    device driver which can result in denial of service or the execution
    of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43976">CVE-2021-43976</a>

    <p>Zekun Shen and Brendan Dolan-Gavitt discovered a flaw in the
    mwifiex_usb_recv() function of the Marvell WiFi-Ex USB Driver. An
    attacker able to connect a crafted USB device can take advantage of
    this flaw to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44733">CVE-2021-44733</a>

    <p>A race condition was discovered in the Trusted Execution
    Environment (TEE) subsystem for Arm processors, which could lead
    to a use-after-free.  A local user permitted to access a TEE
    device could exploit this for denial of service (memory corruption
    or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45095">CVE-2021-45095</a>

    <p>It was discovered that the Phone Network protocol (PhoNet) driver
    has a reference count leak in the pep_sock_accept() function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45469">CVE-2021-45469</a>

    <p>Wenqing Liu reported an out-of-bounds memory access in the f2fs
    implementation if an inode has an invalid last xattr entry. An
    attacker able to mount a specially crafted image can take advantage
    of this flaw for denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45480">CVE-2021-45480</a>

    <p>A memory leak flaw was discovered in the __rds_conn_create()
    function in the RDS (Reliable Datagram Sockets) protocol subsystem.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a> (INTEL-SA-00598)

    <p>Researchers at VUSec discovered that the Branch History Buffer in
    Intel processors can be exploited to create information side
    channels with speculative execution.  This issue is similar to
    Spectre variant 2, but requires additional mitigations on some
    processors.</p>

    <p>This can be exploited to obtain sensitive information from a
    different security context, such as from user-space to the kernel,
    or from a KVM guest to the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0002">CVE-2022-0002</a> (INTEL-SA-00598)

    <p>This is a similar issue to
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">\
    CVE-2022-0001</a>, but covers exploitation
    within a security context, such as from JIT-compiled code in a
    sandbox to hosting code in the same process.</p>

    <p>This can be partly mitigated by disabling eBPF for unprivileged
    users with the sysctl: kernel.unprivileged_bpf_disabled=2.  This
    update does that by default.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0322">CVE-2022-0322</a>

    <p>Eiichi Tsukata discovered a flaw in the sctp_make_strreset_req()
    function in the SCTP network protocol implementation which can
    result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0330">CVE-2022-0330</a>

    <p>Sushma Venkatesh Reddy discovered a missing GPU TLB flush in the
    i915 driver, resulting in denial of service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0435">CVE-2022-0435</a>

    <p>Samuel Page and Eric Dumazet reported a stack overflow in the
    networking module for the Transparent Inter-Process Communication
    (TIPC) protocol, resulting in denial of service or potentially the
    execution of arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0487">CVE-2022-0487</a>

    <p>A use-after-free was discovered in the MOXART SD/MMC Host Controller
    support driver. This flaw does not impact the Debian binary packages
    as CONFIG_MMC_MOXART is not set.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0492">CVE-2022-0492</a>

    <p>Yiqi Sun and Kevin Wang reported that the cgroup-v1 subsystem does
    not properly restrict access to the release-agent feature. A local
    user can take advantage of this flaw for privilege escalation and
    bypass of namespace isolation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0617">CVE-2022-0617</a>

    <p>butt3rflyh4ck discovered a NULL pointer dereference in the UDF
    filesystem. A local user that can mount a specially crafted UDF
    image can use this flaw to crash the system.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0644">CVE-2022-0644</a>

    <p>Hao Sun reported a missing check for file read permission in the
    finit_module() and kexec_file_load() system calls.  The security
    impact of this is unclear, since these system calls are usually
    only available to the root user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22942">CVE-2022-22942</a>

    <p>It was discovered that wrong file file descriptor handling in the
    VMware Virtual GPU driver (vmwgfx) could result in information leak
    or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24448">CVE-2022-24448</a>

    <p>Lyu Tao reported a flaw in the NFS implementation in the Linux
    kernel when handling requests to open a directory on a regular file,
    which could result in a information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24959">CVE-2022-24959</a>

    <p>A memory leak was discovered in the yam_siocdevprivate() function of
    the YAM driver for AX.25, which could result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25258">CVE-2022-25258</a>

    <p>Szymon Heidrich reported the USB Gadget subsystem lacks certain
    validation of interface OS descriptor requests, resulting in memory
    corruption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25375">CVE-2022-25375</a>

    <p>Szymon Heidrich reported that the RNDIS USB gadget lacks validation
    of the size of the RNDIS_MSG_SET command, resulting in information
    leak from kernel memory.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been
fixed in version 4.19.232-1.  This update additionally includes many
more bug fixes from stable updates 4.19.209-4.19.232 inclusive.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5096.data"
# $Id: $
