# cheat sheet for tags

# "naming": If unset, set "naming" to "default" and used for listing automatically
# naming="none" used not listing at all
# naming="locale" used for listing automatically (see below)
# naming="locale" (used in debian-faq and refcard) provides correct
# pt_BR naming from pt-br declared langs, for every built files, as
# discussed in <20100406221005.GB15316@dedibox.ebzao.info>, but
# keeps pt-br fot html files to comply with Apache rewriting.

# formats: see manuals.defs for available ones
# langs: If unset, file extention becomes .html instead of .en.html
# vcsname: set "vcsname" when the DDP VCS repo name is not same as "name"

# vcstype="ddp": DDP git site (no need for vcsweb and vcsrepo)
# vcstype="git": Generic git site

# vcsweb: URL for vcsweb site etc.
# vcsrepo: repository checkout URL (anonymous R-only one)

<define-tag inddpvcs-debian-faq>
  <inddpvcs name="debian-faq"
            naming="locale"
            langs="en de fr it ja nl pt ru zh-cn"
            formats="html txt pdf"
            srctype="XML"
            vcstype="ddp"
            />
</define-tag>

<define-tag inddpvcs-installation-guide >
  <inddpvcs name="installation-guide "
            naming="none"
            srctype="XML"
            vcstype="git"
            vcsweb="https://salsa.debian.org/installer-team/installation-guide"
            vcsrepo="https://salsa.debian.org/installer-team/installation-guide.git"
            />
</define-tag>

<define-tag inddpvcs-release-notes>
  <inddpvcs name="release-notes"
            naming="none"
            srctype="XML"
            vcstype="ddp"
            />
</define-tag>

<define-tag inddpvcs-debian-reference>
  <inddpvcs name="debian-reference" vcsname="quick-reference"
            langs="en fr de es it ja pt zh-cn zh-tw"
            formats="html txt pdf"
            srctype="XML"
            vcstype="git"
            vcsweb="https://salsa.debian.org/debian/debian-reference"
            vcsrepo="https://salsa.debian.org/debian/debian-reference.git"
            />
</define-tag>

<define-tag inddpvcs-refcard>
  <inddpvcs name="refcard"
            naming="locale"
            langs="ar bg ca cs da de el en es eu fi fr gl he hi hu it ja ko lt nb nl pl pt-br pt ro ru sk sv tr vi zh-cn zh-tw"
            formats="pdf"
            srctype="XML"
            vcstype="ddp"
            />
</define-tag>

<define-tag inddpvcs-debian-handbook>
  <inddpvcs name="debian-handbook"
            langs="ar da de el en es fa fr hr id it ja nb pl pt-br ro ru tr vi zh-cn"
            formats="html"
            srctype="XML"
            vcstype="git"
            vcsweb="https://salsa.debian.org/hertzog/debian-handbook"
            vcsrepo="https://salsa.debian.org/hertzog/debian-handbook.git"
            />
</define-tag>

<define-tag inddpvcs-aptitude>
  <inddpvcs name="aptitude"
            langs="cs en es fi fr it ja ru"
            formats="html"
            srctype="XML"
            vcstype="git"
            vcsweb="https://salsa.debian.org/apt-team/aptitude"
            vcsrepo="https://salsa.debian.org/apt-team/aptitude.git"
            />
</define-tag>

<define-tag inddpvcs-apt-guide>
  <inddpvcs name="apt-guide"
            langs="de es fr en it ja pl pt"
#    Commenting out the txt variant. The package does not include txt variants
#    for all languages (*.en.txt is missing).
#    formats="html txt"
            formats="html"
            srctype="XML"
            vcstype="git"
            vcsweb="https://salsa.debian.org/apt-team/apt"
            vcsrepo="https://salsa.debian.org/apt-team/apt.git"
            />
</define-tag>

<define-tag inddpvcs-apt-offline>
  <inddpvcs name="apt-offline"
            langs="de es fr en it ja pl pt"
#    Commenting out the txt variant. The package does not include txt variants
#    for all languages (*.en.txt is missing).
#    formats="html txt"
            formats="html"
            srctype="XML"
            vcstype="git"
            vcsweb="https://salsa.debian.org/apt-team/apt"
            vcsrepo="https://salsa.debian.org/apt-team/apt.git"
            />
</define-tag>

# Pending confirmation with java-common maintainers if we use
# DDP SVN or java-common's SVN available at:
# https://anonscm.debian.org/viewvc/pkg-java/trunk/java-common/debian-java-faq/
# FIXME SVN not available now
<define-tag inddpvcs-debian-java-faq>
  <inddpvcs name="debian-java-faq"
        langs="en it fr"
        formats="html txt"
        srctype="SGML"
        vcsname="java-faq"
        vcstype="ddp"
        />
</define-tag>

<define-tag inddpvcs-securing-debian-manual>
  <inddpvcs name="securing-debian-manual"
            langs="en de es fr it ja pt-br zh-cn"
            formats="html"
            srctype="XML"
            vcstype="git"
            vcsweb="https://salsa.debian.org/ddp-team/securing-debian-manual/"
            vcsrepo="https://salsa.debian.org/ddp-team/securing-debian-manual.git"
            />
</define-tag>

<define-tag inddpvcs-hamradio-maintguide>
  <inddpvcs name="hamradio-maintguide"
            langs="en"
            formats="html pdf"
            srctype="restructuredText"
            vcstype="git"
            vcsweb="https://salsa.debian.org/debian-hamradio-team/hamradio-maintguide/"
            vcsrepo="https://salsa.debian.org/debian-hamradio-team/hamradio-maintguide.git"
            />
</define-tag>
