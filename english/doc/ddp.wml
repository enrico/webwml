#use wml::debian::ddp title="Debian Documentation Project"

<p>The Debian Documentation Project was formed to coordinate and unify all
efforts to write more and better documentation for the Debian system.</p>

  <h2>DDP Work</h2>
<div class="line">
  <div class="item col50">

    <h3>Manuals</h3>
    <ul>
      <li><strong><a href="user-manuals">Users' manuals</a></strong></li>
      <li><strong><a href="devel-manuals">Developers' manuals</a></strong></li>
      <li><strong><a href="misc-manuals">Miscellaneous manuals</a></strong></li>
      <li><strong><a href="#other">Problematic manuals</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3>Documentation Policy</h3>
    <ul>
      <li>Manual licenses comply with DFSG.</li>
      <li>We use Docbook XML for our documents.</li>
      <li>The sources should be at <a href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a></li>
      <li><tt>www.debian.org/doc/&lt;manual-name&gt;</tt> will be the official URL</li>
      <li>Every document should be actively maintained.</li>
      <li>Please ask on <a href="https://lists.debian.org/debian-doc/">debian-doc</a> if you like to write a new document</li>
    </ul>

    <h3>Git Access</h3>
    <ul>
      <li><a href="vcs">How to access</a> the git repositories of the DDP</li>
    </ul>

  </div>


</div>

<hr />

<h2><a name="other">Problematic manuals</a></h2>

<p>In addition to the normally advertized manuals, we maintain the following
manuals that
are problematic in one way or another, so we can't recommend them to all
users. Caveat emptor.</p>

<ul>
  <li><a href="obsolete#tutorial">Debian Tutorial</a>, obsolete</li>
  <li><a href="obsolete#guide">Debian Guide</a>, obsolete</li>
  <li><a href="obsolete#userref">Debian User Reference Manual</a>,
      stalled and quite incomplete</li>
  <li><a href="obsolete#system">Debian System Administrator's
      Manual</a>, stalled, almost empty</li>
  <li><a href="obsolete#network">Debian Network Administrator's
      Manual</a>, stalled, incomplete</li>
  <li><a href="obsolete#swprod">How Software Producers can distribute
      their products directly in .deb format</a>, stalled, outdated</li>
  <li><a href="obsolete#packman">Debian Packaging Manual</a>, partly
      merged in <a href="devel-manuals#policy">Debian Policy Manual</a>,
      the rest will be included in a dpkg reference manual that is yet to be
      written</li>
  <li><a href="obsolete#makeadeb">Introduction: Making a Debian
      Package</a>, obsoleted by
      <a href="devel-manuals#maint-guide">Debian New Maintainers' Guide</a></li>
  <li><a href="obsolete#programmers">Debian Programmers' Manual</a>,
      obsoleted by
      <a href="devel-manuals#maint-guide">Debian New Maintainers' Guide</a>
      and
      <a href="devel-manuals#debmake-doc">Guide for Debian Maintainers</a></li>
  <li><a href="obsolete#repo">Debian Repository HOWTO</a>, outdated after introduction of secure APT</li>
  <li><a href="obsolete#i18n">Introduction to i18n</a>, stalled</li>
  <li><a href="obsolete#sgml-howto">Debian SGML/XML HOWTO</a>, stalled, obsolete</li>
  <li><a href="obsolete#markup">Debiandoc-SGML Markup Manual</a>, stalled; DebianDoc is about to being removed</li>
</ul>
