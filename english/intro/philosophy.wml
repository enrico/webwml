#use wml::debian::template title="Our Philosophy: Why we do it and how we do it" MAINPAGE="true"

#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freedom">Our Mission: Creating a Free Operating System</a></li>
    <li><a href="#how">Our Values: How the Debian Community works</a></li>
  </ul>
</div>

<h2><a id="freedom">Our Mission: Creating a Free Operating System</a></h2>

<p>The Debian Project is an association of individuals, sharing a common
goal: We want to create a free operating system, freely available for
everyone. Now, when we use the word "free", we're not talking about money,
instead, we are referring to software <em>freedom</em>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-unlock-alt fa-2x"></span> <a href="free">Our Definition of Free Software</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.gnu.org/philosophy/free-sw">Read the Free Software Foundation's statement</a></button></p>

<p>Maybe you're wondering why so many people choose to spend lots
of hours of their own time writing software, carefully packaging and
maintaining it, just to give it all away without charging for it? Well,
there are plenty of reasons, here are some of them:</p>

<ul>
  <li>Some people simply like to help others, and contributing to a free software project is a great way to accomplish this.</li>
  <li>Many developers write programs to learn more about computers, different architectures, and programming languages.</li>
  <li>Some developers contribute to say "thank you" for all the great free software they've received from others.</li>
  <li>Many people in academia create free software to share the results of their research.</li>
  <li>Companies also help maintaining free software: to influence how applications develop or to implement new features quickly.</li>
  <li>Of course, most Debian developers participate, because they think it's great fun!</li>
</ul>

<p>Although we believe in free software, we respect that people sometimes
have to install non-free software on their machines – whether they
want to or not. We have decided to support these users, whenever
possible. There is a growing number of packages which install non-free
software on a Debian system.</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> We are committed to Free Software and we have formalized that commitment in a document: our <a href="$(HOME)/social_contract">Social Contract</a></p>
</aside>

<h2><a id="how">Our Values: How the Community works</a></h2>

<p>The Debian project has more than thousand active <a
href="people">developers and contributors</a> spread <a
href="$(DEVEL)/developers.loc">around the world</a>. A project of this
size needs a carefully <a href="organization">organized structure</a>.
So, if you wonder how the Debian project works and whether the Debian
community has rules and guidelines, have a look at the following
statements:</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">The Debian Constitution</a>: <br>
          This document describes the organizational structure and explains how the Debian project makes formal decisions.</li>
        <li><a href="../social_contract">The Social Contract and the Free Software Guidelines</a>: <br>
          The Debian Social Contract and the Debian Free Software Guidelines (DFSG) as a part of this contract describe our commitment to free software and the free software community.</li>
        <li><a href="diversity">The Diversity Statement:</a> <br>
          The Debian Project welcomes and encourages everyone to participate, no matter how you identify yourself or how others perceive you.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><a href="../code_of_conduct">The Code of Conduct:</a> <br>
          We have adopted a code of conduct for participants of our mailing lists, IRC channels, etc.</li>
        <li><a href="../doc/developers-reference/">The Developer's Reference:</a> <br>
          This document provides an overview of the recommended procedures and the available resources for Debian developers and maintainers.</li>
        <li><a href="../doc/debian-policy/">The Debian Policy:</a> <br>
          A manual describing the policy requirements for the Debian distribution, e.g. the Debian archive's structure and contents, technical requirements which every package must satisfy to be included, etc.</li>
      </ul>
    </div>
  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-code fa-2x"></span> Inside Debian: <a href="$(DEVEL)/">Developers' Corner</a></button></p>
