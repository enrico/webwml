# Brazilian Portuguese translation for Debian website organization.pot
# Copyright (C) 2003-2017 Software in the Public Interest, Inc.
#
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2009
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2011-2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-02-02 21:38-0300\n"
"Last-Translator: Thiago Pezzo (tico) <pezzo@protonmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "mensagem de delegação"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "mensagem de nomeação"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegado"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"female\"/>delegada"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>ele/o"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>ela/a"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>delegado(a)"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>eles(as)/os(as)"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "atual"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "membro(a)"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "gerente"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Gerente de Lançamento da Estável"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "mago(a)"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "presidente(a)"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "assistente"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "secretário(a)"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "representante"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "função"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"Na lista a seguir, <q>atual</q> é usado para posições que são\n"
"transitórias (eleito(a) ou nomeado(a) com uma determinada data de expiração)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Oficiais"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:107
msgid "Distribution"
msgstr "Distribuição"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:204
msgid "Communication and Outreach"
msgstr "Comunicações e Outreach (Programa de extensão)"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:207
msgid "Data Protection team"
msgstr "Equipe de Proteção de Dados"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:212
msgid "Publicity team"
msgstr "Equipe de Publicidade"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:279
msgid "Membership in other organizations"
msgstr "Participação em outras organizações"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:302
msgid "Support and Infrastructure"
msgstr "Suporte e Infraestrutura"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Líder"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Comitê Técnico"

#: ../../english/intro/organization.data:102
msgid "Secretary"
msgstr "Secretário(a)"

#: ../../english/intro/organization.data:110
msgid "Development Projects"
msgstr "Projetos de Desenvolvimento"

#: ../../english/intro/organization.data:111
msgid "FTP Archives"
msgstr "Repositórios FTP"

#: ../../english/intro/organization.data:113
msgid "FTP Masters"
msgstr "FTP Masters"

#: ../../english/intro/organization.data:119
msgid "FTP Assistants"
msgstr "Assistentes FTP"

#: ../../english/intro/organization.data:125
msgid "FTP Wizards"
msgstr "Assistentes FTP"

#: ../../english/intro/organization.data:129
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:131
msgid "Backports Team"
msgstr "Equipe de Backports"

#: ../../english/intro/organization.data:135
msgid "Release Management"
msgstr "Gerência de Lançamento"

#: ../../english/intro/organization.data:137
msgid "Release Team"
msgstr "Equipe de Lançamento"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Controle de Qualidade"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Equipe do Sistema de Instalação"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr "Equipe Debian Live"

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Notas de Lançamento"

#: ../../english/intro/organization.data:152
msgid "CD/DVD/USB Images"
msgstr "Imagens de CD/DVD/USB"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Produção"

#: ../../english/intro/organization.data:162
msgid "Testing"
msgstr "Testing"

#: ../../english/intro/organization.data:164
msgid "Cloud Team"
msgstr "Equipe Cloud"

#: ../../english/intro/organization.data:168
msgid "Autobuilding infrastructure"
msgstr "Infraestrutura de autoconstrução"

#: ../../english/intro/organization.data:170
msgid "Wanna-build team"
msgstr "Equipe wanna-build"

#: ../../english/intro/organization.data:177
msgid "Buildd administration"
msgstr "Administração de Buildd"

#: ../../english/intro/organization.data:194
msgid "Documentation"
msgstr "Documentação"

#: ../../english/intro/organization.data:199
msgid "Work-Needing and Prospective Packages list"
msgstr "Lista de Pacotes Prospectivos e que Necessitam de Trabalho"

#: ../../english/intro/organization.data:215
msgid "Press Contact"
msgstr "Contato de Imprensa"

#: ../../english/intro/organization.data:217
msgid "Web Pages"
msgstr "Páginas Web"

#: ../../english/intro/organization.data:225
msgid "Planet Debian"
msgstr "Planeta Debian"

#: ../../english/intro/organization.data:230
msgid "Outreach"
msgstr "Programa de extensão (Outreach)"

#: ../../english/intro/organization.data:235
msgid "Debian Women Project"
msgstr "Projeto Debian Women"

#: ../../english/intro/organization.data:243
msgid "Community"
msgstr "Comunidade"

#: ../../english/intro/organization.data:250
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"Para enviar uma mensagem privada a todos(as) os(as) membros(as) da Equipe da "
"Comunidade, use a chave GPG <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:252
msgid "Events"
msgstr "Eventos"

#: ../../english/intro/organization.data:259
msgid "DebConf Committee"
msgstr "Comitê da DebConf"

#: ../../english/intro/organization.data:266
msgid "Partner Program"
msgstr "Programa de Parcerias"

#: ../../english/intro/organization.data:270
msgid "Hardware Donations Coordination"
msgstr "Coordenação de Doações de Hardware"

#: ../../english/intro/organization.data:285
msgid "GNOME Foundation"
msgstr "Fundação GNOME"

#: ../../english/intro/organization.data:287
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:288
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:290
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:291
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:292
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"

#: ../../english/intro/organization.data:295
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:298
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:305
msgid "Bug Tracking System"
msgstr "Sistema de Acompanhamento de Bugs"

#: ../../english/intro/organization.data:310
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr ""
"Administração das Listas de Discussão e dos Arquivos das Listas de Discussão"

#: ../../english/intro/organization.data:318
msgid "New Members Front Desk"
msgstr "Recepção de Novos(as) Mantenedores(as)"

#: ../../english/intro/organization.data:327
msgid "Debian Account Managers"
msgstr "Gerentes de Contas Debian"

#: ../../english/intro/organization.data:333
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Para enviar uma mensagem privada para todos(as) os(as) DAMs, use a chave GPG "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:334
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Mantenedores(as) do Chaveiro (Keyring) (PGP e GPG)"

#: ../../english/intro/organization.data:338
msgid "Security Team"
msgstr "Equipe de Segurança"

#: ../../english/intro/organization.data:350
msgid "Policy"
msgstr "Política (Policy)"

#: ../../english/intro/organization.data:353
msgid "System Administration"
msgstr "Administração do Sistema"

#: ../../english/intro/organization.data:354
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Este é o endereço que se deve usar quando encontrar problemas em uma das "
"máquinas do Debian, incluindo problemas de senha ou em caso de precisar que "
"um determinado pacote seja instalado."

#: ../../english/intro/organization.data:363
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Se tiver problemas com o hardware de máquinas do Debian, por favor consulte "
"<a href=\"https://db.debian.org/machines.cgi\">Máquinas Debian</a>, ela "
"contém informações sobre o(a) administrador(a) de cada máquina."

#: ../../english/intro/organization.data:364
msgid "LDAP Developer Directory Administrator"
msgstr "Administrador(a) do diretório LDAP de desenvolvedores(as)"

#: ../../english/intro/organization.data:365
msgid "Mirrors"
msgstr "Espelhos"

#: ../../english/intro/organization.data:368
msgid "DNS Maintainer"
msgstr "Mantenedor(a) do DNS"

#: ../../english/intro/organization.data:369
msgid "Package Tracking System"
msgstr "Sistema de Acompanhamento de Pacotes"

#: ../../english/intro/organization.data:371
msgid "Treasurer"
msgstr "Tesoureiro(a)"

#: ../../english/intro/organization.data:376
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Pedidos de uso da <a name=\"trademark\" href=\"m4_HOME/trademark\">marca "
"registrada</a>"

#: ../../english/intro/organization.data:380
msgid "Salsa administrators"
msgstr "Administradores(as) do Salsa"

#~ msgid "APT Team"
#~ msgstr "Equipe do APT"

#~ msgid "Accountant"
#~ msgstr "Contador"

#~ msgid "Alioth administrators"
#~ msgstr "Administração do Alioth"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (Não ativo: não foi lançado com o squeeze)"

#~ msgid "Anti-harassment"
#~ msgstr "Antiassédio"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Bits from Debian"
#~ msgstr "Breves do Debian"

#~ msgid "CD Vendors Page"
#~ msgstr "Página de Vendedores de CD"

#~ msgid "Consultants Page"
#~ msgstr "Página de Consultores"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Distribuição Personalizada Debian"

#~ msgid "DebConf chairs"
#~ msgstr "Presidentes da DebConf"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux para Computação Empresarial"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr ""
#~ "Mantenedores do Chaveiro (Keyring) de Mantenedores Debian (Debian "
#~ "Maintainer)"

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Distribuição Debian Multimídia"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian para educação"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian para crianças de 1 a 99"

#~ msgid "Debian for education"
#~ msgstr "Debian para educação"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian para prática e pesquisa médica"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian para organizações sem fins lucrativos"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian para pessoas com deficiências"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian para ciência e pesquisa relacionada"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian em escritórios legais"

#~ msgid "Embedded systems"
#~ msgstr "Sistemas embarcados/embutidos"

#~ msgid "Firewalls"
#~ msgstr "Firewalls"

#~ msgid "Handhelds"
#~ msgstr "Handhelds"

#~ msgid "Individual Packages"
#~ msgstr "Pacotes Individuais"

#~ msgid "Installation System for ``stable''"
#~ msgstr "Sistema de Instalação para ``estável''"

#~ msgid "Key Signing Coordination"
#~ msgstr "Coordenação de Assinatura de Chaves"

#~ msgid "Laptops"
#~ msgstr "Laptops"

#~ msgid "Live System Team"
#~ msgstr "Equipe do Sistema \"Live\""

#~ msgid "Mailing List Archives"
#~ msgstr "Arquivos das Listas de Discussão"

#~ msgid "Marketing Team"
#~ msgstr "Equipe de Marketing"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Nomes dos administradores das buildds também podem ser encontrados em <a "
#~ "href=\"http://www.buildd.net\">http://www.buildd.net</a>. Escolha uma "
#~ "arquitetura e a distribuição para ver as buildds disponíveis e seus "
#~ "administradores."

#~ msgid "Ports"
#~ msgstr "Portes"

#~ msgid "Publicity"
#~ msgstr "Publicidade"

#~ msgid "Release Assistants"
#~ msgstr "Assistentes de Lançamento"

#~ msgid "Release Assistants for ``stable''"
#~ msgstr "Assistentes de Lançamento para ``estável'' (stable)"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Equipe de Lançamento para ``estável'' (stable)"

#~ msgid "Release Wizard"
#~ msgstr "Mago de Lançamento"

#~ msgid "SchoolForge"
#~ msgstr "SchoolForge"

#~ msgid "Security Audit Project"
#~ msgstr "Projeto de Auditoria de Segurança"

#~ msgid "Special Configurations"
#~ msgstr "Configurações Especiais"

#~ msgid "Testing Security Team"
#~ msgstr "Equipe de Segurança da Testing"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "O Sistema Operacional Universal como seu Desktop"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Os administradores responsáveis pelas buildds para uma arquitetura em "
#~ "particular podem ser contatados em <genericemail arch@buildd.debian.org>, "
#~ "por exemplo, <genericemail i386@buildd.debian.org>."

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Este ainda não é um projeto interno oficial do Debian mas anunciou a "
#~ "intenção de ser integrado."

#~ msgid "User support"
#~ msgstr "Suporte ao(a) usuário(a)"

#~ msgid "Vendors"
#~ msgstr "Distribuidores"

#~ msgid "Volatile Team"
#~ msgstr "Equipe da Volatile"

#~ msgid "current Debian Project Leader"
#~ msgstr "atual Líder do Projeto Debian"
