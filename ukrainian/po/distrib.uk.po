# translation of distrib.po to Ukrainian
# Eugeniy Meshcheryakov <eugen@univ.kiev.ua>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: distrib\n"
"PO-Revision-Date: 2006-08-12 18:09+0200\n"
"Last-Translator: Eugeniy Meshcheryakov <eugen@univ.kiev.ua>\n"
"Language-Team: Ukrainian <ukrainian>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"
"Plural-Forms:  nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Ключове слово"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Показувати"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr ""

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "пакунки, що містять файли з такою назвою"

#: ../../english/distrib/search_contents-form.inc:22
#, fuzzy
msgid "packages that contain files whose names contain the keyword"
msgstr ""
"пакунки, що містять файли чи директорії назви яких містять ключове слово"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Дистрибутив"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr ""

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "нестабільний (unstable)"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "тестовий (testing)"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "стабільний (stable)"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "oldstable"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Архітектура"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "будь-який"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Шукати"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Скинути"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Шукати"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Тільки назви пакунків"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Описи"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Назви джерельних пакунків"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr ""

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Розділ"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "Alpha"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr ""

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr ""

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr ""

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr ""

#: ../../english/releases/arches.data:14
#, fuzzy
msgid "HP PA-RISC"
msgstr "HP PA/RISC"

#: ../../english/releases/arches.data:15
#, fuzzy
msgid "Hurd 32-bit PC (i386)"
msgstr "Hurd (i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr ""

#: ../../english/releases/arches.data:17
#, fuzzy
msgid "Intel Itanium IA-64"
msgstr "Intel IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr ""

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr ""

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "Motorola 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr ""

#: ../../english/releases/arches.data:22
msgid "64-bit MIPS (little endian)"
msgstr ""

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr ""

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr ""

#: ../../english/releases/arches.data:26
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr ""

#: ../../english/releases/arches.data:27
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:28
msgid "IBM System z"
msgstr ""

#: ../../english/releases/arches.data:29
msgid "SPARC"
msgstr "SPARC"

#~ msgid "packages that contain files or directories named like this"
#~ msgstr "пакунки, що містять файли чи директорії з такою назвою"

#~ msgid "all files in this package"
#~ msgstr "всі файли в цьому пакунку"

#~ msgid "Case sensitive"
#~ msgstr "Враховувати регістр"

#~ msgid "no"
#~ msgstr "ні"

#~ msgid "yes"
#~ msgstr "так"

#~ msgid "Intel x86"
#~ msgstr "Intel x86"

#~ msgid "AMD64"
#~ msgstr "AMD64"

#~ msgid "MIPS"
#~ msgstr "MIPS"

#~ msgid "MIPS (DEC)"
#~ msgstr "MIPS (DEC)"

#~ msgid "Allow searching on subwords"
#~ msgstr "Шукати частини слів"

#~ msgid "Search case sensitive"
#~ msgstr "Пошук чутливий до регістру"

#~ msgid "non-US"
#~ msgstr "non-US"
