# Debian Web Site. templates.it.po
# Copyright (C) 2002 Giuseppe Sacco.
# Giuseppe Sacco <eppesuigoccas@libero.it>, 2002.
# Giuseppe Sacco <eppesuig@debian.org>, 2004-2007.
# Luca Monducci <luca.mo@tiscali.it>, 2008-2012.
#
msgid ""
msgstr ""
"Project-Id-Version: wnpp.it\n"
"PO-Revision-Date: 2012-03-03 11:24+0200\n"
"Last-Translator: Luca Monducci <luca.mo@tiscali.it>\n"
"Language-Team: debian-l10n-italian <debian-l10n-italian@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/wnpp.wml:8
msgid "No requests for adoption"
msgstr "Nessuna richiesta di adozione"

#: ../../english/template/debian/wnpp.wml:12
msgid "No orphaned packages"
msgstr "Nessun pacchetto orfano"

#: ../../english/template/debian/wnpp.wml:16
msgid "No packages waiting to be adopted"
msgstr "Nessun pacchetto in attesa di adozione"

#: ../../english/template/debian/wnpp.wml:20
msgid "No packages waiting to be packaged"
msgstr "Nessun pacchetto in attesa di essere impacchettato"

#: ../../english/template/debian/wnpp.wml:24
msgid "No Requested packages"
msgstr "Nessun pacchetto richiesto"

#: ../../english/template/debian/wnpp.wml:28
msgid "No help requested"
msgstr "Nessun aiuto richiesto"

#. define messages for timespans
#. first the ones for being_adopted (ITAs)
#: ../../english/template/debian/wnpp.wml:34
msgid "in adoption since today."
msgstr "in adozione da oggi."

#: ../../english/template/debian/wnpp.wml:38
msgid "in adoption since yesterday."
msgstr "in adozione da ieri."

#: ../../english/template/debian/wnpp.wml:42
msgid "%s days in adoption."
msgstr "in adozione da %s giorni."

#: ../../english/template/debian/wnpp.wml:46
msgid "%s days in adoption, last activity today."
msgstr "in adozione da %s giorni, ultima attività oggi."

#: ../../english/template/debian/wnpp.wml:50
msgid "%s days in adoption, last activity yesterday."
msgstr "in adozione da %s giorni, ultima attività ieri."

#: ../../english/template/debian/wnpp.wml:54
msgid "%s days in adoption, last activity %s days ago."
msgstr "in adozione da %s giorni, ultima attività %s giorni fa."

#. timespans for being_packaged (ITPs)
#: ../../english/template/debian/wnpp.wml:60
msgid "in preparation since today."
msgstr "in preparazione da oggi."

#: ../../english/template/debian/wnpp.wml:64
msgid "in preparation since yesterday."
msgstr "in preparazione da ieri."

#: ../../english/template/debian/wnpp.wml:68
msgid "%s days in preparation."
msgstr "in preparazione da %s giorni."

#: ../../english/template/debian/wnpp.wml:72
msgid "%s days in preparation, last activity today."
msgstr "in preparazione da %s giorni, ultima attività oggi."

#: ../../english/template/debian/wnpp.wml:76
msgid "%s days in preparation, last activity yesterday."
msgstr "in preparazione da %s giorni, ultima attività ieri."

#: ../../english/template/debian/wnpp.wml:80
msgid "%s days in preparation, last activity %s days ago."
msgstr "in preparazione da %s giorni, ultima attività %s giorni fa."

#. timespans for request for adoption (RFAs)
#: ../../english/template/debian/wnpp.wml:85
msgid "adoption requested since today."
msgstr "adozione richiesta oggi."

#: ../../english/template/debian/wnpp.wml:89
msgid "adoption requested since yesterday."
msgstr "adozione richiesta ieri."

#: ../../english/template/debian/wnpp.wml:93
msgid "adoption requested since %s days."
msgstr "adozione richiesta da %s giorni."

#. timespans for orphaned packages (Os)
#: ../../english/template/debian/wnpp.wml:98
msgid "orphaned since today."
msgstr "orfano da oggi."

#: ../../english/template/debian/wnpp.wml:102
msgid "orphaned since yesterday."
msgstr "orfano da ieri."

#: ../../english/template/debian/wnpp.wml:106
msgid "orphaned since %s days."
msgstr "orfano da %s giorni."

#. time spans for requested (RFPs) and help requested (RFHs)
#: ../../english/template/debian/wnpp.wml:111
msgid "requested today."
msgstr "richiesto oggi."

#: ../../english/template/debian/wnpp.wml:115
msgid "requested yesterday."
msgstr "richiesto ieri."

#: ../../english/template/debian/wnpp.wml:119
msgid "requested %s days ago."
msgstr "richiesto da %s giorni."

#: ../../english/template/debian/wnpp.wml:133
msgid "package info"
msgstr "informazioni sul pacchetto"

#. popcon rank for RFH bugs
#: ../../english/template/debian/wnpp.wml:139
msgid "rank:"
msgstr "rango:"
