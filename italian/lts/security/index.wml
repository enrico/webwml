#use wml::debian::template title="Informazioni sulla sicurezza LTS" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="51b1f2756d728aa0beb3ba7e5f67a15ab3b8db98"


<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Mantenere il sistema Debian LTS
sicuro</toc-add-entry>

<p>
Per ricevere gli ultimi bollettini sulla sicurezza di Debian LTS, iscriviti
alla lista <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a>.
</p>

<p>
Per ulteriori informazioni sui problemi di sicurezza in Debian, fare
riferimento alle <a href="../../security">Informazioni sulla sicurezza</a>.
</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">Ultimi bollettini</toc-add-entry>

<p>
Queste pagine contengono un archivio dei bollettini della sicurezza
postate nella lista <a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a>.
</p>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (titles only)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (summaries)" href="dla-long">
:#rss#}
<p>
Gli ultimi bollettini della sicurezza di Debian LTS sono disponibili
anche in <a href="dla">formato RDF</a>. È anche disponibile anche un
<a href="dla-long">secondo file</a> che include il primo paragrafo del
relativo bollettino in modo che sia possibile capire cosa riguarda
l'avviso.
</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Sono disponibili anche i precedenti bollettini sulla sicurezza:
<:= get_past_sec_list(); :>

<p>
Le distribuzioni Debian non sono vulnerabili a qualsiasi problema di
sicurezza. Il <a href="https://security-tracker.debian.org/">Debian
Security Tracker</a> raccoglie tutte le informazioni sullo stato delle
vulnerabilità dei pacchetti Debian ed è possibile fare ricerche per
nome CVE oppure per pacchetto.
</p>


<toc-add-entry name="contact">Informazioni di contatto</toc-add-entry>

<p>
Consultare le <a href="https://wiki.debian.org/LTS/FAQ">FAQ su Debian
LTS</a> prima di contattarci, la propria domanda potrebbe già avere
una risposta!
</p>

<p>
Anche le <a href="https://wiki.debian.org/LTS/FAQ">informazioni di
contatto sono nelle FAQ</a>.
</p>
