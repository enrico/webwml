#use wml::debian::template title="I mirror di Debian" BARETITLE="true"
#use wml::debian::translation-check translation="6ab524a7b127f07748ca4f2b1168f6ed8b785a81" maintainer="Giuseppe Sacco"

<p>Debian è distribuita in tutto il mondo utilizzando dei mirror
al fine di fornire agli utenti una migliore accessibilità al nostro
archivio.</p>

<p>I seguenti archivi Debian sono copiati nei mirror:</p>

<dl>
<dt><strong>Pacchetti Debian</strong> (<code>debian/</code>)</dt>
  <dd>L'insieme di pacchetti Debian: questo include la grande
      maggioranza dei pacchetti .deb, il materiale per l'installazione
      e i sorgenti.
      <br/>
      Si veda l'elenco dei <a href="list">mirror Debian che includono
      l'archivio <code>debian/</code></a>.
  </dd>
<dt><strong>Immagini CD</strong> (<code>debian-cd/</code>)</dt>
  <dd>Il repository delle immagini dei CD Debian: file Jigdo e immagini ISO.
      <br/>
      Si veda l'elenco dei <a href="$(HOME)/CD/http-ftp/#mirrors">mirror Debian che
      includono l'archivio <code>debian-cd/</code></a>.
  </dd>
<dt><strong>Rilasci passati</strong> (<code>debian-archive/</code>)</dt>
  <dd>L'archivio dei precedenti rilasci di Debian.
      <br/>
      Si vedano gli <a href="$(HOME)/distrib/archive">archivi della
	  distribuzione</a> per maggiori informazioni.
  </dd>
</dl>

<h2>Informazioni per chi gestisce un mirror</h2>

<p>I mirror Debian sono gestiti da volontari, chiunque non disponga di
un buon mirror nelle proprie vicinanze e abbia la possibilità di donare
spazio disco e connettività, può creare un mirror e contribuire a rendere
Debian maggiormente accessibile. Consultare le pagine su come <a
href="ftpmirror">creare un mirror dell'archivio Debian</a> per maggiori
informazioni.</p>
