#use wml::debian::template title="Creare un mirror dell'archivio Debian"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/sid/archive.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="e466bedb14f9f661d1f4a77514cbe26596877486" maintainer="Luca Monducci"

<toc-display/>

<toc-add-entry name="whether">Se creare un mirror</toc-add-entry>

<p>I nuovi mirror sono sempre ben accetti ma ogni potenziale gestore di
un nuovo mirror dovrebbe rispondere alle seguenti domande prima di avviare
un proprio mirror:</p>

<ul>
<li>È necessario un mirror nella mia area geografica? Magari ce ne
sono già altri nelle vicinanze.</li>
<li>Dispongo delle risorse per ospitare un mirror? I mirror richiedono
un considerevole <a href="size">spazio su disco</a> e molta banda, si
deve essere in grado di coprirne i costi.</li>
<li>Un mirror è la scelta giusta? Se lo scopo principale è fornire il
servizio agli utenti del proprio ISP/struttura allora potrebbe essere una
scelta migliore un proxy, per esempio apt-cacher-ng, squid o varnish.</li>
</ul>

<toc-add-entry name="what">Cosa mettere nel mirror</toc-add-entry>

<p>La <a href="./">pagina principale dei mirror</a> elenca gli archivi
disponibili.</p>

<ul>
<li>Gli utenti cercheranno la directory debian/ per installare Debian
via rete, per scaricare i CD (usando jigdo) e per aggiornare sistemi
già installati. <em>Si raccomanda di fare il mirror di questo
repository.</em></li>

<li>debian-cd/ è una directory che non è identica su tutti i mirror.
Su qualche mirror conterrà i template di jigdo per produrre le immagini
dei CD (insieme ai file contenuti in debian/), da altri conterrà delle
immagini già complete, su altri ancora li conterrà entrambi.
<br/>
Controllare la pagina <a href="$(HOME)/CD/mirroring/">mirror delle immagini
dei CD</a> per ulteriori informazioni a riguardo.</li>

<li>debian-archive/ contiene il vero <em>archivio</em>: le versioni vecchie
e obsolete di Debian. Di solito è interessante solo per un piccolo numero
di utenti. (In caso di dubbi non fare il mirror di questo archivio.)</li>
</ul>

<p>Visitare la pagina delle <a href="size">dimensioni dei mirror</a> per
informazioni più precise riguardo la grandezza dei mirror.</p>

<p>La directory debian-security/ contiene solamente gli aggiornamenti di
sicurezza rilasciati dal Debian Security Team. Nonostante sembri interessante,
non è consigliabile agli utenti di recuperare gli aggiornamenti di sicurezza
da un mirror, invece è raccomandato scaricarli dal servizio distribuito
security.debian.org. <em>Si raccomanda di <strong>not</strong> fare il mirror
di questo repository.</em></p>

<toc-add-entry name="wherefrom">Da dove sincronizzarsi</toc-add-entry>

<p><code>ftp.debian.org</code> non è la locazione canonica dei pacchetti
Debian, invece è solamente uno dei tanti server che sono aggiornati da un
server interno di Debian. Ci sono molti <a href="list-full">mirror
pubblici</a> che gestiscono rsync da cui poter fare la sincronizzazione.
Si raccomanda di utilizzarne uno rete-geograficamente vicino.</p>

<p>Si deve evitare di scegliere come fonte un qualsiasi nome che viene
risolto in più di un indirizzo (come <code>ftp.us.debian.org</code>)
perché nel caso che un server non sia disponibile la sincronizzazione
potrebbe avvenire con server in stato diverso diverso da quello con cui si
è solitamente collegati e quindi perdere la sincronia.
#
Notare anche che l'unico servizio di cui è garantita l'esistenza per
<code>ftp.CC.debian.org</code> è HTTP. A chi vuole fare il mirror usando
rsync si consiglia di scegliere il nome della macchina che attualmente sta
fornendo il servizio per <code>ftp.CC.debian.org</code> (consultare la
lista <code>/debian/project/trace</code> sul server per conoscerlo).</p>


<toc-add-entry name="how">Come sincronizzarsi</toc-add-entry>

<p>Il metodo raccomandato per sincronizzarsi è l'insieme di script
ftpsync, disponibile in due formati:</p>
<ul>
    <li>come tarball da <url "https://ftp-master.debian.org/ftpsync.tar.gz"></li>
    <li>dal repository git: <kbd>git clone https://salsa.debian.org/mirror-team/archvsync.git</kbd>
	(vedere <url "https://salsa.debian.org/mirror-team/archvsync/">)</li>
</ul>

<p>Si raccomanda di non usare script fatti da soli né di usare direttamente
rsync. L'uso di ftpsync assicura che gli aggiornamenti sono in modo da non
confondere apt, in particolare ftpsync elabora traduzioni, contenuti e altri
file con metadati in modo che apt non trovi errori di validazione nel caso
un utente esegua un aggiornamento mentre è in corso la sincronizzazione del
server. Inoltre produce anche un file di traccia con molte informazioni
utili per determinare se il mirro funziona, quali architetture espone
e da dove si sincronizza.</p>


<toc-add-entry name="partial">Mirroring parziale</toc-add-entry>

<p>Considerando le già <a href="size">grandi dimensioni dell'archivio
Debian</a>, potrebbe essere consigliabile fare il mirror solo di parti
dell'archivio. I mirror pubblici dovrebbero esporre tutte le varianti
(testing, unstable, ecc.) ma potrebbero restringere l'insieme delle
architeture. Proprio per questo scopo il file di configurazione di ftpsync
dispone delle impostazioni ARCH_EXCLUDE e ARCH_INCLUDE.</p>


<toc-add-entry name="when">Quando sincronizzarsi</toc-add-entry>

<p>L'archivio principale viene aggiornato quattro volte al giorno. I mirror
solitamente iniziano la sincronizzazione intorno alle 03:00, 09:00, 15:00 e
21:00 (tutti orari UTC) ma questi non sono mai orari definitivi e ogni mirror
non si dovrebbe fissare su quelli.</p>

<p>Il mirror andrebbe aggiornato dopo qualche ora rispetto agli
aggiornamenti dell'archivio principale.
È opportuno controllare se il mirror utilizzato per la sincronizzazione
crea un file timestamp nella sua directory <kbd>project/trace/</kbd>. Il
file di timestamp sarà chiamato come il sito, e conterrà l'orario di
completamento dell'ultimo aggiornamento. Aggiungere un paio di ore da
quell'orario (per essere sicuri) e avviare la sincronizzazione a
quell'ora.</p>

<p><b>È essenziale che il proprio mirror sia sincronizzato con l'archivio
principale</b>. Un minimo di 4 aggiornamenti ogni 24 ore garantiranno che
il proprio mirror sia un vero specchio dell'archivio. I mirror non
sincronizzati con l'archivio principale non saranno inseriti nell'elenco
ufficiale dei mirror.</p>

<p>La maniera più semplice per sincronizzarsi ogni giorno è usare
cron. Consultare <kbd>man crontab</kbd> per dettagli.</p>

<p>Notare che se l'aggiornamento del proprio mirror viene attivato tramite
il meccanismo di push, allora questa operazione non è necessaria.</p>

<h3>Avvio del mirroring con push</h3>

<p>Il <q>push mirroring</q> è una forma di mirroring basata su rsync che
è stata sviluppata per minimizzare i tempi che impiegano i cambiamenti
dell'archivio a raggiungere i vari mirror. Il server di origine usa un
trigger SSH per avvisare i mirror destinatari di aggiornarsi.
Per una descrizione più dettagliata del funzionamento, perché è più
sicuro e come creare un push mirror, consultare <a href="push_mirroring">la
spiegazione completa</a>.

<toc-add-entry name="settings">Ulteriori impostazioni raccomandate</toc-add-entry>

<p>I mirror pubblici dovrebbero rendere disponibile l'archivio Debian in
HTTP come <code>/debian</code>.</p>

<p>Inoltre, assicurarsi che sia possibile vedere l'elenco della directory
(con i nomi dei file completi) e che funzionino i collegamenti simbolici.
Con Apache dovrebbe essere sufficiente:
<pre>&lt;Directory <var>/percorso/al/vostro/mirror/debian</var>&gt;
   Options +Indexes +SymlinksIfOwnerMatch
   IndexOptions NameWidth=* +SuppressDescription
&lt;/Directory&gt;
</pre>


<toc-add-entry name="submit">Come aggiungere un mirror alla lista dei
mirror</toc-add-entry>

<p>
Per inserire il proprio mirror nell'elenco ufficiale è necessario:
</p>

<ul>
<li>assicurarsi che il proprio mirror si sincronizzi con l'archivio
4 volte in 24 ore</li>
<li>assicurarsi che il proprio mirror includa anche i file sorgente
delle architetture contenute</li>
</ul>

<p>Una volta che il mirror è pronto, deve essere <a
href="submit">registrato in Debian</a> per essere inserito nella <a
href="list">lista dei mirror</a>.
Gli invii delle registrazioni possono essere effettuati utilizzando il nostro
<a href="submit">semplice form web</a>.</p>

<p>Problemi o domande possono essere inoltrati a <email mirrors@debian.org>.</p>


<toc-add-entry name="mailinglists">Liste di messaggi</toc-add-entry>

<p>Ci sono due <a href="../MailingLists/">liste di messaggi</a>, entrambe in
lingua inglese, sui mirror Debian: <a
href="https://lists.debian.org/debian-mirrors-announce/">debian-mirrors-announce</a>
e <a href="https://lists.debian.org/debian-mirrors/">debian-mirrors</a>.
Tutti i gestori di mirror sono invitati a iscriversi alla lista degli
annunci perché è usata per tutti gli annunci importanti. Questa lista è
moderata e non ha molto traffico. La seconda lista è destinata alla
discussione generale ed è aperta a tutti.</p>

<p>Per qualsiasi domanda che non abbia trovato risposta in queste pagine
web, è possibile contattaci all'indirizzo <email mirrors@debian.org/>
oppure tramite il canale IRC #debian-mirrors su <tt>irc.debian.org</tt>.</p>


<toc-add-entry name="private-mirror">Note per mirror privati
(parziali)</toc-add-entry>

<p>
A chi vuol gestire un mirror solo per il proprio sito e si ha bisogno solo
di esporre un sottoinsieme delle varianti (per esempio stable), si consiglia
di usare <a href="https://packages.debian.org/stable/debmirror">debmirror</a>.
</p>
