# SOME DESCRIPTIVE TITLE.
# Copyright ©
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2016-01-13 19:43+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr ""

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr ""

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr ""

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr ""

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr ""

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr ""

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr ""

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr ""

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr ""

#: ../../stattrans.pl:474
msgid "hits"
msgstr ""

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr ""

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr ""

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr ""

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr ""

#: ../../stattrans.pl:611
msgid "Translated"
msgstr ""

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr ""

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr ""

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr ""

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr ""

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr ""

#: ../../stattrans.pl:638
msgid "Diff"
msgstr ""

#: ../../stattrans.pl:640
msgid "Comment"
msgstr ""

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr ""

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:644
msgid "Log"
msgstr ""

#: ../../stattrans.pl:645
msgid "Translation"
msgstr ""

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr ""

#: ../../stattrans.pl:648
msgid "Status"
msgstr ""

#: ../../stattrans.pl:649
msgid "Translator"
msgstr ""

#: ../../stattrans.pl:650
msgid "Date"
msgstr ""

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr ""

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr ""

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr ""

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr ""

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr ""

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr ""

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr ""

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr ""

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr ""

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr ""

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr ""

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr ""

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr ""

#: ../../stattrans.pl:694
msgid "Total"
msgstr ""

#: ../../stattrans.pl:711
msgid "Total:"
msgstr ""

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr ""

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr ""

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr ""

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr ""

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr ""

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr ""
