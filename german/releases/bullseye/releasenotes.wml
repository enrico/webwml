#use wml::debian::template title="Debian 11 -- Veröffentlichungshinweise" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="ce7e85638a047b969574a7abeb8a634c27b9086b"


<if-stable-release release="buster">
<p>Dies ist eine <strong>in Arbeit</strong> befindliche Version der
Veröffentlichungshinweise für Debian 11, Codename Bullseye, die noch nicht
veröffentlicht ist. Die hier aufgeführten Informationen könnten ungenau
und veraltet sein und sind höchstwahrscheinlich unvollständig.</p>
</if-stable-release>

<p>Wenn Sie wissen wollen, was in Debian 11 neu ist, dann lesen Sie die
Veröffentlichungshinweise für Ihre Architektur:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Veröffentlichungshinweise'); :>
</ul>

<p>Die Veröffentlichungshinweise beinhalten ebenfalls Informationen für
   Benutzer, die ein Upgrade von früheren Versionen durchführen möchten.</p>

<p>Wenn Sie Ihren Browser richtig auf Ihre Sprache
eingestellt haben, können Sie den obigen Link verwenden, um
automatisch die richtige HTML-Version zu bekommen &ndash; siehe auch
<a href="$(HOME)/intro/cn">Inhalts-Aushandlung</a>. Andernfalls müssen
Sie selber aus der Tabelle unten die richtige Architektur, Sprache und
Format aussuchen.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architektur</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Sprachen</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
