#use wml::debian::template title="Dokumentation"
#use wml::debian::translation-check translation="fca004f92b97a053bc89c121827943f1eae0531b"
# $Id$
# Translation update: Holger Wansing <linux@wansing-online.de>, 2015-2017.
# Translation update: Holger Wansing <hwansing@mailbox.org>, 2020.
# Translation update: Fabian Baumanis <fabian.baumanis@mailbox.org>, 2022.

<p>Ein wichtiger Teil eines jeden Betriebssystems ist die Dokumentation,
technische Handbücher, die den Ablauf und das Benutzen von Programmen
beschreiben. Als Teil der Bemühungen, ein qualitativ hochwertiges freies
Betriebssystem zu entwickeln, unternimmt das Debian-Projekt alle
Anstrengungen, um seine Benutzer mit geeigneter Dokumentation in einfach
verfügbarer Form zu versorgen.</p>

<h2>Schnellstart</h2>

<p>Falls Sie <em>neu</em> sind bei Debian, empfehlen wir Ihnen, zuerst
   folgendes zu lesen:</p>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">Installationsanleitung</a></li>
  <li><a href="manuals/debian-faq/">Debian GNU/Linux FAQ</a></li>
</ul>

<p>Sie sollten diese bei Ihrer ersten Debian-Installation griffbereit haben,
   sie werden Ihnen wahrscheinlich viele Fragen beantworten und
   Ihnen beim Arbeiten mit dem Debian-System helfen. Später können Sie sich durch
   folgende Dokumente arbeiten:</p>

<ul>
  <li><a href="manuals/debian-handbook/">das Debian Administrationshandbuch</a>,
      ein umfassendes Benutzerhandbuch</li>
  <li><a href="manuals/debian-reference/">Debian-Referenz</a>,
      ein prägnantes Benutzerhandbuch mit Fokus auf die
      Shell-Befehlszeile</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">\
      Veröffentlichungshinweise</a>, für Personen, die eine Systemaktualisierung
      durchführen möchten</li>
  <li><a href="https://wiki.debian.org/">Debian-Wiki</a>, eine gute
      Informationsquelle für Neulinge.</li>
</ul>


<p>Stellen Sie auch sicher, dass Sie die <a
   href="https://www.debian.org/doc/manuals/refcard/refcard">Debian
   GNU/Linux-Referenzkarte</a>, eine Auflistung der wichtigsten Befehle für
   Debian-Systeme, ausdrucken und griffbereit haben.</p>

<p>Es gibt eine ganze Reihe weiterer Dokumentation, die unten aufgeführt
   ist.</p>

<h2>Arten von Dokumentation</h2>

<p>Der größte Teil der Dokumentation in Debian wurde generell für
GNU/Linux geschrieben. Es gibt aber auch Dokumentation, die speziell für
Debian geschrieben wurde. Diese Dokumente kann man in folgende grundlegende
Kategorien unterteilen:</p>

<ul>
  <li><a href="#manuals">Anleitungen</a></li>
  <li><a href="#howtos">Howtos</a></li>
  <li><a href="#faqs">FAQs</a></li>
  <li><a href="#other">weitere kürzere Dokumente</a></li>
</ul>


<h3 id="manuals">Anleitungen</h3>

<p>Die Anleitungen gleichen Büchern, da sie umfassend Hauptthemen
beschreiben.</p>

<p>Viele der hier aufgelisteten Anleitungen sind sowohl als Debian-Paket als auch online
verfügbar. Tatsächlich stammen die meisten Anleitungen auf der Website aus ihren jeweiligen Debian-Paketen. Wählen Sie die gewünschte Anleitung im nächsten Absatz aus, um den Paket-Namen oder einen Link zu einer Online-Version zu erhalten.</p>

<h3>Debian-spezifische Anleitungen</h3>

<div class="line">
  <div class="item col50">

    <h4><a href="user-manuals">Benutzer-Handbücher</a></h4>
    <ul>
      <li><a href="user-manuals#faq">Debian GNU/Linux-FAQ</a></li>
      <li><a href="user-manuals#install">Debian-Installationsanleitung</a></li>
      <li><a href="user-manuals#relnotes">Debian-Veröffentlichungshinweise (Release Notes)</a></li>
      <li><a href="user-manuals#refcard">Debian-Referenzkarte</a></li>
      <li><a href="user-manuals#debian-handbook">Das Debian Administrationshandbuch</a></li>
      <li><a href="user-manuals#quick-reference">Debian-Referenz</a></li>
      <li><a href="user-manuals#securing">Securing-Debian-Handbuch</a></li>
      <li><a href="user-manuals#aptitude">aptitude-Benutzeranleitung</a></li>
      <li><a href="user-manuals#apt-guide">APT-Benutzeranleitung</a></li>
      <li><a href="user-manuals#apt-offline">APT Offline verwenden</a></li>
      <li><a href="user-manuals#java-faq">Debian-GNU/Linux- und Java-FAQ</a></li>
      <li><a href="user-manuals#hamradio-maintguide">Debian-Handbuch für Hamradio-Paketbetreuer</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Entwickler-Handbücher</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Debian Policy-Handbuch</a></li>
      <li><a href="devel-manuals#devref">Debian-Entwicklerreferenz</a></li>
      <li><a href="devel-manuals#debmake-doc">Handbuch für Debian-Paketbetreuer</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Einführung in Debian-Paketierung</a></li>
      <li><a href="devel-manuals#menu">Debian Menü-System</a></li>
      <li><a href="devel-manuals#d-i-internals">Debian-Installer Internals</a></li>
      <li><a href="devel-manuals#dbconfig-common">Handbuch für Paketbetreuer, die Datenbanken nutzen</a></li>
      <li><a href="devel-manuals#dbapp-policy">Richtlinien für Pakete, in denen Datenbanken genutzt werden</a></li>
    </ul>

    <h4><a href="misc-manuals">Verschiedene Handbücher</a></h4>
    <ul>
      <li><a href="misc-manuals#history">Debian Projekt-Geschichte</a></li>
    </ul>

  </div>


</div>

<p class="clr">Eine vollständige Liste von Debian-Anleitungen und weiterer Dokumentation
finden Sie auf den Webseiten des <a href="ddp">Debian-Dokumentationsprojekts</a>.</p>

<p>Es gibt auch einige auf Benutzer ausgelegte Anleitungen für Debian
GNU/Linux, verfügbar als <a href="books">gedruckte
Bücher</a>.</p>


<h3 id="howtos">Howtos</h3>

<p>Die <a href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">Howto-Dokumente</a>
beschreiben, wie ihr Name schon sagt, <em>wie man etwas macht</em>, und
behandelt üblicherweise ein spezielleres Thema.</p>


<h3 id="faqs">FAQs</h3>

<p>FAQ steht für <em>frequently asked questions</em> (häufig gestellte
Fragen). Eine FAQ ist ein Dokument, das diese Fragen beantwortet.</p>

<p>Fragen speziell zu
Debian werden in der <a href="manuals/debian-faq/">Debian-FAQ</a> beantwortet.
Es gibt außerdem eine separate <a href="../CD/faq/">FAQ für Debian
CD/DVD-Images</a>.</p>


<h3 id="other">Weitere, kürzere Dokumente</h3>

<p>Die folgenden Dokumente beinhalten schnellere, kürzere Anweisungen:</p>

<dl>
 <dt><strong><a href="https://tldp.org/docs.html#man">\
      Handbuchseiten</a></strong></dt>
    <dd>Traditionell sind alle Unix-Programme in <em>Handbuchseiten</em>
        dokumentiert, das sind Referenzhandbücher, die Sie über den <tt>man</tt>-Befehl
        erreichen können. Sie sind üblicherweise nicht für Anfänger gedacht.
        Auf <a href="https://manpages.debian.org/cgi-bin/man.cgi">\
	https://manpages.debian.org/</a> können Sie nach Handbuchseiten suchen
        und sie auch lesen.
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">\
      Info-Dateien</a></strong></dt>
    <dd>Viel GNU-Software wird über <em>Info-Dateien</em> anstatt über
        Handbuchseiten dokumentiert. Diese Dateien enthalten detaillierte
	Informationen über das Programm selbst; Optionen und Beispiele für
	die Verwendung sind über den <tt>info</tt>-Befehl verfügbar.
    </dd>

  <dt><strong>verschiedene README-Dateien</strong></dt>
    <dd>Die <em>read me</em>-(lies mich)-Dateien sind ebenfalls verbreitet &ndash;
        es sind einfache Textdateien, die einen einzelnen Punkt beschreiben,
        üblicherweise ein Paket. Sie können viele davon in den
        Verzeichnissen unterhalb von <tt>/usr/share/doc/</tt> auf Ihrem Debian-System
        finden. Jedes Software-Paket hat dort ein Unterverzeichnis mit seinen eigenen
	Read-Me-Dateien, diese können auch Konfigurationsbeispiele
	enthalten. Beachten Sie, dass für größere Programme die Dokumentation
	typischerweise in einem separaten Paket (mit dem gleichen Namen wie das
	Original-Paket, aber auf <em>-doc</em> endend) bereitgestellt wird.
    </dd>

  <dt><strong>Schnell-Referenzkarten</strong></dt>
    <dd>
      <p>Schnell-Referenzkarten sind sehr kurze Zusammenfassungen von
         bestimmten (Unter-)Systemen. Gewöhnlich enthält solch eine Karte die
         häufigsten Befehle auf einem Stück Papier. Einige bemerkenswerte
	 Referenzkarten und Sammlungen sind:</p>

         <dl>
          <dt><strong><a href="https://www.debian.org/doc/manuals/refcard/refcard">\
	      Debian GNU/Linux-Referenzkarte</a></strong></dt>
   <dd>Diese Karte, die auf einem einzelnen Blatt ausgedruckt werden kann,
       stellt eine Liste der wichtigsten Befehle bereit und ist eine gute
       Referenz für neue Benutzer von Debian, die sich mit ihnen vertraut
       machen möchten. Zumindest grundlegendes Wissen über Computer, Dateien,
       Verzeichnisse und die Kommandozeile wird benötigt. Neue Benutzer sollten
       vielleicht zuerst die <a href="user-manuals#quick-reference">\
       Debian-Referenz</a> lesen.
   </dd>

         </dl>
    </dd>
</dl>

<hrline />

<p>Wenn Sie die obigen Quellen durchsucht und immer noch keine Antworten
auf Ihre Fragen oder noch keine Lösungen für Ihre Debian-Probleme gefunden haben,
besuchen Sie die <a href="../support">Support-Seite</a>.</p>
